/*
 * $Id: Mainpage.h,v 1.5 2009-05-18 08:39:09 cholm Exp $
 *
 * PVSS RCU++ plugin
 * Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License 
 * as published by the Free Software Foundation; either version 2 of 
 * the License, or (at your option) any later version.  
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. 
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 * MA 02111-1307  USA  
 *
 */
/** @file   Mainpage.h
    @author Christian Holm
    @date   Sun Sep 15 16:29:24 2002
    @brief  Title page documentation. */
/** @mainpage PVSS Plugin for ALICE RCU Abstraction layer

    @section intro Introduction 
    
    This is a control script plugin for PVSS II that calls Rcu++
    functions.

    PVSS II is a SCADA system

    Rcu++ is a C++ class library to communicate with the ALICE RCU via
    various channels of communication.  It can talk to the RCU via

    - a FeeServer (running in an embedded Linux machine on the DCS
      daughter card),
    - a FedServer (running in the InterCom Layer software). 
    - a DDL link (directly on the DAQ LDC machine the RCU is attached
      to). 
    - USB interface (for the U2F test card). 

    @section usage Usage

    Compile and install the plugin: 
    @verbatim
	./configure 
	make 
	make install 
    @endverbatim 

    Then, make a symlink from the plugin (@e pkglibdir@c /Rcuxx.so) to your
    projects @e bin directory.

    Scripts that need to use functionality of the Rcu++ plugin should
    put somewhere near the top of the scripit, a line like

    @code
    #uses "Rcuxx" 
    @endcode 

    The first thing to do is to open a connection to an Rcu: 
    
    @code
    int handle = rcuOpen(url, emul, debug) 
    @endcode 

    where @a url is a valid communication protocol URL string, for
    example  
    @verbatim
    fee://localhost/FMD-FEE_0_0_0
    fed://localhost/ztt_dimfedserver:FMD-FEE_0_0_0
    ddl:0:0
    u2f:/dev/altro0 
    @endverbatim 

    (see also the Rcu++ documentation).   @a emul is a boolean flag
    whether to do emulation (for testing and development only) and
    @a debug is a boolean flag whether to turn on debugging symbols.  
    
    The handle returned by @c rcuOpen should be kept, because that
    handle is needed in other functions.  One way to do that, is to
    make a global variable in some initialisation. 
    @code
    addGlobal("rcuHandle", INT_VAR)
    rcuHandle = -1;
    @endcode 

   One can open as many RCU connections as one likes.  Note, that if
   you call @c rcuOpen with the same URL, you will get the same handle
   back.  Since there's no way to ensure that multiple handles to the
   same connenction is valid, one should be careful with this.

   A connection can be closed by calling @c rcuClose passing the
   appropriate handle.  
   @code
   rcuClose(rcuHandle);
   @endcode

   Once a connection has been made, one can use the various functions
   of the Rcu++ plugin.  All functions return an integer status.
   Negative values indicates plugin errors, and other non-null return
   values indicate library errors.  All errors are tracked and can be
   obtained using the normal PVSS functions.  Return values of @e
   getters are always passed by reference, while parameters are passed
   by value. For example:

   @code
   bool pattern, abort, timeout, altro, address, busy;
   unsigned where;
   int ret = rcuReadERRST(rcuHandle, pattern, abort, timeout, 
	                  altro, address, busy, where);
   @endcode 

   @section exa Example

   In the @c test directory, you will find some example panels and 
   possibly scripts. 

   @section status Status

   The RCU interface is fully implemented, except that there's no
   direct access to the memories (IMEM, RMEM, PMEM, DM1, DM2) of the
   RCU.  Access to the STATUS memory is provided via the functions
   @code
       int rcuReadStatusCount(int handle, unsigned& count)
       int rcuReadStatusEntry(int handle, unsigned i, bool& id, 
                              unsigned& w, bool& soft, bool& ans, 
			      bool& sclk, bool& alps, bool& paps, 
			      bool& dc, bool& dv, bool& ac, bool& av, 
			      bool& temp)
   @endcode 

   Note, that @c rcuReadStatusCount @e must be called before calling
   @c rcuReadStatusEntry. 

   The ALTRO and BC interfaces are not defined, except for the
   commands.  What needs to be done, is the handling of parameters and
   return values.  Most importantly, the address handling must be
   done.

   To talk to a particular ALTRO or BC, one must first set the address: 
   @code  
   int ret = altroSetAddress(board, chip, channel);
   int ret = bcSetAddress(board); 
   @endcode 

   Note, that if @a board is set to a negative value, then
   communication is done in broadcast.  Note, that one cannot read
   registers etc. in broadcast.  Also, some registers does not accept
   to be set in broadcast.

   The particular FMDD interface should be defined as a sub-class to
   Bc, and provide the extra functions it need.  Note, that there's no
   real point in recoding the function look-up table - we will just
   append to it.
*/

#error Not for compilation
//
// EOF
//
