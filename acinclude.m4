dnl -*- mode: autoconf -*- 
dnl
dnl
dnl  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk>
dnl
dnl  This library is free software; you can redistribute it and/or
dnl  modify it under the terms of the GNU Lesser General Public License
dnl  as published by the Free Software Foundation; either version 2.1
dnl  of the License, or (at your option) any later version.
dnl
dnl  This library is distributed in the hope that it will be useful,
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
dnl  Lesser General Public License for more details.
dnl
dnl  You should have received a copy of the GNU Lesser General Public
dnl  License along with this library; if not, write to the Free
dnl  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
dnl  02111-1307 USA
dnl
dnl ------------------------------------------------------------------
AC_DEFUN([AC_DEBUG],
[
  AC_REQUIRE([AC_PROG_CC])
  AC_REQUIRE([AC_PROG_CXX])
  AC_MSG_CHECKING(whether to make debug objects)
  AC_ARG_ENABLE(debug,
    [AC_HELP_STRING([--enable-debug],[Enable debugging symbols in objects])])
  if test "x$enable_debug" = "xno" ; then
    CFLAGS=`echo $CFLAGS | sed 's,-g,,'`
    CXXFLAGS=`echo $CXXFLAGS | sed 's,-g,,'`
  else
    case $CXXFLAGS in
    *-g*) ;;
    *)    CXXFLAGS="$CXXFLAGS -g" ;;
    esac
    case $CFLAGS in
    *-g*) ;;
    *)    CFLAGS="$CFLAGS -g" ;;
    esac
  fi
  AC_MSG_RESULT($enable_debug 'CFLAGS=$CFLAGS')
])

dnl ------------------------------------------------------------------
AC_DEFUN([AC_OPTIMIZATION],
[
  AC_REQUIRE([AC_PROG_CC])
  AC_REQUIRE([AC_PROG_CXX])

  AC_ARG_ENABLE(optimization,
    [AC_HELP_STRING([--enable-optimization],[Enable optimization of objects])])

  AC_MSG_CHECKING(for optimiztion level)

  changequote(<<, >>)dnl
  if test "x$enable_optimization" = "xno" ; then
    CFLAGS=`echo   $CFLAGS   | sed 's,-O\([0-9][0-9]*\|\),,'`
    CXXFLAGS=`echo $CXXFLAGS | sed 's,-O\([0-9][0-9]*\|\),,'`
  elif test "x$enable_optimization" = "xyes" ; then
    case $CXXFLAGS in
    *-O*) ;;
    *)    CXXFLAGS="$CXXFLAGS -O2" ;;
    esac
    case $CFLAGS in
    *-O*) ;;
    *)    CFLAGS="$CXXFLAGS -O2" ;;
    esac
  else
    CFLAGS=`echo   $CFLAGS   | sed "s,-O\([0-9][0-9]*\|\),-O$enable_optimization,"`
    CXXFLAGS=`echo $CXXFLAGS | sed "s,-O\([0-9][0-9]*\|\),-O$enable_optimization,"`
  fi
  changequote([, ])dnl
  AC_MSG_RESULT($enable_optimization 'CFLAGS=$CFLAGS')
])

dnl ------------------------------------------------------------------
AC_DEFUN([AC_PROFILING],
[
  AC_REQUIRE([AC_PROG_CC])
  AC_REQUIRE([AC_PROG_CXX])
	
  AC_ARG_ENABLE([profiling],
	        [AC_HELP_STRING([--enable-profiling],
			        [Compile code to enable profiling])],
                [],[enable_profiling=no])
  AC_MSG_CHECKING([whether to enable profiling])
  if test "x$enable_profiling" = "xyes" ; then 
    CFLAGS="$CFLAGS -pg" 
    CXXFLAGS="$CXXFLAGS -pg"
    LDFLAGS="$LDFLAGS -pg"
  fi
  AC_MSG_RESULT([$enable_profiling])
])

dnl ------------------------------------------------------------------
AC_DEFUN([AC_STRICT],
[
  AC_REQUIRE([AC_PROG_CC])
  AC_REQUIRE([AC_PROG_CXX])
	
  AC_ARG_ENABLE([strict],
	        [AC_HELP_STRING([--enable-strict],
			        [Require strictly correct code])],
                [],[enable_strict=no])
  AC_MSG_CHECKING([whether require strictly correct code])
  if test "x$enable_strict" = "xyes" ; then 
    CFLAGS="$CFLAGS -Wall -Werror -pedantic -ansi" 
    # Cannot use `-pedantic' due to use of `long long'
    CXXFLAGS="$CXXFLAGS -Wall -Werror -ansi"
  fi
  AC_MSG_RESULT([$enable_strict ($CFLAGS)])
])
  
dnl __________________________________________________________________
dnl
dnl AC_RCUXX([MINIMUM-VERSION 
dnl             [,ACTION-IF_FOUND 
dnl              [, ACTION-IF-NOT-FOUND]]])
AC_DEFUN([AC_RCUXX],
[
    # Command line argument to specify prefix. 
    AC_ARG_WITH([rcuxx],
        [AC_HELP_STRING([--with-rcuxx],	[Prefix where Rcu++ is installed])],
	[],[with_rcuxx="yes"])

    # Command line argument to specify documentation URL. 
    AC_ARG_WITH([rcuxx-url],
        [AC_HELP_STRING([--with-rcuxx-url],
		[Base URL where the Rcu++ dodumentation is installed])],
        rcuxx_url=$withval, rcuxx_url="")
    if test "x${RCUXX_CONFIG+set}" != xset ; then 
        if test "x$with_rcuxx" != "xno" ; then 
	    RCUXX_CONFIG=$with_rcuxx/bin/rcuxx-config
	fi
    fi   
	
    # Check for the configuration script. 
    if test "x$with_rcuxx" != "xno" ; then 
        AC_PATH_PROG(RCUXX_CONFIG, rcuxx-config, no)
        rcuxx_min_version=ifelse([$1], ,0.3,$1)
        # Message to user
        AC_MSG_CHECKING(for Rcu++ version >= $rcuxx_min_version)

        # Check if we got the script
        with_rcuxx=no    
        if test "x$RCUXX_CONFIG" != "xno" ; then 
           # If we found the script, set some variables 
           RCUXX_CPPFLAGS=`$RCUXX_CONFIG --cppflags`
           RCUXX_INCLUDEDIR=`$RCUXX_CONFIG --includedir`
           RCUXX_LIBS=`$RCUXX_CONFIG --libs`
           RCUXX_LTLIBS=`$RCUXX_CONFIG --ltlibs`
           RCUXX_LIBDIR=`$RCUXX_CONFIG --libdir`
           RCUXX_LDFLAGS=`$RCUXX_CONFIG --ldflags`
           RCUXX_LTLDFLAGS=`$RCUXX_CONFIG --ltldflags`
           RCUXX_PREFIX=`$RCUXX_CONFIG --prefix`
           
           # Check the version number is OK.
           rcuxx_version=`$RCUXX_CONFIG -V` 
           rcuxx_vers=`echo $rcuxx_version | \
             awk 'BEGIN { FS = "."; } \
    	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
           rcuxx_regu=`echo $rcuxx_min_version | \
             awk 'BEGIN { FS = "."; } \
    	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
           if test $rcuxx_vers -ge $rcuxx_regu ; then 
                with_rcuxx=yes
           fi
        fi
        AC_MSG_RESULT($with_rcuxx - is $rcuxx_version) 
    
        # Some autoheader templates. 
        AH_TEMPLATE(HAVE_RCUXX, [Whether we have rcuxx])
    
    
        if test "x$with_rcuxx" = "xyes" ; then
            # Now do a check whether we can use the found code. 
            save_LDFLAGS=$LDFLAGS
    	    save_CPPFLAGS=$CPPFLAGS
            LDFLAGS="$LDFLAGS $RCUXX_LDFLAGS"
            CPPFLAGS="$CPPFLAGS $RCUXX_CPPFLAGS"
     
            # Change the language 
            AC_LANG_PUSH(C++)
    
     	    # Check for a header 
            have_rcuxx_rcu_h=0
            AC_CHECK_HEADER([rcuxx/Rcu.h], [have_rcuxx_rcu_h=1])
    
            # Check the library. 
            have_librcuxx=no
            AC_MSG_CHECKING(for -lrcuxx)
            AC_LINK_IFELSE([AC_LANG_PROGRAM([#include <rcuxx/Rcu.h>],
                                            [Rcuxx::Rcu::Open("foo")])], 
                                            [have_librcuxx=yes])
            AC_MSG_RESULT($have_librcuxx)
    
            if test $have_rcuxx_rcu_h -gt 0    && \
                test "x$have_librcuxx"   = "xyes" ; then
    
                # Define some macros
                AC_DEFINE(HAVE_RCUXX)
            else 
                with_rcuxx=no
            fi
            # Change the language 
            AC_LANG_POP(C++)
    	CPPFLAGS=$save_CPPFLAGS
    	LDFLAGS=$save_LDFLAGS
        fi
    
        AC_MSG_CHECKING(where the Rcu++ documentation is installed)
        if test "x$rcuxx_url" = "x" && \
    	test ! "x$RCUXX_PREFIX" = "x" ; then 
           RCUXX_URL=${RCUXX_PREFIX}/share/doc/rcuxx/html
        else 
    	RCUXX_URL=$rcuxx_url
        fi	
        AC_MSG_RESULT($RCUXX_URL)
    fi
   
    if test "x$with_rcuxx" = "xyes" ; then 
        ifelse([$2], , :, [$2])
    else 
        ifelse([$3], , :, [$3])
    fi
    AC_SUBST(RCUXX_URL)
    AC_SUBST(RCUXX_PREFIX)
    AC_SUBST(RCUXX_CPPFLAGS)
    AC_SUBST(RCUXX_INCLUDEDIR)
    AC_SUBST(RCUXX_LDFLAGS)
    AC_SUBST(RCUXX_LIBDIR)
    AC_SUBST(RCUXX_LIBS)
    AC_SUBST(RCUXX_LTLIBS)
    AC_SUBST(RCUXX_LTLDFLAGS)
])
dnl
dnl EOF
dnl 


dnl __________________________________________________________________
dnl
dnl AC_PVSS([TYPE], [ACTION-IF-FOUND], [ACTION-IF-NOT-FOUND])
dnl Check for PVSS
dnl
AC_DEFUN([AC_PVSS],
[
  AC_ARG_WITH(pvss-prefix,
              [AC_HELP_STRING([--with-pvss-prefix],
   	                      [PVSS installation prefix $pvss_prefix])],
              [pvss_prefix=$with_pvss_prefix], 
   	      [pvss_prefix=/opt/pvss/pvss2_v3.6])
  # check if the directory exists at all 
  if test ! -d $pvss_prefix ; then 
    AC_MSG_WARN([The directory $pvss_prefix does not exist])
    pvss_prefix=no
  fi 
  if test ! -d $pvss_prefix/api ; then 
    AC_MSG_WARN([The directory $pvss_prefix/api does not exist])
    pvss_prefix=no
  fi 

  if test ! "x$pvss_prefix" = "xno" ; then
    _PVSS_PLATFORM
    _PVSS_SYS_DEFINES
    _PVSS_OS_DEFINES
    _PVSS_COMPILER_DEFINES
    _PVSS_GLIBC
    _PVSS_GSSAPI_KRB

    AC_MSG_CHECKING(for library main and release version numbers)
    pvss_basics=`ls $pvss_prefix/api/lib.$PVSS_PLATFORM/libBasics*.so`
    pvss_basics=`basename $pvss_basics .so`
    pvss_lversion=`echo $pvss_basics | sed -e 's/libBasicsV//' -e 's/_.*//'`
    pvss_lrevision=`echo $pvss_basics | sed -e 's/.*libBasicsV.*_//'`
    AC_MSG_RESULT([$pvss_lversion and $pvss_lrevision])

    # Set the include path for PVSS 
    PVSS_PREFIX=$pvss_prefix
    PVSS_INCDIR=$pvss_prefix/api/include
    PVSS_LIBDIR="$pvss_prefix/api/lib.$PVSS_PLATFORM"
    PVSS_DLL_VERSMAIN=V$pvss_lversion
    PVSS_DLL_VERS=${PVSS_DLL_VERSMAIN}_$pvss_lrevision
    PVSS_CPPFLAGS="-I$PVSS_INCDIR $KRB5_CPPFLAGS"
    PVSS_LDFLAGS="-L$PVSS_LIBDIR $KRB5_LDFLAGS"

    case $PVSS_PLATFORM in 
    solaris*) PVSS_LDFLAGS="$PVSS_LDFLAGS -lnsl -ldl -lsocket -lposix4" ;;
    linux*)   PVSS_LDFLAGS="$PVSS_LDFLAGS -ldl -pthread" ;;
    esac

    AC_DEFINE(ADDVERINFO_HXX, <Basics/Utilities/addVerInfo.hxx>, 
              [Version info class])
    PVSS_MAN_CPPFLAGS="-I$PVSS_INCDIR/Basics/Variables	\
                       -I$PVSS_INCDIR/Basics/Utilities	\
                       -I$PVSS_INCDIR/Basics/NoPosix	\
                       -I$PVSS_INCDIR/Basics/DpBasics	\
                       -I$PVSS_INCDIR/BCMNew		\
                       -I$PVSS_INCDIR/Configs		\
                       -I$PVSS_INCDIR/Datapoint		\
                       -I$PVSS_INCDIR/Messages		\
                       -I$PVSS_INCDIR/Manager"
    PVSS_MAN_LIBS="$KRB5_LIBS				\
		   -lManager$PVSS_DLL_VERS		\
                   -lMessages$PVSS_DLL_VERS		\
                   -lDatapoint$PVSS_DLL_VERS		\
                   -lBasics$PVSS_DLL_VERS		\
                   -lbcm$PVSS_DLL_VERS			\
                   -lPVSSUtil$PVSS_DLL_VERSMAIN		\
	           -lz"
    if test $pvss_lversion -lt 37 ; then 		   
        PVSS_MAN_OBJS="$PVSS_LIBDIR/DpConfig.o		\
	               $PVSS_LIBDIR/DpConfigManager.o"
    fi

    PVSS_COMDRV_CPPFLAGS="$PVSS_MAN_CPPFLAGS		\
			  -I$PVSS_INCDIR/Configs/DrvConfigs/DrvCommon  \
                          -I$PVSS_INCDIR/Configs/DrvConfigs/ConvSmooth \
                          -I$PVSS_INCDIR/V24 -I$PVSS_INCDIR/ComDrv"
    PVSS_COMDRV_LIBS="$PVSS_MAN_LIBS -lComDrv -lV24 -lConfigs$PVSS_DLL_VERS"
    
    PVSS_CTRL_CPPFLAGS="$PVSS_MAN_CPPFLAGS -I$PVSS_INCDIR/Ctrl"
    PVSS_CTRL_LIBS="$PVSS_MAN_LIBS -lCtrl$PVSS_DLL_VERS"

  fi

  AC_SUBST(PVSS_PREFIX)
  AC_SUBST(PVSS_INCDIR)
  AC_SUBST(PVSS_LIBDIR)
  AC_SUBST(PVSS_CPPFLAGS)
  AC_SUBST(PVSS_LDFLAGS)
  AC_SUBST(PVSS_PLATFORM)
  AC_SUBST(PVSS_MAN_CPPFLAGS)
  AC_SUBST(PVSS_MAN_LIBS)
  AC_SUBST(PVSS_MAN_OBJS)
  AC_SUBST(PVSS_COMDRV_CPPFLAGS)
  AC_SUBST(PVSS_COMDRV_LIBS)
  AC_SUBST(PVSS_CTRL_CPPFLAGS)
  AC_SUBST(PVSS_CTRL_LIBS)

  AC_MSG_CHECKING(whether we have PVSS API)
  if test "x$pvss_prefix" = "xno" ; then 
    AC_MSG_RESULT(no)
    ifelse([$2], , :, [$2])
  else 
    AC_MSG_RESULT(yes)
    ifelse([$1], , :, [$1])
  fi
])

dnl __________________________________________________________________
AC_DEFUN([_PVSS_SYS_DEFINES],[
  # Check if we can find the platform that PVSS needs 
  AC_REQUIRE([AC_PROG_CXX])
  AC_LANG_PUSH(C++)
  dnl Checking for Posix
  AC_MSG_CHECKING([whether we are on a POSIX platform])
  AH_TEMPLATE([__UNIX],   [Unix platform])
  AC_PREPROC_IFELSE([
#include <cstdlib>
#ifndef _POSIX_SOURCE
# error not a posix platform
#endif
], [AC_DEFINE(__UNIX) 
    AC_MSG_RESULT(yes)], 
   [AC_MSG_RESULT(no)])

  dnl Checking for BSD
  AC_MSG_CHECKING([whether we are on a BSD compatible platform])
  AH_TEMPLATE([BSD_COMP],   [BSD platform])
  AC_PREPROC_IFELSE([
#include <cstdlib>
#ifndef _BSD_SOURCE
# error not a BSD compatible platform
#endif
], [AC_DEFINE(BSD_COMP)
    AC_MSG_RESULT(yes)], 
   [AC_MSG_RESULT(no)])

  dnl Checking for GCC3
  AC_MSG_CHECKING([whether we are using GCC3 or better])
  AH_TEMPLATE([GCC3],   [GCC version 3 or better])
  AC_PREPROC_IFELSE([
#include <cstdlib>
#ifndef __GNUC__ || __GNUC__ < 2
# error not a gcc 3 or better 
#endif
], [AC_DEFINE(GCC3)
    AC_MSG_RESULT(yes)], 
   [AC_MSG_RESULT(no)])
  AC_LANG_POP(C++)

    
  dnl Check if this is on a PC (i386) 
  case $host in 
  *i386*) AC_DEFINE(__PC, 1, [We are on an Intel like machine]) ;; 
  *i486*) AC_DEFINE(__PC, 1, [We are on an Intel like machine]) ;; 
  *i586*) AC_DEFINE(__PC, 1, [We are on an Intel like machine]) ;; 
  *i686*) AC_DEFINE(__PC, 1, [We are on an Intel like machine]) ;; 
  *)
  esac 
])

dnl __________________________________________________________________
AC_DEFUN([_PVSS_PLATFORM],[
  # Check if we can find the platform that PVSS needs 
  AC_MSG_CHECKING([for PVSS platform])    
  if test -f $pvss_prefix/api/platform && \
     test $pvss_prefix/api/platform > /dev/null 2>&1 ; then 
    PVSS_PLATFORM=`$pvss_prefix/api/platform 2>/dev/null` 
  fi
  if test "x$PVSS_PLATFORM" = "x" ; then 
    case $host in 
    *hpux9)     PVSS_PLATFORM=hpux9    ;; 
    *hpux10)    PVSS_PLATFORM=hpux10   ;; 
    *hpux*)     PVSS_PLATFORM=hpux     ;; 
    *solaris2*) PVSS_PLATFORM=solaris9 ;;
    *windows*)  PVSS_PLATFORM=windows  ;;
    *linux*)    
      if test "x$GXX" = "xyes" ; then 
        PLATFORM=linux_gcc32
	gcc_version=`$CXX -dumpversion 2>/dev/null`
	case $CXX:$gcc_version in 
	  *g++32:3.2*) PVSS_PLATFORM=linux_RHEL4 ;;
	  *:3.2*)      PVSS_PLATFORM=linux_RH90  ;; 
	  *:*)    
            if test `$CXX -V 3.2 --version > /dev/null 2>&1` ; then 
	      CXXFLAGS="$CXXFLAGS -V 3.2"
              PVSS_PLATFORM=linux_RH90
            elif test `g++32 --version > /dev/null 2>&1` ; then 
	      pvss_error_rerun=yes
	    else 
	      pvss_warn_gcc=yes
	      PVSS_PLATFORM=linux
            fi
          ;;
        esac
      fi
      ;;
    esac
  fi
  AC_MSG_RESULT([$PVSS_PLATFORM])
  if test "x$pvss_warn_gcc" = "xyes" ; then 
    AC_MSG_WARN([GCC $gcc_version may not be work])
  fi
  if test "X$pvss_error_rerun" = x"yes" ; then 
    AC_MSG_ERROR([Please re-run with CXX=g++32])
  fi
])

dnl __________________________________________________________________
AC_DEFUN([_PVSS_OS_DEFINES],[
  AC_REQUIRE([_PVSS_PLATFORM])

  case $PVSS_PLATFORM in 
  hpux*) ;;
  solaris*)
    AC_DEFINE([OS_SOLARIS],  1, [We're on Solaris])
    AC_DEFINE([OS_SOLARIS9], 1, [We're on Solaris 9])
    AC_DEFINE([OS_SYSV],     4, [Version 4 of SYSV])
    ;;
  linux*)
    dnl Check kernel version 
    KERNEL_VERS=`uname -r 2>/dev/null` 
    case $KERNEL_VERS in 
    2.4*)  AC_DEFINE(LINUX2_4,1,Linux kernel 2.4) ;; 
    2.6*)  AC_DEFINE(LINUX2_6,1,Linux kernel 2.6) ;; 
    esac
    AC_DEFINE([LINUX],    1, [We're on Linux])
    AC_DEFINE([LINUX2],   1, [We're on Linux])
    AC_DEFINE([OS_LINUX], 1, [We're on Linux])
    ;; 
  esac
])

dnl __________________________________________________________________
AC_DEFUN([_PVSS_COMPILER_DEFINES],[
  AC_DEFINE([DLLEXP_BASICS],      [],[DLL export for Basic])
  AC_DEFINE([DLLEXP_CONFIGS],     [],[DLL export for Configs])
  AC_DEFINE([DLLEXP_DATAPOINT],   [],[DLL export for Datapoint])
  AC_DEFINE([DLLEXP_MESSAGES],    [],[DLL export for Messages])
  AC_DEFINE([DLLEXP_MANAGER],     [],[DLL export for Manager])
  AC_DEFINE([DLLEXP_CTRL],        [],[DLL export for CTRL])
  AC_DEFINE([BC_DESTRUCTOR],      1, [Use BC destructor])
  AC_DEFINE([bcm_boolean_h],      1, [Use of bcm boolan])
  AC_DEFINE([_REENTRANT],         1, [Reentrant sys functions ])
  AC_DEFINE([USES_NEW_IOSTREAMS], 1, [ISO standard-style includes])
  AC_DEFINE([HAS_TEMPLATES],      1, [Can do templates])
])

dnl __________________________________________________________________
AC_DEFUN([_PVSS_GLIBC],
[
  dnl Check GLibC version 
  AC_REQUIRE([AC_PROG_CXX])
  AC_LANG_PUSH(C++)
  AC_MSG_CHECKING(for GNU Libc version)
  AC_COMPILE_IFELSE([
#include <cstdio>
int main() {
#if !defined(__GLIBC__) || __GLIBC__ < 2
# error Not GNU LibC or very old 
#else 
# if __GLIBC__ == 2 && __GLIBC_MINOR__ == 2
#  /* OK */
# else 
chokeme
# endif
#endif
return 0;
}
], [AC_DEFINE([GLIBC2_2], 1, [Using GLIBC 2.2])
    glibc=2.2],[AC_COMPILE_IFELSE([
#include <cstdio>
int main() {
#if !defined(__GLIBC__) || __GLIBC__ < 2
# error Not GNU LibC or very old 
#else 
# if __GLIBC__ == 2 && __GLIBC_MINOR__ > 2
#  /* OK */
# else 
chokeme
# endif
#endif
return 0;
}
], [AC_DEFINE([GLIBC2_3], 1, [Using GLIBC 2.3 or better])
    glibc="2.3+"],
   [glibc="unkkown"])])
  AC_MSG_RESULT([$glibc])
  if test "x$glibc" = "xunknown" ; then 
    AC_MSG_WARN([Couldn't determine version of GNU Libc])
  fi
  AC_LANG_POP(C++)
])

dnl __________________________________________________________________
AC_DEFUN([_PVSS_GSSAPI_KRB],
[
  AC_ARG_WITH([krb5-prefix],
              [AC_HELP_STRING([--with-krb5-prefix],
                              [Prefix of Kerberos installation])])
  save_CPPFLAGS="$CPPFLAGS"
  save_LDFLAGS="$LDFLAGS"
  save_LIBS="$LIBS" 
  for i in "$with_krb5_prefix" /usr/kerberos ; do 
    if test ! "x$i" = "x" ; then 
      CPPFLAGS="$save_CPPFLAGS -I$i/include"
      LDFLAGS="$save_LDFLAGS -L$i/lib"
    fi  
    found_krb5=yes
    unset ac_cv_lib_gssapi_krb5_gss_acquire_cred
    AC_CHECK_LIB([gssapi_krb5],[gss_acquire_cred],
                 [], [found_krb5=no])
    unset ac_cv_header_gssapi_gssapi_h
    AC_CHECK_HEADER([gssapi/gssapi.h], [],[found_krb5=no])
    if test "x$found_krb5" = "xyes" ; then 
      KRB5_LIBS="-lgssapi_krb5"
      if test ! "x$i" = "x" ; then 
	KRB5_LDFLAGS="-L$i/lib" 
	KRB5_CPPFLAGS="-L$i/include"
      fi
      break
    fi
  done 
  if test "x$found_krb5" = "xno" ; then 
    AC_MSG_WARN([GSS Api for Kerberos not found])
  fi
  LIBS="$save_LIBS"
  LDFLAGS="$save_LDLFAGS"
  CPPFLAGS="$save_CPPLFAGS"
])


dnl
dnl EOF
dnl
