//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    pvss-rcuxx/Rcu.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of RCU interface 
*/
#include <stdexcept>
#include "Rcu.h"
#include "Bc.h"
#include "Fmd.h"
#include "Altro.h"
#include "Util.h"
#include <rcuxx/Rcu.h>
#include <rcuxx/Altro.h>
#include <rcuxx/Bc.h>
#include <rcuxx/Fmd.h>
#include <rcuxx/rcu/RcuACTFEC.h>
#include <rcuxx/rcu/RcuALTROCFG1.h>
#include <rcuxx/rcu/RcuALTROCFG2.h>
#include <rcuxx/rcu/RcuALTROIF.h>
#include <rcuxx/rcu/RcuBPVERS.h>
#include <rcuxx/rcu/RcuCHADD.h>
#include <rcuxx/rcu/RcuCommand.h>
#include <rcuxx/rcu/RcuCounter.h>
#include <rcuxx/rcu/RcuERRREG.h>
#include <rcuxx/rcu/RcuERRST.h>
#include <rcuxx/rcu/RcuEVWORD.h>
#include <rcuxx/rcu/RcuFECERR.h>
#include <rcuxx/rcu/RcuFMIREG.h>
#include <rcuxx/rcu/RcuFMOREG.h>
#include <rcuxx/rcu/RcuFWVERS.h>
#include <rcuxx/rcu/RcuINTMOD.h>
#include <rcuxx/rcu/RcuINTREG.h>
#include <rcuxx/rcu/RcuIRADD.h>
#include <rcuxx/rcu/RcuIRDAT.h>
#include <rcuxx/rcu/RcuTTCControl.h>	// Trigger control register (0x4000)
#include <rcuxx/rcu/RcuROIConfig1.h>	// Region of interest configuration 
#include <rcuxx/rcu/RcuROIConfig2.h>	// Region of interest configuration 
#include <rcuxx/rcu/RcuL1Timeout.h>	// L1 timeout and window (0x4006)
#include <rcuxx/rcu/RcuL2Timeout.h>	// L2 time-out (0x4007)
#include <rcuxx/rcu/RcuRoiTimeout.h>	// ROI message timeout (0x4009)
#include <rcuxx/rcu/RcuL1MsgTimeout.h>	// L1 message timeout (0x400A)
#include <rcuxx/rcu/RcuTTCEventInfo.h>	// Event information (0x4028)
#include <rcuxx/rcu/RcuTTCEventError.h>	// Event information (0x4029)
#include <rcuxx/rcu/RcuLWADD.h>
#include <rcuxx/rcu/RcuPMCFG.h>
#include <rcuxx/rcu/RcuRCUBUS.h>
#include <rcuxx/rcu/RcuRDOERR.h>
#include <rcuxx/rcu/RcuRDOFEC.h>
#include <rcuxx/rcu/RcuRDOMOD.h>
#include <rcuxx/rcu/RcuRegister.h>
#include <rcuxx/rcu/RcuRESREG.h>
#include <rcuxx/rcu/RcuSCADD.h>
#include <rcuxx/rcu/RcuSCDAT.h>
#include <rcuxx/rcu/RcuStatusEntry.h>
#include <rcuxx/rcu/RcuTRCFG1.h>
#include <rcuxx/rcu/RcuTRCFG2.h>
#include <rcuxx/rcu/RcuTRGCONF.h>
#include <rcuxx/rcu/RcuTRCNT.h>
#include <rcuxx/rcu/RcuIMEM.h>
#include <rcuxx/rcu/RcuPMEM.h>
#include <rcuxx/rcu/RcuRMEM.h>
#include <rcuxx/rcu/RcuDMEM.h>
#include <rcuxx/rcu/RcuACL.h>
#include <rcuxx/rcu/RcuSTATUS.h>
#include <rcuxx/rcu/RcuHEADER.h>
namespace {
#ifdef NO_CHECK
  bool fgDebug = true;
#else
  bool fgDebug = false;
#endif
}

//____________________________________________________________________
Pvss::Rcu::Rcu(Rcuxx::Rcu* rcu) 
  : fRcu(rcu), 
    fAltro(0), 
    fBc(0)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::Rcu(%p)", rcu);
}

//____________________________________________________________________
Pvss::Rcu::~Rcu()
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::~Rcu()");
  if (fAltro) delete fAltro;
  if (fBc)    delete fBc;
  if (fRcu)   delete fRcu;
}

//____________________________________________________________________
const Variable* 
Pvss::Rcu::execute(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, 
		      "Rcu::execute [# %d -> name: %s, # args %d]", 
		      param.funcNum, param.funcName.c_str(), 
		      (param.args ? param.args->getNumberOfItems() : 0));

  static IntegerVar ret;
  ret.setValue(-1);
  if (!fRcu) throw std::runtime_error("No RCU connecton open");
  try {
    switch (param.funcNum) {
      // Internal commands 
    case kAddAltro:   		return AddAltro(param);
    case kAddBc:      		return AddBc(param);
      // Execute commands 
    case kExecABORT:		return ExecABORT(param);
    case kExecCLR_EVTAG:	return ExecCLR_EVTAG(param);
    case kExecDCS_ON:		return ExecDCS_ON(param);
    case kExecDDL_ON:		return ExecDDL_ON(param);
    case kExecEXEC:		return ExecEXEC(param);
    case kExecFECRST:		return ExecFECRST(param);
    case kExecGLB_RESET:	return ExecGLB_RESET(param);
    case kExecL1_CMD:		return ExecL1_CMD(param);
    case kExecL1_I2C:		return ExecL1_I2C(param);
    case kExecL1_TTC:		return ExecL1_TTC(param);
    case kExecRCU_RESET:	return ExecRCU_RESET(param);
    case kExecRDABORT:		return ExecRDABORT(param);
    case kExecRDFM:		return ExecRDFM(param);
    case kExecRS_DMEM1:		return ExecRS_DMEM1(param);
    case kExecRS_DMEM2:		return ExecRS_DMEM2(param);
    case kExecRS_STATUS:	return ExecRS_STATUS(param);
    case kExecRS_TRCFG:		return ExecRS_TRCFG(param);
    case kExecRS_TRCNT:		return ExecRS_TRCNT(param);
    case kExecSCCOMMAND:	return ExecSCCOMMAND(param);
    case kExecSWTRG:		return ExecSWTRG(param);
    case kExecTRG_CLR:		return ExecTRG_CLR(param);
    case kExecWRFM:		return ExecWRFM(param);
    case kExecRS_ERRREG:	return ExecRS_ERRREG(param);
      /** Register read access */       
    case kReadERRST:		return ReadERRST(param);
    case kReadTRCNT:		return ReadTRCNT(param);
    case kReadLWADD:		return ReadLWADD(param);
    case kReadIRADD:		return ReadIRADD(param);
    case kReadIRDAT:		return ReadIRDAT(param);
    case kReadEVWORD:		return ReadEVWORD(param);
    case kReadACTFEC:		return ReadACTFEC(param);
    case kReadRDOFEC:		return ReadRDOFEC(param);
    case kReadTRCFG1:		return ReadTRCFG1(param);
    case kReadTRCFG2:		return ReadTRCFG2(param);
    case kReadFMIREG:		return ReadFMIREG(param);
    case kReadFMOREG:		return ReadFMOREG(param);
    case kReadPMCFG:		return ReadPMCFG(param);
    case kReadCHADD:		return ReadCHADD(param);
    case kReadINTREG:		return ReadINTREG(param);
    case kReadRESREG:		return ReadRESREG(param);
    case kReadERRREG:		return ReadERRREG(param);
    case kReadINTMOD:		return ReadINTMOD(param);
    case kReadStatusCount:  	return ReadStatusCount(param);
    case kReadStatusEntry:  	return ReadStatusEntry(param);
    case kReadACL:		return ReadACL(param);
    case kReadIMEM:		return ReadIMEM(param);
    case kReadPMEM:		return ReadPMEM(param);
      /** Register write access */       
    case kWriteACTFEC:		return WriteACTFEC(param);
    case kWriteTRCFG1:		return WriteTRCFG1(param);
    case kWriteTRCFG2:		return WriteTRCFG2(param);
    case kWriteFMIREG:		return WriteFMIREG(param);
    case kWriteFMOREG:		return WriteFMOREG(param);
    case kWritePMCFG:		return WritePMCFG(param);
    case kWriteRESREG:		return WriteRESREG(param);
    case kWriteINTMOD:		return WriteINTMOD(param);
    case kWriteACL:		return WriteACL(param);
    case kWriteIMEM:		return WriteIMEM(param);
    case kWritePMEM:		return WritePMEM(param);
    case kZeroACL:		return ZeroACL(param);
    case kZeroIMEM:		return ZeroIMEM(param);
    case kZeroPMEM:		return ZeroPMEM(param);
      // Sizes of memories 
    case kSizeOfACL:		return SizeOfACL(param);
    case kSizeOfIMEM:		return SizeOfIMEM(param);
    case kSizeOfPMEM:		return SizeOfPMEM(param);
    }
#ifndef NO_CHECK 
    if (!fAltro) fAltro = new Altro(new Rcuxx::Altro(*fRcu));
    if (!fBc)    fBc    = new Bc(new Rcuxx::Bc(*fRcu));
#endif
    if (param.funcNum < Altro::kBcBase && fAltro) 
      return fAltro->execute(param);
    else if (param.funcNum >= Altro::kBcBase && fBc) 
      return fBc->execute(param);
  }
  catch (unsigned& e) {
    ErrClass err(ErrClass::PRIO_WARNING, ErrClass::ERR_CONTROL, 
		 ErrClass::UNEXPECTEDSTATE, 
		 "Rcuxx", "execute", fRcu->ErrorString(e).c_str());
    param.thread->appendLastError(&err);
    if (fgDebug) 
      std::cerr << "Rcuxx exception: " << fRcu->ErrorString(e) << std::endl;
    ret.setValue(e);
  }
  catch (...) {
    throw; // Re-throw other errors 
  }
  return &ret;
}

//____________________________________________________________________
const IntegerVar* 
Pvss::Rcu::AddAltro(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::AddAltro()");
  CHECK_CALL(param, kAddAltro, "rcuAddAltro");
  static IntegerVar ret;
  if (!fAltro) {
    Rcuxx::Altro* altro = new Rcuxx::Altro(*fRcu);
    if (altro)    fAltro = new Altro(altro);
  }
  ret.setValue(fAltro ? 0 : 1);
  return &ret;
}

//____________________________________________________________________
const IntegerVar* 
Pvss::Rcu::AddBc(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::AddBc()");
  CHECK_CALL(param, kAddBc, "rcuAddBc");
  static IntegerVar ret;
  Rcuxx::DebugGuard::Message(fgDebug, "fBC = %p", fBc);
  if (!fBc) {
    IntegerVar type;
    Rcuxx::DebugGuard::Message(fgDebug, "Checking parameter");
    CheckArg(type, param.args->getNext(), param.thread);
    Rcuxx::Bc* bc = 0;
    Rcuxx::DebugGuard::Message(fgDebug, "Type of BC=%d", type.getValue());
    switch (type.getValue()) {
    case 0:  bc = new Rcuxx::Bc(*fRcu);  break;
    case 1:  bc = new Rcuxx::Fmd(*fRcu); break;
    default: bc = 0;
    }
    Rcuxx::DebugGuard::Message(fgDebug, "Low level BC: %p", bc);
    if (bc) {
      Rcuxx::DebugGuard::Message(fgDebug, "Creating BC interface");
      switch (type.getValue()) {
      case 0:  fBc = new Bc(bc);  break;
      case 1:  fBc = new Fmd(bc); break;
      default: fBc = 0;
      }
      Rcuxx::DebugGuard::Message(fgDebug, "made BC interface %p", fBc);
    }
  }
  Rcuxx::DebugGuard::Message(fgDebug, "BC interface %p", fBc);
  ret.setValue(fBc ? 0 : 1);
  return &ret;
}

//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::ExecABORT(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::ExecABORT()");
  CHECK_CALL(param, kExecABORT, "rcuExecABORT");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuCommand* abort = fRcu->ABORT();
  if (!abort) throw interface_missing("ABORT", "RCU");

  int r = abort->Commit();
  if (r != 0) throw r;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::ExecCLR_EVTAG(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::ExecCLR_EVTAG()");
  CHECK_CALL(param, kExecCLR_EVTAG, "rcuExecCLR_EVTAG");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuCommand* clr_evtag = fRcu->CLR_EVTAG();
  if (!clr_evtag) throw interface_missing("CLR_EVTAG", "RCU");

  int r = clr_evtag->Commit();
  if (r != 0) throw r;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::ExecDCS_ON(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::ExecDCS_ON()");
  CHECK_CALL(param, kExecDCS_ON, "rcuExecDCS_ON");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuCommand* dcs_on = fRcu->DCS_ON();
  if (!dcs_on) throw interface_missing("DCS_ON", "RCU");

  int r = dcs_on->Commit();
  if (r != 0) throw r;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::ExecDDL_ON(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::ExecDDL_ON()");
  CHECK_CALL(param, kExecDDL_ON, "rcuExecDDL_ON");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuCommand* ddl_on = fRcu->DDL_ON();
  if (!ddl_on) throw interface_missing("DDL_ON", "RCU");

  int r = ddl_on->Commit();
  if (r != 0) throw r;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::ExecEXEC(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::ExecEXEC()");
  CHECK_CALL(param, kExecEXEC, "rcuExecEXEC");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuCommand* exec = fRcu->EXEC();
  if (!exec) throw interface_missing("EXEC", "RCU");

  UIntegerVar addr;
  CheckArg(addr, param.args->getNext(), param.thread);
  exec->SetArgument(addr.getValue());
  int r = exec->Commit();
  if (r != 0) throw r;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::ExecFECRST(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::ExecFECRST()");
  CHECK_CALL(param, kExecFECRST, "rcuExecFECRST");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuCommand* fecrst = fRcu->FECRST();
  if (!fecrst) throw interface_missing("FECRST", "RCU");

  int r = fecrst->Commit();
  if (r != 0) throw r;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::ExecGLB_RESET(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::ExecGLB_RESET()");
  CHECK_CALL(param, kExecGLB_RESET, "rcuExecGLB_RESET");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuCommand* glb_reset = fRcu->GLB_RESET();
  if (!glb_reset) throw interface_missing("GLB_RESET", "RCU");

  int r = glb_reset->Commit();
  if (r != 0) throw r;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::ExecL1_CMD(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::ExecL1_CMD()");
  CHECK_CALL(param, kExecL1_CMD, "rcuExecL1_CMD");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuCommand* l1_cmd = fRcu->L1_CMD();
  if (!l1_cmd) throw interface_missing("L1_CMD", "RCU");

  int r = l1_cmd->Commit();
  if (r != 0) throw r;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::ExecL1_I2C(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::ExecL1_I2C()");
  CHECK_CALL(param, kExecL1_I2C, "rcuExecL1_I2C");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuCommand* l1_i2c = fRcu->L1_I2C(); // 
  if (!l1_i2c) throw interface_missing("L1_I2C", "RCU");

  int r = l1_i2c->Commit();
  if (r != 0) throw r;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::ExecL1_TTC(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::ExecL1_TTC()");
  CHECK_CALL(param, kExecL1_TTC, "rcuExecL1_TTC");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuCommand* l1_ttc = fRcu->L1_TTC();
  if (!l1_ttc) throw interface_missing("L1_TTC", "RCU");

  int r = l1_ttc->Commit();
  if (r != 0) throw r;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::ExecRCU_RESET(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::ExecRCU_RESET()");
  CHECK_CALL(param, kExecRCU_RESET, "rcuExecRCU_RESET");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuCommand* rcu_reset = fRcu->RCU_RESET();
  if (!rcu_reset) throw interface_missing("RCU_RESET", "RCU");
  
  int r = rcu_reset->Commit();
  if (r != 0) throw r;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::ExecRDABORT(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::ExecRDABORT()");
  CHECK_CALL(param, kExecRDABORT, "rcuExecRDABORT");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuCommand* rdabort = fRcu->RDABORT();
  if (!rdabort) throw interface_missing("RDABORT", "RCU");

  int r = rdabort->Commit();
  if (r != 0) throw r;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::ExecRDFM(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::ExecRDFM()");
  CHECK_CALL(param, kExecRDFM, "rcuExecRDFM");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuCommand* rdfm = fRcu->RDFM();
  if (!rdfm) throw interface_missing("RDFM", "RCU");

  int r = rdfm->Commit();
  if (r != 0) throw r;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::ExecRS_DMEM1(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::ExecRS_DMEM1()");
  CHECK_CALL(param, kExecRS_DMEM1, "rcuExecRS_DMEM1");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuCommand* rs_dmem1 = fRcu->RS_DMEM1();
  if (!rs_dmem1) throw interface_missing("RS_DMEM1", "RCU");

  int r = rs_dmem1->Commit();
  if (r != 0) throw r;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::ExecRS_DMEM2(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::ExecRS_DMEM2()");
  CHECK_CALL(param, kExecRS_DMEM2, "rcuExecRS_DMEM2");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuCommand* rs_dmem2 = fRcu->RS_DMEM2();
  if (!rs_dmem2) throw interface_missing("RS_DMEM2", "RCU");

  int r = rs_dmem2->Commit();
  if (r != 0) throw r;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::ExecRS_STATUS(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::ExecRS_STATUS()");
  CHECK_CALL(param, kExecRS_STATUS, "rcuExecRS_STATUS");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuCommand* rs_status = fRcu->RS_STATUS();
  if (!rs_status) throw interface_missing("RS_STATUS", "RCU");	// 

  int r = rs_status->Commit();
  if (r != 0) throw r;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::ExecRS_TRCFG(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::ExecRS_TRCFG()");
  CHECK_CALL(param, kExecRS_TRCFG, "rcuExecRS_TRCFG");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuCommand* rs_trcfg = fRcu->RS_TRCFG(); // 
  if (!rs_trcfg) throw interface_missing("RS_TRCFG", "RCU");
  
  int r = rs_trcfg->Commit();
  if (r != 0) throw r;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::ExecRS_TRCNT(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::ExecRS_TRCNT()");
  CHECK_CALL(param, kExecRS_TRCNT, "rcuExecRS_TRCNT");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuCommand* rs_trcnt = fRcu->RS_TRCNT();
  if (!rs_trcnt) throw interface_missing("RS_TRCNT", "RCU");	// 
  int r = rs_trcnt->Commit();
  if (r != 0) throw r;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::ExecSCCOMMAND(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::ExecSCCOMMAND()");
  CHECK_CALL(param, kExecSCCOMMAND, "rcuExecSCCOMMAND");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuCommand* sccommand = fRcu->SCCOMMAND();
  if (!sccommand) throw interface_missing("SCCOMMAND", "RCU");
  int r = sccommand->Commit();
  if (r != 0) throw r;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::ExecSWTRG(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::ExecSWTRG()");
  CHECK_CALL(param, kExecSWTRG, "rcuExecSWTRG");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuCommand* swtrg = fRcu->SWTRG();
  if (!swtrg) throw interface_missing("SWTRG", "RCU");	// 
  int r = swtrg->Commit();
  if (r != 0) throw r;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::ExecTRG_CLR(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::ExecTRG_CLR()");
  CHECK_CALL(param, kExecTRG_CLR, "rcuExecTRG_CLR");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuCommand* trg_clr = fRcu->TRG_CLR();
  if (!trg_clr) throw interface_missing("TRG_CLR", "RCU");
  int r = trg_clr->Commit();
  if (r != 0) throw r;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::ExecWRFM(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::ExecWRFM()");
  CHECK_CALL(param, kExecWRFM, "rcuExecWRFM");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuCommand* wrfm = fRcu->WRFM();
  if (!wrfm) throw interface_missing("WRFM", "RCU");
  int r = wrfm->Commit();	// 
  if (r != 0) throw r;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::ExecRS_ERRREG(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::ExecRS_ERRREG()");
  CHECK_CALL(param, kExecRS_ERRREG, "rcuExecRS_ERRREG");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuCommand* rs_errreg = fRcu->RS_ERRREG();
  if (!rs_errreg) throw interface_missing("RS_ERRREG", "RCU");	// 
  int r = rs_errreg->Commit();
  if (r != 0) throw r;
  ret.setValue(r);

  return &ret;
}
      /** Register read access */       
//____________________________________________________________________
const IntegerVar* 
Pvss::Rcu::ReadERRST(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::ReadERRST()");
  CHECK_CALL(param, kReadERRST, "rcuReadERRST");
  static IntegerVar ret;
  ret.setValue(-1);

  BitVar* pattern;
  BitVar* abort;
  BitVar* timeout;
  BitVar* altro;
  BitVar* add;
  BitVar* busy;
  UIntegerVar* where;
  CheckArg(pattern, param.args->getNext(), param.thread);
  CheckArg(abort,   param.args->getNext(), param.thread);
  CheckArg(timeout, param.args->getNext(), param.thread);
  CheckArg(altro,   param.args->getNext(), param.thread);
  CheckArg(add,     param.args->getNext(), param.thread);
  CheckArg(busy,    param.args->getNext(), param.thread);
  CheckArg(where,   param.args->getNext(), param.thread);

  Rcuxx::RcuERRST* errst = fRcu->ERRST();
  if (!errst) throw interface_missing("ERRST", "RCU");

  unsigned r = errst->Update();
  // if (r) throw r;

  pattern->setValue(errst->IsPattern());
  abort->setValue(errst->IsAbort());
  timeout->setValue(errst->IsTimeout());
  altro->setValue(errst->IsAltro());
  add->setValue(errst->IsHWAdd());
  busy->setValue(errst->IsBusy());
  where->setValue(errst->Where());

  ret.setValue(r);
  return &ret;
}

//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::ReadTRCNT(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::ReadTRCNT()");
  CHECK_CALL(param, kReadTRCNT, "rcuReadTRCNT");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuTRCNT* trcnt = fRcu->TRCNT();
  if (!trcnt) throw interface_missing("TRCNT", "RCU");
  
  UIntegerVar* l1;
  UIntegerVar* l2;
  CheckArg(l1, param.args->getNext(), param.thread);
  CheckArg(l2, param.args->getNext(), param.thread);
  
  int r = trcnt->Update();
  if (r != 0) throw r;
  ret.setValue(r);
  l1->setValue(trcnt->Received());
  l2->setValue(trcnt->Accepted());

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::ReadLWADD(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::ReadLWADD()");
  CHECK_CALL(param, kReadLWADD, "rcuReadLWADD");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuLWADD* lwadd = fRcu->LWADD();
  if (!lwadd) throw interface_missing("LWADD", "RCU");
  
  UIntegerVar* v1;
  UIntegerVar* v2;
  CheckArg(v1, param.args->getNext(), param.thread);
  CheckArg(v2, param.args->getNext(), param.thread);
  
  int r = lwadd->Update();
  if (r != 0) throw r;
  ret.setValue(r);
  v1->setValue(lwadd->Bank1());
  v2->setValue(lwadd->Bank2());
  
  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::ReadIRADD(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::ReadIRADD()");
  CHECK_CALL(param, kReadIRADD, "rcuReadIRADD");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuIRADD* iradd = fRcu->IRADD();
  if (!iradd) throw interface_missing("IRADD", "RCU");

  UIntegerVar* add;
  CheckArg(add, param.args->getNext(), param.thread);
  
  int r = iradd->Update();
  if (r != 0) throw r;
  ret.setValue(r);
  add->setValue(iradd->IRADD());
  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::ReadIRDAT(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::ReadIRDAT()");
  CHECK_CALL(param, kReadIRDAT, "rcuReadIRDAT");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuIRDAT* irdat = fRcu->IRDAT();
  if (!irdat) throw interface_missing("IRDAT", "RCU");

  UIntegerVar* data;
  CheckArg(data, param.args->getNext(), param.thread);
  
  int r = irdat->Update();
  if (r != 0) throw r;
  ret.setValue(r);
  data->setValue(irdat->IRDAT());

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::ReadEVWORD(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::ReadEVWORD()");
  CHECK_CALL(param, kReadEVWORD, "rcuReadEVWORD");
  static IntegerVar ret;
  ret.setValue(-1);

  std::cout << "Getting EVWORD" << std::endl;
  Rcuxx::RcuEVWORD* evword = fRcu->EVWORD();
  if (!evword) throw interface_missing("EVWORD", "RCU");

  std::cout << "Checking out parameters 'add1' and 'add2'" << std::endl;
  UIntegerVar* add1;
  UIntegerVar* add2;
  CheckArg(add1, param.args->getNext(), param.thread);
  CheckArg(add2, param.args->getNext(), param.thread);
  
  std::cout << "Doing update" << std::endl;
  int r = evword->Update();
  std::cout << "Update returned " << r << std::endl;
  if (r != 0) throw r;

  std::cout << "Setting return value to " << evword->View1() << " and " << evword->View2() << std::endl;
  add1->setValue(evword->View1());
  add2->setValue(evword->View2());
  std::cout << "Returning " << r << " to caller" << std::endl; 
  ret.setValue(r);
  Rcuxx::DebugGuard::Message(fgDebug, "ReadEVWORD returns %d", ret.getValue());
  std::cout << "Returning the IntegerVar address" << std::endl;
  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::ReadACTFEC(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::ReadACTFEC()");
  CHECK_CALL(param, kReadACTFEC, "rcuReadACTFEC");
  static IntegerVar ret;
  ret.setValue(-1);

  std::cout << "Getting ACTFEC" << std::endl;
  Rcuxx::RcuACTFEC* actfec = fRcu->ACTFEC();
  if (!actfec) throw interface_missing("ACTFEC", "RCU");

  std::cout << "Checking out parameter 'mask'" << std::endl;
  UIntegerVar* mask;
  CheckArg(mask, param.args->getNext(), param.thread);
  
  std::cout << "Doing update" << std::endl;
  int r = actfec->Update();
  std::cout << "Update returned " << r << std::endl;
  if (r != 0) throw r;

  std::cout << "Setting return value to " << actfec->Value() << std::endl;
  mask->setValue(actfec->Value());
  std::cout << "Returning " << r << " to caller" << std::endl; 
  ret.setValue(r);
  Rcuxx::DebugGuard::Message(fgDebug, "ReadACTFEC returns %d", ret.getValue());
  std::cout << "Returning the IntegerVar address" << std::endl;
  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::ReadRDOFEC(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::ReadRDOFEC()");
  CHECK_CALL(param, kReadRDOFEC, "rcuReadRDOFEC");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuRDOFEC* rdofec = fRcu->RDOFEC();
  if (!rdofec) throw interface_missing("RDOFEC", "RCU");
  
  UIntegerVar* mask;
  CheckArg(mask, param.args->getNext(), param.thread);
  
  int r = rdofec->Update();
  if (r != 0) throw r;
  ret.setValue(r);
  mask->setValue(rdofec->Value());
  
  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::ReadTRCFG1(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::ReadTRCFG1()");
  CHECK_CALL(param, kReadTRCFG1, "rcuReadTRCFG1");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuTRCFG1* trcfg1 = fRcu->TRCFG1();
  if (!trcfg1) throw interface_missing("TRCFG1", "RCU");

  BitVar*      opt;
  BitVar*      pop;
  UIntegerVar* bmd;
  UIntegerVar* mode;
  UIntegerVar* twv;
  UIntegerVar* wptr;
  UIntegerVar* rptr;
  UIntegerVar* rem;
  BitVar*      empty;
  BitVar*      full;
  CheckArg(opt,   param.args->getNext(), param.thread);
  CheckArg(pop,   param.args->getNext(), param.thread);
  CheckArg(bmd,   param.args->getNext(), param.thread);
  CheckArg(mode,  param.args->getNext(), param.thread);
  CheckArg(twv,   param.args->getNext(), param.thread);
  CheckArg(wptr,  param.args->getNext(), param.thread);
  CheckArg(rptr,  param.args->getNext(), param.thread);
  CheckArg(rem,   param.args->getNext(), param.thread);
  CheckArg(empty, param.args->getNext(), param.thread);
  CheckArg(full,  param.args->getNext(), param.thread);

  int r = trcfg1->Update();
  if (r != 0) throw r;
  ret.setValue(r);
  opt->setValue(trcfg1->IsOpt());
  pop->setValue(trcfg1->IsPop());
  bmd->setValue(trcfg1->BMD() == Rcuxx::RcuTRCFG1::k4Buffers ? 4 : 8);
  mode->setValue(trcfg1->Mode() == Rcuxx::RcuTRCFG1::kDerivedL2 ? 2 : 
		trcfg1->Mode() == Rcuxx::RcuTRCFG1::kExternal  ? 3 : 0);
  twv->setValue(trcfg1->Twv());
  wptr->setValue(trcfg1->Wptr());
  rptr->setValue(trcfg1->Rptr());
  rem->setValue(trcfg1->Remb());
  empty->setValue(trcfg1->IsEmpty());
  full->setValue(trcfg1->IsFull());

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::ReadTRCFG2(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::ReadTRCFG2()");
  CHECK_CALL(param, kReadTRCFG2, "rcuReadTRCFG2");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuTRCFG2* trcfg2 = fRcu->TRCFG2();
  if (!trcfg2) throw interface_missing("TRCFG2", "RCU");
  BitVar* push;
  BitVar* hardware;
  CheckArg(push, param.args->getNext(), param.thread);
  CheckArg(hardware, param.args->getNext(), param.thread);

  int r = trcfg2->Update();
  if (r != 0) throw r;
  ret.setValue(r);

  push->setValue(trcfg2->Mode() == Rcuxx::RcuTRCFG2::kPush ? true : false);
  hardware->setValue(trcfg2->IsEnableHT());

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::ReadFMIREG(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::ReadFMIREG()");
  CHECK_CALL(param, kReadFMIREG, "rcuReadFMIREG");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuFMIREG* fmireg = fRcu->FMIREG();
  if (!fmireg) throw interface_missing("FMIREG", "RCU");

  UIntegerVar* data;
  CheckArg(data, param.args->getNext(), param.thread);

  int r = fmireg->Update();
  if (r != 0) throw r;
  ret.setValue(r);

  data->setValue(fmireg->Data());

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::ReadFMOREG(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::ReadFMOREG()");
  CHECK_CALL(param, kReadFMOREG, "rcuReadFMOREG");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuFMOREG* fmoreg = fRcu->FMOREG();
  if (!fmoreg) throw interface_missing("FMOREG", "RCU");

  UIntegerVar* data;
  CheckArg(data, param.args->getNext(), param.thread);

  int r = fmoreg->Update();
  if (r != 0) throw r;
  ret.setValue(r);

  data->setValue(fmoreg->Data());

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::ReadPMCFG(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::ReadPMCFG()");
  CHECK_CALL(param, kReadPMCFG, "rcuReadPMCFG");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuPMCFG* pmcfg = fRcu->PMCFG();
  if (!pmcfg) throw interface_missing("PMCFG", "RCU");

  UIntegerVar* begin;
  CheckArg(begin, param.args->getNext(), param.thread);
  UIntegerVar* end;
  CheckArg(end, param.args->getNext(), param.thread);

  int r = pmcfg->Update();
  if (r != 0) throw r;
  ret.setValue(r);

  begin->setValue(pmcfg->Begin());
  end->setValue(pmcfg->End());

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::ReadCHADD(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::ReadCHADD()");
  CHECK_CALL(param, kReadCHADD, "rcuReadCHADD");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuCHADD* chadd = fRcu->CHADD();
  if (!chadd) throw interface_missing("CHADD", "RCU");

  UIntegerVar* add1;
  UIntegerVar* add2;
  CheckArg(add1, param.args->getNext(), param.thread);
  CheckArg(add2, param.args->getNext(), param.thread);
  
  int r = chadd->Update();
  if (r != 0) throw r;
  ret.setValue(r);

  add1->setValue(chadd->Address1());
  add2->setValue(chadd->Address2());

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::ReadINTREG(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::ReadINTREG()");
  CHECK_CALL(param, kReadINTREG, "rcuReadINTREG");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuINTREG* intreg = fRcu->INTREG();
  if (!intreg) throw interface_missing("INTREG", "RCU");

  BitVar*      id;
  UIntegerVar* where;
  BitVar*      soft;
  BitVar*      answer;
  BitVar*      miss_sclk;
  BitVar*      alps;
  BitVar*      paps;
  BitVar*      dc;
  BitVar*      dv;
  BitVar*      ac;
  BitVar*      av;
  BitVar*      temp;
  CheckArg(id,        param.args->getNext(), param.thread);
  CheckArg(where,     param.args->getNext(), param.thread);
  CheckArg(soft,      param.args->getNext(), param.thread);
  CheckArg(answer,    param.args->getNext(), param.thread);
  CheckArg(miss_sclk, param.args->getNext(), param.thread);
  CheckArg(alps,      param.args->getNext(), param.thread);
  CheckArg(paps,      param.args->getNext(), param.thread);
  CheckArg(dc,        param.args->getNext(), param.thread);
  CheckArg(dv,        param.args->getNext(), param.thread);
  CheckArg(ac,        param.args->getNext(), param.thread);
  CheckArg(av,        param.args->getNext(), param.thread);
  CheckArg(temp,      param.args->getNext(), param.thread);
  
  int r = intreg->Update();
  if (r != 0) throw r;
  ret.setValue(r);
  
  id->setValue(intreg->IsIdentified());
  where->setValue(intreg->Where());
  soft->setValue(intreg->IsSoft());
  answer->setValue(intreg->IsAnswering());
  miss_sclk->setValue(intreg->IsMissedSclk());
  alps->setValue(intreg->IsAlps());
  paps->setValue(intreg->IsPaps());
  dc->setValue(intreg->IsDcOverTh());
  dv->setValue(intreg->IsDvOverTh());
  ac->setValue(intreg->IsAcOverTh());
  av->setValue(intreg->IsAvOverTh());
  temp->setValue(intreg->IsTempOverTh());

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::ReadRESREG(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::ReadRESREG()");
  CHECK_CALL(param, kReadRESREG, "rcuReadRESREG");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuRESREG* resreg = fRcu->RESREG();
  if (!resreg) throw interface_missing("RESREG", "RCU");

  UIntegerVar* add;
  UIntegerVar* res;
  CheckArg(add, param.args->getNext(), param.thread);
  CheckArg(res, param.args->getNext(), param.thread);

  int r = resreg->Update();
  if (r != 0) throw r;
  ret.setValue(r);

  add->setValue(resreg->Address());
  res->setValue(resreg->Result());

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::ReadERRREG(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::ReadERRREG()");
  CHECK_CALL(param, kReadERRREG, "rcuReadERRREG");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuERRREG* errreg = fRcu->ERRREG();
  if (!errreg) throw interface_missing("ERRREG", "RCU");

  BitVar* noAck;
  BitVar* notActive;
  CheckArg(noAck, param.args->getNext(), param.thread);
  CheckArg(notActive, param.args->getNext(), param.thread);

  int r = errreg->Update();
  if (r != 0) throw r;
  
  ret.setValue(r);
  noAck->setValue(errreg->IsNoAcknowledge());
  notActive->setValue(errreg->IsNotActive());
  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::ReadINTMOD(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::ReadINTMOD()");
  CHECK_CALL(param, kReadINTMOD, "rcuReadINTMOD");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuINTMOD* intmod = fRcu->INTMOD();
  if (!intmod) throw interface_missing("INTMOD", "RCU");

  BitVar* a;
  BitVar* b;
  CheckArg(a, param.args->getNext(), param.thread);
  CheckArg(b, param.args->getNext(), param.thread);

  int r = intmod->Update();
  if (r != 0) throw r;
  ret.setValue(r);
  a->setValue(intmod->BranchA());
  b->setValue(intmod->BranchB());

  return &ret;
}

//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::ReadStatusCount(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::ReadStatusCount()");
  CHECK_CALL(param, kReadStatusCount, "rcuReadStatusCount");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuSTATUS* status = fRcu->STATUS();
  if (!status) throw interface_missing("STATUS", "RCU");

  UIntegerVar* n;
  CheckArg(n, param.args->getNext(), param.thread);
  int r = status->Update();
  if (r) throw r;
  ret.setValue(r);

  n->setValue(status->Entries());
  return &ret;
}

//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::ReadStatusEntry(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::ReadStatusEntry()");
  CHECK_CALL(param, kReadStatusEntry, "rcuReadStatusEntry");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuSTATUS* status = fRcu->STATUS();
  if (!status) throw interface_missing("STATUS", "RCU");

  UIntegerVar i;
  CheckArg(i, param.args->getNext(), param.thread);
  if (i.getValue() < 0 || i.getValue() >= status->Entries()) return &ret;
  const Rcuxx::RcuStatusEntry& intreg = status->Entry(i.getValue());

  BitVar*      id;
  UIntegerVar* where;
  BitVar*      soft;
  BitVar*      answer;
  BitVar*      miss_sclk;
  BitVar*      alps;
  BitVar*      paps;
  BitVar*      dc;
  BitVar*      dv;
  BitVar*      ac;
  BitVar*      av;
  BitVar*      temp;
  CheckArg(id,        param.args->getNext(), param.thread);
  CheckArg(where,     param.args->getNext(), param.thread);
  CheckArg(soft,      param.args->getNext(), param.thread);
  CheckArg(answer,    param.args->getNext(), param.thread);
  CheckArg(miss_sclk, param.args->getNext(), param.thread);
  CheckArg(alps,      param.args->getNext(), param.thread);
  CheckArg(paps,      param.args->getNext(), param.thread);
  CheckArg(dc,        param.args->getNext(), param.thread);
  CheckArg(dv,        param.args->getNext(), param.thread);
  CheckArg(ac,        param.args->getNext(), param.thread);
  CheckArg(av,        param.args->getNext(), param.thread);
  CheckArg(temp,      param.args->getNext(), param.thread);
  
  ret.setValue(0);
  
  id->setValue(intreg.IsIdentified());
  where->setValue(intreg.Where());
  soft->setValue(intreg.IsSoft());
  answer->setValue(intreg.IsAnswering());
  miss_sclk->setValue(intreg.IsMissedSclk());
  alps->setValue(intreg.IsAlps());
  paps->setValue(intreg.IsPaps());
  dc->setValue(intreg.IsDcOverTh());
  dv->setValue(intreg.IsDvOverTh());
  ac->setValue(intreg.IsAcOverTh());
  av->setValue(intreg.IsAvOverTh());
  temp->setValue(intreg.IsTempOverTh());
  return & ret;
}

//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::WriteACTFEC(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::WriteACTFEC()");
  CHECK_CALL(param, kWriteACTFEC, "rcuWriteACTFEC");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuACTFEC* actfec = fRcu->ACTFEC();
  if (!actfec) throw interface_missing("ACTFEC", "RCU");

  UIntegerVar mask;
  CheckArg(mask, param.args->getNext(), param.thread);
  actfec->SetValue(mask.getValue());

  int r = actfec->Commit();
  if (r != 0) throw r;

  ret.setValue(r);
  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::WriteTRCFG1(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::WriteTRCFG1()");
  CHECK_CALL(param, kWriteTRCFG1, "rcuWriteTRCFG1");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuTRCFG1* trcfg1 = fRcu->TRCFG1();
  if (!trcfg1) throw interface_missing("TRCFG1", "RCU");

  BitVar      opt;
  BitVar      pop;
  UIntegerVar bmd;
  UIntegerVar mode;
  UIntegerVar twv;
  CheckArg(opt,  param.args->getNext(), param.thread);
  CheckArg(pop,  param.args->getNext(), param.thread);
  CheckArg(bmd,  param.args->getNext(), param.thread);
  CheckArg(mode, param.args->getNext(), param.thread);
  CheckArg(twv,  param.args->getNext(), param.thread);
  trcfg1->SetOpt(opt.getValue());
  trcfg1->SetPop(pop.getValue());
  trcfg1->SetBMD(bmd.getValue() >= 8 ? 
		 Rcuxx::RcuTRCFG1::k4Buffers : 
		 Rcuxx::RcuTRCFG1::k8Buffers);
  trcfg1->SetMode(mode.getValue() == 2 ? Rcuxx::RcuTRCFG1::kDerivedL2 : 
		  mode.getValue() == 3 ? Rcuxx::RcuTRCFG1::kExternal  : 
		  Rcuxx::RcuTRCFG1::kInternal);

  int r = trcfg1->Commit();
  if (r != 0) throw r;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::WriteTRCFG2(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::WriteTRCFG2()");
  CHECK_CALL(param, kWriteTRCFG2, "rcuWriteTRCFG2");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuTRCFG2* trcfg2 = fRcu->TRCFG2();
  if (!trcfg2) throw interface_missing("TRCFG2", "RCU");

  BitVar push;
  BitVar hardware;
  CheckArg(push, param.args->getNext(), param.thread);
  CheckArg(hardware, param.args->getNext(), param.thread);
  trcfg2->SetMode(push.getValue() ? 
		  Rcuxx::RcuTRCFG2::kPush : 
		  Rcuxx::RcuTRCFG2::kPop);
  trcfg2->SetEnableHT(hardware.getValue());

  int r = trcfg2->Commit();
  if (r != 0) throw r;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::WriteFMIREG(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::WriteFMIREG()");
  CHECK_CALL(param, kWriteFMIREG, "rcuWriteFMIREG");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuFMIREG* fmireg = fRcu->FMIREG();
  if (!fmireg) throw interface_missing("FMIREG", "RCU");

  UIntegerVar data;
  CheckArg(data, param.args->getNext(), param.thread);

  fmireg->SetData(data.getValue());
  int r = fmireg->Commit();
  if (r != 0) throw r;

  ret.setValue(r);
  
  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::WriteFMOREG(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::WriteFMOREG()");
  CHECK_CALL(param, kWriteFMOREG, "rcuWriteFMOREG");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuFMOREG* fmoreg = fRcu->FMOREG();
  if (!fmoreg) throw interface_missing("FMOREG", "RCU");

  UIntegerVar data;
  CheckArg(data, param.args->getNext(), param.thread);

  fmoreg->SetData(data.getValue());
  int r = fmoreg->Commit();
  if (r != 0) throw r;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::WritePMCFG(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::WritePMCFG()");
  CHECK_CALL(param, kWritePMCFG, "rcuWritePMCFG");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuPMCFG* pmcfg = fRcu->PMCFG();
  if (!pmcfg) throw interface_missing("PMCFG", "RCU");

  UIntegerVar begin;
  UIntegerVar end;
  CheckArg(begin, param.args->getNext(), param.thread);
  CheckArg(end, param.args->getNext(), param.thread);
  pmcfg->SetBegin(begin.getValue());
  pmcfg->SetEnd(end.getValue());
  
  int r = pmcfg->Commit();
  if (r != 0) throw r;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::WriteRESREG(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::WriteRESREG()");
  CHECK_CALL(param, kWriteRESREG, "rcuWriteRESREG");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuRESREG* resreg = fRcu->RESREG();
  if (!resreg) throw interface_missing("RESREG", "RCU");

  UIntegerVar add;
  UIntegerVar dat;
  CheckArg(add, param.args->getNext(), param.thread);
  CheckArg(dat, param.args->getNext(), param.thread);
  resreg->SetAddress(add.getValue());
  resreg->SetResult(dat.getValue());

  int r = resreg->Commit();
  if (r != 0) throw r;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::WriteINTMOD(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::WriteINTMOD()");
  CHECK_CALL(param, kWriteINTMOD, "rcuWriteINTMOD");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuINTMOD* intmod = fRcu->INTMOD();
  if (!intmod) throw interface_missing("INTMOD", "RCU");

  BitVar a;
  BitVar b;
  CheckArg(a, param.args->getNext(), param.thread);
  CheckArg(b, param.args->getNext(), param.thread);
  intmod->SetBranchA(a.getValue());
  intmod->SetBranchB(b.getValue());

  int r = intmod->Commit();
  if (r != 0) throw r;
  ret.setValue(r);

  return &ret;
}

//____________________________________________________________________
void
Pvss::Rcu::WriteArray(const unsigned int          mask,
					  ExecuteParamRec&            param, 
					  std::vector<unsigned int>&  data, 
					  unsigned int&               base) 
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::WriteArray()");
  // Get arguments from caller 
  UIntegerVar	baseAddress; 
  DynVar        vars(UINTEGER_VAR);  
  CheckArg(baseAddress, param.args->getNext(), param.thread);
  CheckArg(vars,		param.args->getNext(), param.thread);

  base  = baseAddress.getValue();
  int m = vars.getArrayLength();
  if (m > data.size()) data.resize(m);

  int i = 0;
  UIntegerVar* vPar = static_cast<UIntegerVar*>(vars.getFirst());
  while (vPar) {
	  data[i] = vPar->getValue() & mask;
	  vPar = static_cast<UIntegerVar*>(vars.getNext());
      i++;
  }
}
//____________________________________________________________________
void
Pvss::Rcu::ReadArray(const unsigned int                mask,
					 ExecuteParamRec&                  param, 
					 const std::vector<unsigned int>&  data) 
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::ReadArray()");
  // Get arguments from caller 
  DynVar* rets = 0;//(UINTEGER_VAR);  
  CheckArg(rets, param.args->getNext(), param.thread);
  if (!rets) throw ErrClass::ARG_MISSING;
  int m = rets->getArrayLength();
  int i = 0;
  Variable*    var  = rets->getFirst();
  if (var && !var->isA(UINTEGER_VAR)) throw std::bad_cast();
  UIntegerVar* vPar = static_cast<UIntegerVar*>(var);
  while (vPar) {
	  if (i >= data.size()) { 
		  std::cerr << "Hmm.  Data array is bigger than target!" << std::endl;
		  break;
	  }
	  vPar->setValue(data[i] & mask);
	  vPar = static_cast<UIntegerVar*>(rets->getNext());
      i++;
  }
  i = 0;
  vPar = static_cast<UIntegerVar*>(rets->getFirst());
  while (vPar){ 
	  std::cout << std::setw(3) << i << ": " << vPar->getValue() << std::endl;
	  vPar = static_cast<UIntegerVar*>(rets->getNext());
	 i++;
  }
}

//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::SizeOfACL(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::SizeOfACL()");
  CHECK_CALL(param, kSizeOfACL, "rcuSizeOfACL");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuACL* acl = fRcu->ACL();
  std::cout << "ACL is " << acl << std::endl;
  if (!acl) throw interface_missing("ACL", "RCU");
  
  std::cout << "Size of ACL is " << acl->Size();
  Rcuxx::DebugGuard::Message(fgDebug, "Rcu::SizeOfACL() reading acl size to %d",  acl->Size() );

  ret.setValue(acl->Size());
  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::SizeOfIMEM(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::SizeOfIMEM()");
  CHECK_CALL(param, kSizeOfIMEM, "rcuSizeOfIMEM");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuIMEM* reg = fRcu->IMEM();
  std::cout << "IMEM  is " << reg << std::endl;
  if (!reg) throw interface_missing("IMEM", "RCU");
  
  std::cout << "Size of IMEM is " << reg->Size();
  Rcuxx::DebugGuard::Message(fgDebug, "Rcu::SizeOfIMEM() reading IMEM size to %d",  reg->Size() );

  ret.setValue(reg->Size());
  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::SizeOfPMEM(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::SizeOfPMEM()");
  CHECK_CALL(param, kSizeOfPMEM, "rcuSizeOfPMEM");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuPMEM* reg = fRcu->PMEM();
  std::cout << "PMEM  is " << reg << std::endl;
  if (!reg) throw interface_missing("PMEM", "RCU");
  
  std::cout << "Size of PMEM is " << reg->Size();
  Rcuxx::DebugGuard::Message(fgDebug, "Rcu::SizeOfPMEM() reading PMEM size to %d",  reg->Size() );

  ret.setValue(reg->Size());
  return &ret;
}

//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::WriteACL(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::WriteACL()");
  CHECK_CALL(param, kWriteACL, "rcuWriteACL");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuACL* acl = fRcu->ACL();
  if (!acl) throw interface_missing("ACL", "RCU");

  unsigned int base;
  std::vector<unsigned int> a;
  WriteArray(0xffff, param, a, base);
  
  Rcuxx::DebugGuard ha(fgDebug, "base address %d  ", base );
  Rcuxx::DebugGuard hc(fgDebug, "array size %d  ", a.size() );
  
  acl->Set(base, a.size(), &a[0]); 

  for (size_t i = 0; i < a.size(); i++) 
	  std::cout << std::setw(3) << i << ": " << a[i] << std::endl;

  int r = acl->Commit();
  if (r != 0) throw r;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::WriteIMEM(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::WriteIMEM()");
  CHECK_CALL(param, kWriteIMEM, "rcuWriteIMEM");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuIMEM* reg = fRcu->IMEM();
  if (!reg) throw interface_missing("IMEM", "RCU");

  unsigned int base;
  std::vector<unsigned int> a;
  WriteArray(0xffff, param, a, base);
  
  Rcuxx::DebugGuard ha(fgDebug, "base address %d  ", base );
  Rcuxx::DebugGuard hc(fgDebug, "array size %d  ", a.size() );
  
  reg->Set(base, a.size(), &a[0]); 

  for (size_t i = 0; i < a.size(); i++) 
	  std::cout << std::setw(3) << i << ": " << a[i] << std::endl;

  int r = reg->Commit();
  if (r != 0) throw r;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::WritePMEM(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::WritePMEM()");
  CHECK_CALL(param, kWritePMEM, "rcuWritePMEM");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuPMEM* reg = fRcu->PMEM();
  if (!reg) throw interface_missing("PMEM", "RCU");

  unsigned int base;
  std::vector<unsigned int> a;
  WriteArray(0xffff, param, a, base);
  
  Rcuxx::DebugGuard ha(fgDebug, "base address %d  ", base );
  Rcuxx::DebugGuard hc(fgDebug, "array size %d  ", a.size() );
  
  reg->Set(base, a.size(), &a[0]); 

  for (size_t i = 0; i < a.size(); i++) 
	  std::cout << std::setw(3) << i << ": " << a[i] << std::endl;

  int r = reg->Commit();
  if (r != 0) throw r;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::ReadACL(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::ReadACL()");
  CHECK_CALL(param, kReadACL, "rcuReadACL");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuACL* acl = fRcu->ACL();
  if (!acl) throw interface_missing("ACL", "RCU");
	
  int r = acl->Update();
  if (r != 0) throw r;
  ret.setValue(r);

  const Rcuxx::RcuMemory::Cache_t& a = acl->Data();
  ReadArray(0xffff, param, a);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::ReadIMEM(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::ReadIMEM()");
  CHECK_CALL(param, kReadIMEM, "rcuReadIMEM");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuIMEM* reg = fRcu->IMEM();
  if (!reg) throw interface_missing("IMEM", "RCU");
	
  int r = reg->Update();
  if (r != 0) throw r;
  ret.setValue(r);

  const Rcuxx::RcuMemory::Cache_t& a = reg->Data();
  ReadArray(0xffff, param, a);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::ReadPMEM(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::ReadPMEM()");
  CHECK_CALL(param, kReadPMEM, "rcuReadPMEM");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuPMEM* reg = fRcu->PMEM();
  if (!reg) throw interface_missing("PMEM", "RCU");
	
  int r = reg->Update();
  if (r != 0) throw r;
  ret.setValue(r);

  const Rcuxx::RcuMemory::Cache_t& a = reg->Data();
  ReadArray(0xffff, param, a);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::ZeroACL(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::ZeroACL()");
  CHECK_CALL(param, kZeroACL, "rcuZeroACL");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuACL* acl = fRcu->ACL();
  if (!acl) throw interface_missing("ACL", "RCU");
	
  acl->Zero();
  
  int r = acl->Commit();
  if (r != 0) throw r;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Rcu::ZeroIMEM(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::ZeroIMEM()");
  CHECK_CALL(param, kZeroIMEM, "rcuZeroIMEM");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuIMEM* reg = fRcu->IMEM();
  if (!reg) throw interface_missing("IMEM", "RCU");
	
  reg->Zero();
  
  int r = reg->Commit();
  if (r != 0) throw r;
  ret.setValue(r);

  return &ret;
}
  //____________________________________________________________________
const IntegerVar*
Pvss::Rcu::ZeroPMEM(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcu::ZeroPMEM()");
  CHECK_CALL(param, kZeroPMEM, "rcuZeroPMEM");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::RcuPMEM* reg = fRcu->PMEM();
  if (!reg) throw interface_missing("PMEM", "RCU");
	
  reg->Zero();
  
  int r = reg->Commit();
  if (r != 0) throw r;
  ret.setValue(r);

  return &ret;
}

//____________________________________________________________________
//
// EOF
// 
