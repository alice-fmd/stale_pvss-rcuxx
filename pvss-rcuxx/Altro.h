// -*- mode: c++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    pvss-rcuxx/Altro.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Declaration of ALTRO interface 
*/
#ifndef Pvss_Altro_h
#define Pvss_Altro_h
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#ifndef _BASEEXTERNHDL_H_
# include <BaseExternHdl.hxx>
#endif
#ifndef Pvss_Rcu_h
# include "Rcu.h"
#endif
class Variable;
class IntegerVar;
namespace Rcuxx
{
  class Altro;
}
namespace Pvss
{
  /** @class Altro Altro.h <pvss-rcuxx/Altro.h>
      @brief ALTRO interface 
  */
  class Altro
  {
  public:
    /** Type of call parameters */
    typedef BaseExternHdl::ExecuteParamRec ExecuteParamRec;
    /** Enum of possible function calls */
    enum {
      /** Address interface */ 
      kSetAddress   = Rcu::kAltroBase, 
      kGetAddress,
      /** Command interfaces */ 
      kExecWPINC,
      kExecRPINC,
      kExecCHRDO, 
      kExecSWTRG, 
      kExecTRCLR, 
      kExecERCLR, 
      /** Register read access */ 
      kReadK1, 
      kReadK2, 
      kReadK3, 
      kReadL1, 
      kReadL2, 
      kReadL3, 
      kReadVFPED, 
      kReadPMDTA, 
      kReadZSTHR,
      kReadBCTHR, 
      kReadTRCFG, 
      kReadDPCFG, 
      kReadDPCF2, 
      kReadPMADD, 
      kReadERSTR, 
      kReadADEVL, 
      kReadTRCNT, 
      /** Register read access */ 
      kWriteK1, 
      kWriteK2, 
      kWriteK3, 
      kWriteL1, 
      kWriteL2, 
      kWriteL3, 
      kWriteVFPED, 
      kWritePMDTA, 
      kWriteZSTHR,
      kWriteBCTHR, 
      kWriteTRCFG, 
      kWriteDPCFG, 
      kWriteDPCF2, 
      kWritePMADD, 
      /** Base off  set for Bc */
      kBcBase
    };

    /** Constructor 
	@param altro Pointer to low-level access */
    Altro(Rcuxx::Altro* altro);
    /** Destructor */ 
    virtual ~Altro();
    /** Execute a function.  This is delegated from the handler to
	here. Note, when we get here, at least one parameter is read
	from the execution structore, so we should use `getNext', not
	`getFirst'.  
	@param param Parameters of the function call 
	@return A pointer to the return value */
    virtual const Variable *execute(ExecuteParamRec &param);

  protected:
    /** Address interface */ 
    /** SetAddress */
    const IntegerVar* SetAddress(ExecuteParamRec& param);
    /** GetAddress */
    const IntegerVar* GetAddress(ExecuteParamRec& param);
    /** Command interfaces */ 
    /** ExecWPINC */
    const IntegerVar* ExecWPINC(ExecuteParamRec& param);
    /** ExecRPINC */
    const IntegerVar* ExecRPINC(ExecuteParamRec& param);
    /** ExecCHRDO */
    const IntegerVar* ExecCHRDO(ExecuteParamRec& param);
    /** ExecSWTRG */
    const IntegerVar* ExecSWTRG(ExecuteParamRec& param);
    /** ExecTRCLR */
    const IntegerVar* ExecTRCLR(ExecuteParamRec& param);
    /** ExecERCLR */
    const IntegerVar* ExecERCLR(ExecuteParamRec& param);
    /** Register read access */ 
    /** ReadK1 */
    const IntegerVar* ReadK1(ExecuteParamRec& param);
    /** ReadK2 */
    const IntegerVar* ReadK2(ExecuteParamRec& param);
    /** ReadK3 */
    const IntegerVar* ReadK3(ExecuteParamRec& param);
    /** ReadL1 */
    const IntegerVar* ReadL1(ExecuteParamRec& param);
    /** ReadL2 */
    const IntegerVar* ReadL2(ExecuteParamRec& param);
    /** ReadL3 */
    const IntegerVar* ReadL3(ExecuteParamRec& param);
    /** ReadVFPED */
    const IntegerVar* ReadVFPED(ExecuteParamRec& param);
    /** ReadPMDTA */
    const IntegerVar* ReadPMDTA(ExecuteParamRec& param);
    /** ReadZSTHR */
    const IntegerVar* ReadZSTHR(ExecuteParamRec& param);
    /** ReadBCTHR */
    const IntegerVar* ReadBCTHR(ExecuteParamRec& param);
    /** ReadTRCFG */
    const IntegerVar* ReadTRCFG(ExecuteParamRec& param);
    /** ReadDPCFG */
    const IntegerVar* ReadDPCFG(ExecuteParamRec& param);
    /** ReadDPCF2 */
    const IntegerVar* ReadDPCF2(ExecuteParamRec& param);
    /** ReadPMADD */
    const IntegerVar* ReadPMADD(ExecuteParamRec& param);
    /** ReadERSTR */
    const IntegerVar* ReadERSTR(ExecuteParamRec& param);
    /** ReadADEVL */
    const IntegerVar* ReadADEVL(ExecuteParamRec& param);
    /** ReadTRCNT */
    const IntegerVar* ReadTRCNT(ExecuteParamRec& param);
    /** Register read access */ 
    /** WriteK1 */
    const IntegerVar* WriteK1(ExecuteParamRec& param);
    /** WriteK2 */
    const IntegerVar* WriteK2(ExecuteParamRec& param);
    /** WriteK3 */
    const IntegerVar* WriteK3(ExecuteParamRec& param);
    /** WriteL1 */
    const IntegerVar* WriteL1(ExecuteParamRec& param);
    /** WriteL2 */
    const IntegerVar* WriteL2(ExecuteParamRec& param);
    /** WriteL3 */
    const IntegerVar* WriteL3(ExecuteParamRec& param);
    /** WriteVFPED */
    const IntegerVar* WriteVFPED(ExecuteParamRec& param);
    /** WritePMDTA */
    const IntegerVar* WritePMDTA(ExecuteParamRec& param);
    /** WriteZSTHR */
    const IntegerVar* WriteZSTHR(ExecuteParamRec& param);
    /** WriteBCTHR */
    const IntegerVar* WriteBCTHR(ExecuteParamRec& param);
    /** WriteTRCFG */
    const IntegerVar* WriteTRCFG(ExecuteParamRec& param);
    /** WriteDPCFG */
    const IntegerVar* WriteDPCFG(ExecuteParamRec& param);
    /** WriteDPCF2 */
    const IntegerVar* WriteDPCF2(ExecuteParamRec& param);
    /** WritePMADD */
    const IntegerVar* WritePMADD(ExecuteParamRec& param);
    /** Pointer to low-level access */
    Rcuxx::Altro* fAltro;
  };
}

#endif
//
// EOF
//
