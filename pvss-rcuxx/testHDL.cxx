#include "config.h"
#include "Util.h"
#include <Ctrl/CtrlVar.hxx>
#include <stdexcept>
#include <vector>
#ifndef ADDVERINFO_HXX
# include <addVerInfo.cxx>
#else 
# include ADDVERINFO_HXX
#endif
bool fgDebug = true;

//____________________________________________________________________
struct Test
{
  Test() : fFoo(42), fBar(3.14), fBaz(0xf) 
  {
  }
  
  int Foo() const { 
    std::cout << "Test::Foo() const -> " << fFoo << std::endl;
    return fFoo;
  }
  float Bar() const 
  { 
    std::cout << "Test::Bar() const -> " << fBar << std::endl;
    return fBar;
  }
  unsigned int Baz() const 
  { 
    std::cout << "Test::Baz() const -> 0x" 
	      << std::hex << fBaz << std::dec << std::endl;
    return fBaz;
  }
  void SetFoo(int x)
  { 
    fFoo = x;
    std::cout << "Test::SetFoo(" << x << ") -> " << fFoo << std::endl;
  }
  void SetBar(float x) 
  { 
    fBar = x;
    std::cout << "Test::SetBar(" << x << ") -> " << fBar << std::endl;
  }
  void SetBaz(unsigned int x) 
  { 
    fBaz = x;
    std::cout << "Test::SetBaz(0x" << std::hex << x << ") -> 0x" << fBaz 
	      << std::dec << std::endl;
  }
  int fFoo;
  float fBar;
  unsigned int fBaz;
};

struct TestHDL : public BaseExternHdl
{
  typedef std::vector<Pvss::InterfaceDeclBase*> InterfaceVector;

  static FunctionListRec* MakeIntefaces()
  {
    static FunctionListRec* ret = 0;
    if (fInterfaces.size() <= 0) {
      Pvss::InterfaceDeclT<Test>* i1 = 
	Pvss::MakeReadIF<Test,int>("readTest",&Test::Foo,&Test::Bar,&Test::Baz);
      Pvss::InterfaceDeclT<Test>* i2 = 
	Pvss::MakeWriteIF<Test,int>("writeTest",
				    &Test::SetFoo,&Test::SetBar,&Test::SetBaz);
      fInterfaces.push_back(i1);
      fInterfaces.push_back(i2);
      
      const size_t n = fInterfaces.size();
      ret = new FunctionListRec[n];
      for (size_t i = 0; i < n; i++) { 
	ret[i] = fInterfaces[i]->GetRecord();
      }
    }
    return ret;
  }
  static const InterfaceVector& GetInterfaces() { return fInterfaces; }
  TestHDL(BaseExternHdl*  nextHdl, 
	  PVSSulong       funcCount, 
	  FunctionListRec fnList[])
    : BaseExternHdl(nextHdl, funcCount, fnList)
  {}

  /** Execute a function 
      @param param Function call parameters 
      @return A return value */
  virtual const Variable *execute(ExecuteParamRec &param)
  {
    static IntegerVar ret;
    ret.setValue(-1);
  
    // Clear list of errors. 
    param.thread->clearLastError();

    // Everyhting inside a try box 
    try {
      const int id = param.funcNum;
      param.args->getFirst();
      if (id < 0 || id >= fInterfaces.size()) 
	throw std::invalid_argument("Index out of bounds");
    
      Pvss::InterfaceDeclBase* inf = fInterfaces[id];
      const Variable* r = (inf->operator()(param));
      ret = *r;
    }
  
    catch (Pvss::interface_missing& e) {
      ErrClass err(ErrClass::PRIO_WARNING, ErrClass::ERR_CONTROL, 
		   ErrClass::ILLEGAL_OP, param.clientData->getLocation(), 
		   param.funcName, e.what());
      param.thread->appendLastError(&err);
      if (fgDebug) std::cerr << "Rcuxx exception: " << e.what() << std::endl;
      ret.setValue(-1);
    }    
    catch (Pvss::not_lvalue& e) {
      ErrClass err(ErrClass::PRIO_WARNING, ErrClass::ERR_PARAM, 
		   ErrClass::NO_LVALUE, param.clientData->getLocation(), 
		   param.funcName, e.what());
      param.thread->appendLastError(&err);
      if (fgDebug) std::cerr << "Rcuxx exception: " << e.what() << std::endl;
      ret.setValue(-1);
    }    
    catch (std::invalid_argument& e) {
      ErrClass err(ErrClass::PRIO_WARNING, ErrClass::ERR_PARAM, 
		   ErrClass::ARG_MISSING, param.clientData->getLocation(), 
		   param.funcName, e.what());
      param.thread->appendLastError(&err);
      if (fgDebug) std::cerr << "Rcuxx exception: " << e.what() << std::endl;
      ret.setValue(-1);
    }    
    catch (std::bad_cast& e) {
      ErrClass err(ErrClass::PRIO_WARNING, ErrClass::ERR_PARAM, 
		   ErrClass::ILLEGAL_ARG, param.clientData->getLocation(), 
		   param.funcName, e.what());
      param.thread->appendLastError(&err);
      if (fgDebug) std::cerr << "Rcuxx exception: " << e.what() << std::endl;
      ret.setValue(-1);
    }    
    catch (std::exception& e) {
      ErrClass err(ErrClass::PRIO_SEVERE, ErrClass::ERR_CONTROL, 
		   ErrClass::UNEXPECTEDSTATE, param.clientData->getLocation(), 
		   param.funcName, e.what());
      param.thread->appendLastError(&err);
      if (fgDebug) std::cerr << "Rcuxx exception: " << e.what() << std::endl;
      ret.setValue(-1);
    }
    return &ret;
  }
  
protected:
  static InterfaceVector fInterfaces;
};

//____________________________________________________________________
BaseExternHdl *newExternHdl(BaseExternHdl *nextHdl)
{
  Rcuxx::DebugGuard g(fgDebug, "newExternHdl");
  // this line counts the number of functions you want to implement.
  
  FunctionListRec* fnList    = TestHDL::MakeIntefaces();
  PVSSulong        funcCount = TestHDL::GetInterfaces().size();

  // now allocate the new instance
  return new TestHDL(nextHdl, funcCount, fnList);
}
  
  
