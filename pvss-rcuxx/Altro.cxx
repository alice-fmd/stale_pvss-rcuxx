//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    pvss-rcuxx/Bc.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of RCU interface 
*/
#include <stdexcept>
#include "Altro.h"
#include "Util.h"
#include <rcuxx/Altro.h>

namespace {
#ifdef NO_CHECK
  bool fgDebug = true;
#else
  bool fgDebug = false;
#endif
}

//____________________________________________________________________
Pvss::Altro::Altro(Rcuxx::Altro* altro) 
  : fAltro(altro)
{
  Rcuxx::DebugGuard g(fgDebug, "Altro::Altro(%p)", altro);
  if (!fAltro) throw std::runtime_error("No ALTRO interface");
}

//____________________________________________________________________
Pvss::Altro::~Altro()
{
  Rcuxx::DebugGuard g(fgDebug, "Altro::~Altro()");
  if (fAltro) delete fAltro;
}

//____________________________________________________________________
const Variable* 
Pvss::Altro::execute(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, 
		      "Altro::execute [# %d -> name: %s, # args %d]", 
		      param.funcNum, param.funcName.c_str(), 
		      (param.args ? param.args->getNumberOfItems() : 0));

  static IntegerVar ret;
  switch (param.funcNum) {
    /** Address interface */ 
  case kSetAddress:	return SetAddress(param);
  case kGetAddress:	return GetAddress(param);
    /** Command interfaces */ 
  case kExecWPINC:	return ExecWPINC(param);
  case kExecRPINC:	return ExecRPINC(param);
  case kExecCHRDO:	return ExecCHRDO(param);
  case kExecSWTRG:	return ExecSWTRG(param);
  case kExecTRCLR:	return ExecTRCLR(param);
  case kExecERCLR:	return ExecERCLR(param);
    /** Register read access */ 
  case kReadK1:		return ReadK1(param);
  case kReadK2:		return ReadK2(param);
  case kReadK3:		return ReadK3(param);
  case kReadL1:		return ReadL1(param);
  case kReadL2:		return ReadL2(param);
  case kReadL3:		return ReadL3(param);
  case kReadVFPED:	return ReadVFPED(param);
  case kReadPMDTA:	return ReadPMDTA(param);
  case kReadZSTHR:	return ReadZSTHR(param);
  case kReadBCTHR:	return ReadBCTHR(param);
  case kReadTRCFG:	return ReadTRCFG(param);
  case kReadDPCFG:	return ReadDPCFG(param);
  case kReadDPCF2:	return ReadDPCF2(param);
  case kReadPMADD:	return ReadPMADD(param);
  case kReadERSTR:	return ReadERSTR(param);
  case kReadADEVL:	return ReadADEVL(param);
  case kReadTRCNT:	return ReadTRCNT(param);
    /** Register read access */ 
  case kWriteK1:	return WriteK1(param);
  case kWriteK2:	return WriteK2(param);
  case kWriteK3:	return WriteK3(param);
  case kWriteL1:	return WriteL1(param);
  case kWriteL2:	return WriteL2(param);
  case kWriteL3:	return WriteL3(param);
  case kWriteVFPED:	return WriteVFPED(param);
  case kWritePMDTA:	return WritePMDTA(param);
  case kWriteZSTHR:	return WriteZSTHR(param);
  case kWriteBCTHR:	return WriteBCTHR(param);
  case kWriteTRCFG:	return WriteTRCFG(param);
  case kWriteDPCFG:	return WriteDPCFG(param);
  case kWriteDPCF2:	return WriteDPCF2(param);
  case kWritePMADD:	return WritePMADD(param);
  default: 
    std::cerr << "Function # " << param.funcNum 
	      << ": " << param.funcName.c_str() 
	      << " is not an ALTRO function [" << kSetAddress
	      << "," << kWritePMADD << "]" << std::endl;
  }
  // In case we didn't get a valid call 
  ret.setValue(-1);
  return &ret;
}

//____________________________________________________________________
const IntegerVar*
Pvss::Altro::SetAddress(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Altro::SetAddress()");
  CHECK_CALL(param, kSetAddress, "altroSetAddress");
  static IntegerVar ret;
  ret.setValue(-1);
	
  if (!fAltro) throw interface_missing("ALTRO", "ALTRO");

  UIntegerVar bd;
  UIntegerVar chip;
  UIntegerVar channel;
  BitVar broadcast;

  CheckArg(bd,		param.args->getNext(), param.thread);
  CheckArg(chip,		param.args->getNext(), param.thread);
  CheckArg(channel,	param.args->getNext(), param.thread);
  CheckArg(broadcast, param.args->getNext(), param.thread);

  if(broadcast) {
    fAltro->SetBroadcast();
  } else {
    fAltro->SetAddress(bd, chip, channel);
  }

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Altro::GetAddress(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Altro::GetAddress()");
  CHECK_CALL(param, kGetAddress, "altroGetAddress");
  static IntegerVar ret;
  ret.setValue(-1);

  return &ret;
}
/** Command interfaces */ 
//____________________________________________________________________
const IntegerVar*
Pvss::Altro::ExecWPINC(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Altro::ExecWPINC()");
  CHECK_CALL(param, kExecWPINC, "altroExecWPINC");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::AltroCommand* cmd = fAltro->WPINC();
  if (!cmd) throw interface_missing("WPINC", "ALTRO");

  int r = cmd->Commit();
  if (r != 0) throw r;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Altro::ExecRPINC(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Altro::ExecRPINC()");
  CHECK_CALL(param, kExecRPINC, "altroExecRPINC");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::AltroCommand* cmd = fAltro->RPINC();
  if (!cmd) throw interface_missing("RPINC", "ALTRO");

  int r = cmd->Commit();
  if (r != 0) throw r;
  ret.setValue(r);


  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Altro::ExecCHRDO(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Altro::ExecCHRDO()");
  CHECK_CALL(param, kExecCHRDO, "altroExecCHRDO");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::AltroCommand* cmd = fAltro->CHRDO();
  if (!cmd) throw interface_missing("CHRDO", "ALTRO");

  int r = cmd->Commit();
  if (r != 0) throw r;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Altro::ExecSWTRG(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Altro::ExecSWTRG()");
  CHECK_CALL(param, kExecSWTRG, "altroExecSWTRG");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::AltroCommand* cmd = fAltro->SWTRG();
  if (!cmd) throw interface_missing("SWTRG", "ALTRO");

  int r = cmd->Commit();
  if (r != 0) throw r;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Altro::ExecTRCLR(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Altro::ExecTRCLR()");
  CHECK_CALL(param, kExecTRCLR, "altroExecTRCLR");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::AltroCommand* cmd = fAltro->TRCLR();
  if (!cmd) throw interface_missing("TRCLR", "ALTRO");

  int r = cmd->Commit();
  if (r != 0) throw r;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Altro::ExecERCLR(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Altro::ExecERCLR()");
  CHECK_CALL(param, kExecERCLR, "altroExecERCLR");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::AltroCommand* cmd = fAltro->ERCLR();
  if (!cmd) throw interface_missing("ERCLR", "ALTRO");

  int r = cmd->Commit();
  if (r != 0) throw r;
  ret.setValue(r);

  return &ret;
}

//____________________________________________________________________
const IntegerVar*
Pvss::Altro::ReadK1(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Altro::ReadK1()");
  CHECK_CALL(param, kReadK1, "altroReadK1");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::AltroKLCoeffs* reg = fAltro->K1();
  if (!reg) throw interface_missing("K1", "ALTRO");

  UIntegerVar* v;
  CheckArg(v, param.args->getNext(), param.thread);

  int r = reg->Update();
  if (r != 0) throw r;
  ret.setValue(r);

  v->setValue( reg->Value() );

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Altro::ReadK2(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Altro::ReadK2()");
  CHECK_CALL(param, kReadK2, "altroReadK2");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::AltroKLCoeffs* reg = fAltro->K2();
  if (!reg) throw interface_missing("K2", "ALTRO");

  UIntegerVar* v;
  CheckArg(v, param.args->getNext(), param.thread);

  int r = reg->Update();
  if (r != 0) throw r;
  ret.setValue(r);

  v->setValue( reg->Value() );

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Altro::ReadK3(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Altro::ReadK3()");
  CHECK_CALL(param, kReadK3, "altroReadK3");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::AltroKLCoeffs* reg = fAltro->K3();
  if (!reg) throw interface_missing("K3", "ALTRO");

  UIntegerVar* v;
  CheckArg(v, param.args->getNext(), param.thread);

  int r = reg->Update();
  if (r != 0) throw r;
  ret.setValue(r);

  v->setValue( reg->Value() );

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Altro::ReadL1(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Altro::ReadL1()");
  CHECK_CALL(param, kReadL1, "altroReadL1");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::AltroKLCoeffs* reg = fAltro->L1();
  if (!reg) throw interface_missing("L1", "ALTRO");

  UIntegerVar* v;
  CheckArg(v, param.args->getNext(), param.thread);

  int r = reg->Update();
  if (r != 0) throw r;
  ret.setValue(r);

  v->setValue( reg->Value() );

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Altro::ReadL2(ExecuteParamRec& param)
{

  Rcuxx::DebugGuard g(fgDebug, "Altro::ReadL2()");
  CHECK_CALL(param, kReadL2, "altroReadL2");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::AltroKLCoeffs* reg = fAltro->L2();
  if (!reg) throw interface_missing("L2", "ALTRO");

  UIntegerVar* v;
  CheckArg(v, param.args->getNext(), param.thread);

  int r = reg->Update();
  if (r != 0) throw r;
  ret.setValue(r);

  v->setValue( reg->Value() );

  return &ret;

}
//____________________________________________________________________
const IntegerVar*
Pvss::Altro::ReadL3(ExecuteParamRec& param)
{

  Rcuxx::DebugGuard g(fgDebug, "Altro::ReadL3()");
  CHECK_CALL(param, kReadL3, "altroReadL3");
  static IntegerVar ret;
  ret.setValue(-1);
  Rcuxx::AltroKLCoeffs* reg = fAltro->L3();
  if (!reg) throw interface_missing("L3", "ALTRO");

  UIntegerVar* v;
  CheckArg(v, param.args->getNext(), param.thread);

  int r = reg->Update();
  if (r != 0) throw r;
  ret.setValue(r);

  v->setValue( reg->Value() );

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Altro::ReadVFPED(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Altro::ReadVFPED()");
  CHECK_CALL(param, kReadVFPED, "altroReadVFPED");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::AltroVFPED* reg = fAltro->VFPED();
  if (!reg) throw interface_missing("VFPED", "ALTRO");

  UIntegerVar* vp;
  UIntegerVar* fp;
  CheckArg(vp, param.args->getNext(), param.thread);
  CheckArg(fp, param.args->getNext(), param.thread);

  int r = reg->Update();
  if (r != 0) throw r;
  ret.setValue(r);

  vp->setValue( reg->VP() );
  fp->setValue( reg->FP() );
  return &ret;

}
//____________________________________________________________________
const IntegerVar*
Pvss::Altro::ReadPMDTA(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Altro::ReadPMDTA()");
  CHECK_CALL(param, kReadPMDTA, "altroReadPMDTA");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::AltroPMDTA* reg = fAltro->PMDTA();
  if (!reg) throw interface_missing("PMDTA", "ALTRO");

  UIntegerVar* v;
  CheckArg(v, param.args->getNext(), param.thread);

  int r = reg->Update();
  if (r != 0) throw r;
  ret.setValue(r);

  v->setValue( reg->PMDTA() );

  return &ret;

}
//____________________________________________________________________
const IntegerVar*
Pvss::Altro::ReadZSTHR(ExecuteParamRec& param)
{

  Rcuxx::DebugGuard g(fgDebug, "Altro::ReadZSTHR()");
  CHECK_CALL(param, kReadZSTHR, "altroReadZSTHR");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::AltroZSTHR* reg = fAltro->ZSTHR();
  if (!reg) throw interface_missing("ZSTHR", "ALTRO");

  UIntegerVar* offset;
  UIntegerVar* zs_thr;
  CheckArg(offset, param.args->getNext(), param.thread);
  CheckArg(zs_thr, param.args->getNext(), param.thread);

  int r = reg->Update();
  if (r != 0) throw r;
  ret.setValue(r);

  offset->setValue( reg->Offset());
  zs_thr->setValue( reg->ZS_THR() );
  return &ret;

}
//____________________________________________________________________
const IntegerVar*
Pvss::Altro::ReadBCTHR(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Altro::ReadBCTHR()");
  CHECK_CALL(param, kReadBCTHR, "altroReadBCTHR");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::AltroBCTHR* reg = fAltro->BCTHR();
  if (!reg) throw interface_missing("BCTHR", "ALTRO");

  UIntegerVar* hi;
  UIntegerVar* lo;
  CheckArg(lo, param.args->getNext(), param.thread);
  CheckArg(hi, param.args->getNext(), param.thread);

  int r = reg->Update();
  if (r != 0) throw r;
  ret.setValue(r);

  hi->setValue( reg->THR_HI() );
  lo->setValue( reg->THR_LO() );

  return &ret;

}
//____________________________________________________________________
const IntegerVar*
Pvss::Altro::ReadTRCFG(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Altro::ReadTRCFG()");
  CHECK_CALL(param, kReadTRCFG, "altroReadTRCFG");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::AltroTRCFG* reg = fAltro->TRCFG();
  if (!reg) throw interface_missing("TRCFG", "ALTRO");

  UIntegerVar* b;
  UIntegerVar* e;
  CheckArg(b, param.args->getNext(), param.thread);
  CheckArg(e, param.args->getNext(), param.thread);

  int r = reg->Update();
  if (r != 0) throw r;
  ret.setValue(r);

  b->setValue( reg->ACQ_START() );
  e->setValue( reg->ACQ_END() );
  return &ret;

}
//____________________________________________________________________
const IntegerVar*
Pvss::Altro::ReadDPCFG(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Altro::ReadDPCFG()");
  CHECK_CALL(param, kReadDPCFG, "altroReadDPCFG");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::AltroDPCFG* reg = fAltro->DPCFG();
  if (!reg) throw interface_missing("DPCFG", "ALTRO");

  UIntegerVar*	firstBMode;
  BitVar*			IsFirstBPol;
  UIntegerVar*	SecBPre;
  UIntegerVar*	SecBPost;
  BitVar*			IsSecBEnable;
  UIntegerVar*	ZSPre; 
  UIntegerVar*	ZSPost;
  UIntegerVar*	ZSGlitch;
  BitVar*			IsZSEnable;

  CheckArg(firstBMode,	param.args->getNext(), param.thread);
  CheckArg(IsFirstBPol,	param.args->getNext(), param.thread);
  CheckArg(SecBPre,		param.args->getNext(), param.thread);
  CheckArg(SecBPost,		param.args->getNext(), param.thread);
  CheckArg(IsSecBEnable,	param.args->getNext(), param.thread);
  CheckArg(ZSPre,			param.args->getNext(), param.thread);
  CheckArg(ZSPost,		param.args->getNext(), param.thread);
  CheckArg(ZSGlitch,		param.args->getNext(), param.thread);
  CheckArg(IsZSEnable,	param.args->getNext(), param.thread);

  int r = reg->Update();
  if (r != 0) throw r;
  ret.setValue(r);

  firstBMode->setValue( reg->FirstBMode() );
  IsFirstBPol->setValue(	reg->IsFirstBPol() );
  SecBPre->setValue(		reg->SecondBPre() );
  SecBPost->setValue(		reg->SecondBPost() );
  IsSecBEnable->setValue( reg->IsSecondBEnable() );
  ZSPre->setValue(		reg->ZSPost() );
  ZSPost->setValue(		reg->ZSPre()	);
  ZSGlitch->setValue(		reg->ZSGlitch()	);
  IsZSEnable->setValue(	reg->IsZSEnable()	);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Altro::ReadDPCF2(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Altro::ReadDPCF2()");
  CHECK_CALL(param, kReadDPCF2, "altroReadDPCF2");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::AltroDPCF2* reg = fAltro->DPCF2();
  if (!reg) throw interface_missing("DPCF2", "ALTRO");

  UIntegerVar* ptrg;
  BitVar* IsFlt_en;
  BitVar* IsPwsv;
  UIntegerVar* buff;

  CheckArg(ptrg,	param.args->getNext(), param.thread);
  CheckArg(IsFlt_en,	param.args->getNext(), param.thread);
  CheckArg(IsPwsv,	param.args->getNext(), param.thread);
  CheckArg(buff,	param.args->getNext(), param.thread);

  int r = reg->Update();
  if (r != 0) throw r;
  ret.setValue(r);

  ptrg->setValue( reg->PTRG() );
  IsFlt_en->setValue( reg->IsFLT_EN() );
  IsPwsv->setValue( reg->IsPWSV() );

  if( reg->BUF() == Rcuxx::AltroDPCF2::k8Buffers) buff->setValue( 8 );
  else  buff->setValue( 4 );

	
  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Altro::ReadPMADD(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Altro::ReadPMADD()");
  CHECK_CALL(param, kReadPMADD, "altroReadPMADD");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::AltroPMADD* reg = fAltro->PMADD();
  if (!reg) throw interface_missing("PMADD", "ALTRO");

  UIntegerVar* pmadd;
  CheckArg(pmadd,	param.args->getNext(), param.thread);

  int r = reg->Update();
  if (r != 0) throw r;
  ret.setValue(r);

  pmadd->setValue( reg->PMADD() );

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Altro::ReadERSTR(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Altro::ReadERSTR()");
  CHECK_CALL(param, kReadERSTR, "altroReadERSTR");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::AltroERSTR* reg = fAltro->ERSTR();
  if (!reg) throw interface_missing("ERSTR", "ALTRO");

  BitVar* IsRdo;
  BitVar* IsInt2Seu;
  BitVar* IsInt1Seu;
  BitVar* IsMMU2Seu;
  BitVar* IsMMU1Seu;
  BitVar* IsTrigger;
  BitVar* IsInstr;
  BitVar* IsParity;
  BitVar* IsEmpty;
  BitVar* IsFull;
  UIntegerVar* buf;
  UIntegerVar* wp;
  UIntegerVar* rp;

  CheckArg(IsRdo,	param.args->getNext(), param.thread);
  CheckArg(IsInt2Seu,	param.args->getNext(), param.thread);
  CheckArg(IsInt1Seu,	param.args->getNext(), param.thread);
  CheckArg(IsMMU2Seu,	param.args->getNext(), param.thread);
  CheckArg(IsMMU1Seu,	param.args->getNext(), param.thread);
  CheckArg(IsTrigger,	param.args->getNext(), param.thread);
  CheckArg(IsInstr,	param.args->getNext(), param.thread);
  CheckArg(IsParity,	param.args->getNext(), param.thread);
  CheckArg(IsEmpty,	param.args->getNext(), param.thread);
  CheckArg(IsFull,	param.args->getNext(), param.thread);
  CheckArg(buf,	param.args->getNext(), param.thread);
  CheckArg(wp,	param.args->getNext(), param.thread);
  CheckArg(rp,	param.args->getNext(), param.thread);

  int r = reg->Update();
  if (r != 0) throw r;
  ret.setValue(r);

  IsRdo->setValue(		reg->IsRdo()	);
  IsInt2Seu->setValue(	reg->IsInt2Seu()	);
  IsInt1Seu->setValue(	reg->IsInt1Seu()	);
  IsMMU2Seu->setValue(	reg->IsMMU2Seu()	);
  IsMMU1Seu->setValue(	reg->IsMMU1Seu()	);
  IsTrigger->setValue(	reg->IsTrigger()	);
  IsInstr->setValue(		reg->IsInstruction());
  IsParity->setValue(		reg->IsParity()		);
  IsEmpty->setValue(		reg->IsEmpty()		);
  IsFull->setValue(		reg->IsFull()		);
  buf->setValue(			reg->Buffers()		);
  wp->setValue(			reg->Wp()			);
  rp->setValue(			reg->Rp()			);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Altro::ReadADEVL(ExecuteParamRec& param)
{

  Rcuxx::DebugGuard g(fgDebug, "Altro::ReadADEVL()");
  CHECK_CALL(param, kReadADEVL, "altroReadADEVL");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::AltroADEVL* reg = fAltro->ADEVL();
  if (!reg) throw interface_missing("ADEVL", "ALTRO");
	
  UIntegerVar* hadd;
  UIntegerVar* evl;
  CheckArg(hadd,	param.args->getNext(), param.thread);
  CheckArg(evl,	param.args->getNext(), param.thread);

  int r = reg->Update();
  if (r != 0) throw r;
  ret.setValue(r);


  hadd->setValue( reg->HADD() );
  evl->setValue( reg->EVL() );

  return &ret;

}
//____________________________________________________________________
const IntegerVar*
Pvss::Altro::ReadTRCNT(ExecuteParamRec& param)
{

  Rcuxx::DebugGuard g(fgDebug, "Altro::ReadTRCNT()");
  CHECK_CALL(param, kReadTRCNT, "altroReadTRCNT");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::AltroTRCNT* reg = fAltro->TRCNT();
  if (!reg) throw interface_missing("TRCNT", "ALTRO");

  UIntegerVar* v;
  CheckArg(v,	param.args->getNext(), param.thread);

  int r = reg->Update();
  if (r != 0) throw r;
  ret.setValue(r);

  v->setValue( reg->TRCNT() );

  return &ret;

}

//____________________________________________________________________
const IntegerVar*
Pvss::Altro::WriteK1(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Altro::WriteK1()");
  CHECK_CALL(param, kWriteK1, "altroWriteK1");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::AltroKLCoeffs* reg = fAltro->K1();
  if (!reg) throw interface_missing("K1", "ALTRO");

  UIntegerVar v;
  CheckArg(v,  param.args->getNext(), param.thread);
  reg->SetValue( unsigned(v.getValue()) );

  int r = reg->Commit();
  if (r != 0) throw r;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Altro::WriteK2(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Altro::WriteK2()");
  CHECK_CALL(param, kWriteK2, "altroWriteK2");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::AltroKLCoeffs* reg = fAltro->K2();
  if (!reg) throw interface_missing("K2", "ALTRO");
	
  UIntegerVar v;
  CheckArg(v,  param.args->getNext(), param.thread);
  reg->SetValue( unsigned(v.getValue()) );

  int r = reg->Commit();
  if (r != 0) throw r;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Altro::WriteK3(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Altro::WriteK3()");
  CHECK_CALL(param, kWriteK3, "altroWriteK3");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::AltroKLCoeffs* reg = fAltro->K3();
  if (!reg) throw interface_missing("K3", "ALTRO");

  UIntegerVar v;
  CheckArg(v,  param.args->getNext(), param.thread);
  reg->SetValue( unsigned(v.getValue()) );

  int r = reg->Commit();
  if (r != 0) throw r;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Altro::WriteL1(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Altro::WriteL1()");
  CHECK_CALL(param, kWriteL1, "altroWriteL1");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::AltroKLCoeffs* reg = fAltro->L1();
  if (!reg) throw interface_missing("L1", "ALTRO");

  UIntegerVar v;
  CheckArg(v,  param.args->getNext(), param.thread);
  reg->SetValue( unsigned(v.getValue()) );

  int r = reg->Commit();
  if (r != 0) throw r;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Altro::WriteL2(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Altro::WriteL2()");
  CHECK_CALL(param, kWriteL2, "altroWriteL2");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::AltroKLCoeffs* reg = fAltro->L2();
  if (!reg) throw interface_missing("L2", "ALTRO");

  UIntegerVar v;
  CheckArg(v,  param.args->getNext(), param.thread);
  reg->SetValue( unsigned(v.getValue()) );

  int r = reg->Commit();
  if (r != 0) throw r;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Altro::WriteL3(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Altro::WriteL3()");
  CHECK_CALL(param, kWriteL3, "altroWriteL3");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::AltroKLCoeffs* reg = fAltro->L3();
  if (!reg) throw interface_missing("L3", "ALTRO");

  UIntegerVar v;
  CheckArg(v,  param.args->getNext(), param.thread);
  reg->SetValue( unsigned(v.getValue()) );

  int r = reg->Commit();
  if (r != 0) throw r;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Altro::WriteVFPED(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Altro::WriteVFPED()");
  CHECK_CALL(param, kWriteVFPED, "altroWriteVFPED");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::AltroVFPED* reg = fAltro->VFPED();
  if (!reg) throw interface_missing("VFPED", "ALTRO");

  UIntegerVar v;
  CheckArg(v,  param.args->getNext(), param.thread);
  reg->SetFP( v.getValue() );
	
  int r = reg->Commit();
  if (r != 0) throw r;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Altro::WritePMDTA(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Altro::WritePMDTA()");
  CHECK_CALL(param, kWritePMDTA, "altroWritePMDTA");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::AltroPMDTA* reg = fAltro->PMDTA();
  if (!reg) throw interface_missing("PMDTA", "ALTRO");

  UIntegerVar v;
  CheckArg(v,  param.args->getNext(), param.thread);
  reg->SetPMDTA( v.getValue() );

  int r = reg->Commit();
  if (r != 0) throw r;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Altro::WriteZSTHR(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Altro::WriteZSTHR()");
  CHECK_CALL(param, kWriteZSTHR, "altroWriteZSTHR");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::AltroZSTHR* reg = fAltro->ZSTHR();
  if (!reg) throw interface_missing("ZSTHR", "ALTRO");

  UIntegerVar offset;
  UIntegerVar zs_thr;
  CheckArg(offset,  param.args->getNext(), param.thread);
  CheckArg(zs_thr,  param.args->getNext(), param.thread);
  reg->SetOffset( offset.getValue() );
  reg->SetZS_THR(	zs_thr.getValue() );

  int r = reg->Commit();
  if (r != 0) throw r;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Altro::WriteBCTHR(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Altro::WriteBCTHR()");
  CHECK_CALL(param, kWriteBCTHR, "altroWriteBCTHR");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::AltroBCTHR* reg = fAltro->BCTHR();
  if (!reg) throw interface_missing("BCTHR", "ALTRO");

  UIntegerVar lo;
  UIntegerVar hi;
  CheckArg(lo,  param.args->getNext(), param.thread);
  CheckArg(hi,  param.args->getNext(), param.thread);
  reg->SetTHR_LO( lo.getValue() );
  reg->SetTHR_HI( hi.getValue() );

  int r = reg->Commit();
  if (r != 0) throw r;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Altro::WriteTRCFG(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Altro::WriteTRCFG()");
  CHECK_CALL(param, kWriteTRCFG, "altroWriteTRCFG");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::AltroTRCFG* reg = fAltro->TRCFG();
  if (!reg) throw interface_missing("TRCFG", "ALTRO");

  UIntegerVar b;
  UIntegerVar e;
  CheckArg(b,  param.args->getNext(), param.thread);
  CheckArg(e,  param.args->getNext(), param.thread);
  reg->SetACQ_START( b.getValue() );
  reg->SetACQ_END( e.getValue() );

  int r = reg->Commit();
  if (r != 0) throw r;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Altro::WriteDPCFG(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Altro::WriteDPCFG()");
  CHECK_CALL(param, kWriteDPCFG, "altroWriteDPCFG");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::AltroDPCFG* reg = fAltro->DPCFG();
  if (!reg) throw interface_missing("DPCFG", "ALTRO");

  UIntegerVar		firstBMode;
  BitVar			IsFirstBPol;
  UIntegerVar		SecBPre;
  UIntegerVar		SecBPost;
  BitVar			IsSecBEnable;
  UIntegerVar		ZSPre; 
  UIntegerVar		ZSPost;
  UIntegerVar		ZSGlitch;
  BitVar			IsZSEnable;

  CheckArg(firstBMode,	param.args->getNext(), param.thread);
  CheckArg(IsFirstBPol,	param.args->getNext(), param.thread);
  CheckArg(SecBPre,		param.args->getNext(), param.thread);
  CheckArg(SecBPost,		param.args->getNext(), param.thread);
  CheckArg(IsSecBEnable,	param.args->getNext(), param.thread);
  CheckArg(ZSPre,			param.args->getNext(), param.thread);
  CheckArg(ZSPost,		param.args->getNext(), param.thread);
  CheckArg(ZSGlitch,		param.args->getNext(), param.thread);
  CheckArg(IsZSEnable,	param.args->getNext(), param.thread);

  reg->SetFirstBMode(	firstBMode.getValue()	);
  reg->SetFirstBPol(	IsFirstBPol.getValue()	);
  reg->SetSecondBPre(	SecBPre.getValue()		);
  reg->SetSecondBPost(SecBPost.getValue()		);
  reg->SetSecondBEnable(IsSecBEnable.getValue());
  reg->SetZSPre(	ZSPre.getValue()	);
  reg->SetZSPost(	ZSPost.getValue()	);
  reg->SetZSGlitch(ZSGlitch.getValue());
  reg->SetZSEnable(IsZSEnable.getValue());

  int r = reg->Commit();
  if (r != 0) throw r;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Altro::WriteDPCF2(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Altro::WriteDPCF2()");
  CHECK_CALL(param, kWriteDPCF2, "altroWriteDPCF2");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::AltroDPCF2* reg = fAltro->DPCF2();
  if (!reg) throw interface_missing("DPCF2", "ALTRO");

  UIntegerVar ptrg;
  BitVar		IsFlt_en;
  BitVar		IsPwsv;
  UIntegerVar buff;

  CheckArg(ptrg,	param.args->getNext(), param.thread);
  CheckArg(IsFlt_en,	param.args->getNext(), param.thread);
  CheckArg(IsPwsv,	param.args->getNext(), param.thread);
  CheckArg(buff,	param.args->getNext(), param.thread);

  reg->SetPTRG(	ptrg.getValue() );
  reg->SetFLT_EN( IsFlt_en.getValue() );
  reg->SetPWSV(	IsPwsv.getValue() );
	
  if ( buff.getValue() == 4){
    reg->SetBUF( Rcuxx::AltroDPCF2::k4Buffers  );
  } else {
    reg->SetBUF( Rcuxx::AltroDPCF2::k8Buffers  );
  }
  int r = reg->Commit();

  if (r != 0) throw r;
  ret.setValue(r);

  return &ret;

}
//____________________________________________________________________
const IntegerVar*
Pvss::Altro::WritePMADD(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Altro::WritePMADD()");
  CHECK_CALL(param, kWritePMADD, "altroWritePMADD");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::AltroPMADD* reg = fAltro->PMADD();
  if (!reg) throw interface_missing("PMADD", "ALTRO");

  UIntegerVar v;
  CheckArg(v,  param.args->getNext(), param.thread);
  reg->SetPMADD( v.getValue() );

  int r = reg->Commit();
  if (r != 0) throw r;
  ret.setValue(r);

  return &ret;
}


//____________________________________________________________________
//
// EOF
// 
