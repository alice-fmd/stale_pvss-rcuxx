#include "RcuxxExternHdl.h"
#include <iostream>
#include <iomanip>

struct DummyExpr : public CtrlExpr 
{
  DummyExpr(int v=0) : CtrlExpr(-1,-1) { fVar = new IntegerVar(v); }
  ~DummyExpr() { delete fVar; }
  const Variable* evaluate(CtrlThread*) const { return fVar; }
  Variable*       getTarget(CtrlThread*) const { return fVar; }
  ExprType        isA() const { return VAR_EXPR; }
  VariableType    evalTo(CtrlThread*) const { return INTEGER_VAR; }
  void            writeTrace(CtrlThread*,unsigned int) const {}
  CtrlExpr*       getFirstExpr(CtrlThread*) const { return 0; }
  const SmentWeight     getWeight() const { return SM_WT_None; }
  IntegerVar* fVar;
};

int 
main()
{
  RcuxxExternHdl*  rcuxx    = static_cast<RcuxxExternHdl*>(newExternHdl(0));
  FunctionListRec* funcList = rcuxx->getFuncList();
  long             nFunc    = rcuxx->getFunctionCount();
  
  for (size_t i = 0; i < nFunc; i++) {
    FunctionListRec& f = funcList[i];
    // std::cout << "Function # " << std::setw(3) << i << ": " 
    //           << f.name << std::endl;
    ExprList* args = new ExprList;
    DummyExpr expr;
    args->append(&expr);
    BaseExternHdl::ExecuteParamRec param;
    param.funcName   = f.name;
    param.funcNum    = i;
    param.thread     = 0;
    param.clientData = 0;
    param.args       = args;
    
    rcuxx->execute(param);
  }
  return 0;
}

