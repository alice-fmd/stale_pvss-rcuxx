// -*- mode: c++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    pvss-rcuxx/Rcu.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Declaration of Rcu interface 
*/
#ifndef Pvss_Rcu_h
#define Pvss_Rcu_h
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#ifndef RcuxxExternHdl_h
# include "RcuxxExternHdl.h"
#endif
class Variable;
class IntegerVar;
namespace Rcuxx
{
  class Rcu;
}

namespace Pvss
{
  class Altro;
  class Bc;

  /** @class Rcu Rcu.h <pvss-rcuxx/Rcu.h>
      @brief RCU interface 
  */
  class Rcu 
  {
  public:
    /** Type of call parameters */
    typedef BaseExternHdl::ExecuteParamRec ExecuteParamRec;
    /** Enum of possible function calls */
    enum {
      kAddAltro = RcuxxExternHdl::kRcuBase, 
      kAddBc, 
      /** Command access */ 
      kExecABORT,	// ABORT instruction execution 
      kExecCLR_EVTAG,	// CLeaR EVent TAG command
      kExecDCS_ON,	// DCS OwN bus command
      kExecDDL_ON,	// DDL OwN bus command
      kExecEXEC,	// EXECute instructions command
      kExecFECRST,	// Front-End Card ReSeT command
      kExecGLB_RESET,	// GLoBal RESET command
      kExecL1_CMD,	// L1-enable via CoMmanD command
      kExecL1_I2C,	// L1-enable via I2C command
      kExecL1_TTC,	// L1-enable via TTC command
      kExecRCU_RESET,	// RCU RESET command
      kExecRDABORT,	// ReaD-out ABORT command
      kExecRDFM,	// ReaD FirMware command
      kExecRS_DMEM1,	// ReSet DMEM1 command
      kExecRS_DMEM2,	// ReSet DMEM2 command
      kExecRS_STATUS,	// ReSet STATUS command
      kExecRS_TRCFG,	// ReSet TRigger ConFiGuration command
      kExecRS_TRCNT,	// ReSet TRigger CouNTer command
      kExecSCCOMMAND,	// Slow Control COMMAND command
      kExecSWTRG,	// SoftWare TRiGger command
      kExecTRG_CLR,	// TRiGger CLeaR command
      kExecWRFM,	// WRite FirMware command
      kExecRS_ERRREG,	// ReSet ERRor REGister
      /** Register read access */       
      kReadERRST,	// ERRor and STatus register.  
      kReadTRCNT,	// TRigger CouNTers.  
      kReadLWADD,	// Last Written ADDress in the RcuDMEM1 and RcuDMEM2.  
      kReadIRADD,	// Last executed ALTRO InstRuction ADDress.  
      kReadIRDAT,	// Last executed ALTRO InstRuction DATa.
      kReadEVWORD,	// EVent WORD register.  
      kReadACTFEC,	// ACTive FrontEnd Card.  
      kReadRDOFEC,	// ReaDOut FrontEnd Card register.  
      kReadTRCFG1,	// TRigger ConFiG register.  
      kReadTRCFG2,	// ReaD-Out MODe register.  
      kReadFMIREG,	// FirMware Input ReGister.
      kReadFMOREG,	// FirMware Output ReGister 
      kReadPMCFG,	// Pedestal Memory ConFiGuration
      kReadCHADD,	// CHannel Address register 
      kReadINTREG,	// INTerrupt REGister 
      kReadRESREG,	// RESult REGister 
      kReadERRREG,	// ERRor REGister 
      kReadINTMOD,	// INTerrupt MODe register 
      kReadStatusCount, // 
      kReadStatusEntry, // 
	  kReadACL,		//
	  kReadIMEM,	//
	  kReadPMEM,	//
      /** Register write access */       
      kWriteACTFEC,	// ACTive FrontEnd Card.  
      kWriteTRCFG1,	// TRigger ConFiG register.  
      kWriteTRCFG2,	// Write-Out MODe register.  
      kWriteFMIREG,	// FirMware Input ReGister.
      kWriteFMOREG,	// FirMware Output ReGister 
      kWritePMCFG,	// Pedestal Memory ConFiGuration
      kWriteRESREG,	// RESult REGister 
      kWriteINTMOD,	// INTerrupt MODe register 
	  kWriteACL,	// write ACL data
	  kWriteIMEM,	// write IMEM data
	  kWritePMEM,	// write PMEM data
	  kZeroACL,		// zero IMEM data
	  kZeroIMEM,	// zero ACL data
	  kZeroPMEM,	// zero PMEM data
	  kSizeOfACL,	// return the size of the ACL data cache
	  kSizeOfIMEM,	// return the size of the IMEM data cache
	  kSizeOfPMEM,	// return the size of the PMEM data cache
      /** Flag for BC function off-set */
      kAltroBase
    };

    /** Constructor 
	@param rcu Pointer to low-level access */
    Rcu(Rcuxx::Rcu* rcu);
    /** Destructor */
    virtual ~Rcu();

    /** Execute a function.  This is delegated from the handler to
	here. Note, when we get here, at least one parameter is read
	from the execution structore, so we should use `getNext', not
	`getFirst'.  
	@param param Parameters of the function call 
	@return A pointer to the return value */
    virtual const Variable *execute(ExecuteParamRec &param);

  protected:
    /** Add an ALTRO interface */
    const IntegerVar* AddAltro(ExecuteParamRec& param);
    /** Add a BC interface */
    const IntegerVar* AddBc(ExecuteParamRec& param);

    /** Command access */ 
    /** ABORT instruction execution  */
    const IntegerVar* ExecABORT(ExecuteParamRec& param);
    /** CLeaR EVent TAG command */
    const IntegerVar* ExecCLR_EVTAG(ExecuteParamRec& param);
    /** DCS OwN bus command */
    const IntegerVar* ExecDCS_ON(ExecuteParamRec& param);
    /** DDL OwN bus command */
    const IntegerVar* ExecDDL_ON(ExecuteParamRec& param);
    /** EXECute instructions command */
    const IntegerVar* ExecEXEC(ExecuteParamRec& param);
    /** Front-End Card ReSeT command */
    const IntegerVar* ExecFECRST(ExecuteParamRec& param);
    /** GLoBal RESET command */
    const IntegerVar* ExecGLB_RESET(ExecuteParamRec& param);
    /** L1-enable via CoMmanD command */
    const IntegerVar* ExecL1_CMD(ExecuteParamRec& param);
    /** L1-enable via I2C command */
    const IntegerVar* ExecL1_I2C(ExecuteParamRec& param);
    /** L1-enable via TTC command */
    const IntegerVar* ExecL1_TTC(ExecuteParamRec& param);
    /** RCU RESET command */
    const IntegerVar* ExecRCU_RESET(ExecuteParamRec& param);
    /** ReaD-out ABORT command */
    const IntegerVar* ExecRDABORT(ExecuteParamRec& param);
    /** ReaD FirMware command */
    const IntegerVar* ExecRDFM(ExecuteParamRec& param);
    /** ReSet DMEM1 command */
    const IntegerVar* ExecRS_DMEM1(ExecuteParamRec& param);
    /** ReSet DMEM2 command */
    const IntegerVar* ExecRS_DMEM2(ExecuteParamRec& param);
    /** ReSet STATUS command */
    const IntegerVar* ExecRS_STATUS(ExecuteParamRec& param);
    /** ReSet TRigger ConFiGuration command */
    const IntegerVar* ExecRS_TRCFG(ExecuteParamRec& param);
    /** ReSet TRigger CouNTer command */
    const IntegerVar* ExecRS_TRCNT(ExecuteParamRec& param);
    /** Slow Control COMMAND command */
    const IntegerVar* ExecSCCOMMAND(ExecuteParamRec& param);
    /** SoftWare TRiGger command */
    const IntegerVar* ExecSWTRG(ExecuteParamRec& param);
    /** TRiGger CLeaR command */
    const IntegerVar* ExecTRG_CLR(ExecuteParamRec& param);
    /** WRite FirMware command */
    const IntegerVar* ExecWRFM(ExecuteParamRec& param);
    /** ReSet ERRor REGister */
    const IntegerVar* ExecRS_ERRREG(ExecuteParamRec& param);
    /** Register read access */       
    /** ERRor and STatus register.   */
    const IntegerVar* ReadERRST(ExecuteParamRec& param);
    /** TRigger CouNTers.   */
    const IntegerVar* ReadTRCNT(ExecuteParamRec& param);
    /** Last Written ADDress in the RcuDMEM1 and RcuDMEM2.   */
    const IntegerVar* ReadLWADD(ExecuteParamRec& param);
    /** Last executed ALTRO InstRuction ADDress.   */
    const IntegerVar* ReadIRADD(ExecuteParamRec& param);
    /** Last executed ALTRO InstRuction DATa. */
    const IntegerVar* ReadIRDAT(ExecuteParamRec& param);
    /** EVent WORD register.   */
    const IntegerVar* ReadEVWORD(ExecuteParamRec& param);
    /** ACTive FrontEnd Card.   */
    const IntegerVar* ReadACTFEC(ExecuteParamRec& param);
    /** ReaDOut FrontEnd Card register.   */
    const IntegerVar* ReadRDOFEC(ExecuteParamRec& param);
    /** TRigger ConFiG register.   */
    const IntegerVar* ReadTRCFG1(ExecuteParamRec& param);
    /** ReaD-Out MODe register.   */
    const IntegerVar* ReadTRCFG2(ExecuteParamRec& param);
    /** FirMware Input ReGister. */
    const IntegerVar* ReadFMIREG(ExecuteParamRec& param);
    /** FirMware Output ReGister  */
    const IntegerVar* ReadFMOREG(ExecuteParamRec& param);
    /** Pedestal Memory ConFiGuration */
    const IntegerVar* ReadPMCFG(ExecuteParamRec& param);
    /** CHannel Address register  */
    const IntegerVar* ReadCHADD(ExecuteParamRec& param);
    /** INTerrupt REGister  */
    const IntegerVar* ReadINTREG(ExecuteParamRec& param);
    /** RESult REGister  */
    const IntegerVar* ReadRESREG(ExecuteParamRec& param);
    /** ERRor REGister  */
    const IntegerVar* ReadERRREG(ExecuteParamRec& param);
    /** INTerrupt MODe register  */
    const IntegerVar* ReadINTMOD(ExecuteParamRec& param);
    const IntegerVar* ReadStatusCount(ExecuteParamRec& param);
    const IntegerVar* ReadStatusEntry(ExecuteParamRec& param);
    /** Register write access */       
    /** ACTive FrontEnd Card.   */
    const IntegerVar* WriteACTFEC(ExecuteParamRec& param);
    /** TRigger ConFiG register.   */
    const IntegerVar* WriteTRCFG1(ExecuteParamRec& param);
    /** Write-Out MODe register.   */
    const IntegerVar* WriteTRCFG2(ExecuteParamRec& param);
    /** FirMware Input ReGister. */
    const IntegerVar* WriteFMIREG(ExecuteParamRec& param);
    /** FirMware Output ReGister  */
    const IntegerVar* WriteFMOREG(ExecuteParamRec& param);
    /** Pedestal Memory ConFiGuration */
    const IntegerVar* WritePMCFG(ExecuteParamRec& param);
    /** RESult REGister  */
    const IntegerVar* WriteRESREG(ExecuteParamRec& param);
    /** INTerrupt MODe register  */
    const IntegerVar* WriteINTMOD(ExecuteParamRec& param);
	/**  */
    const IntegerVar* WriteACL(ExecuteParamRec& param);
	/**  */
    const IntegerVar* WriteIMEM(ExecuteParamRec& param);
	/**  */
    const IntegerVar* WritePMEM(ExecuteParamRec& param);
	/**  */
	const IntegerVar* ReadACL(ExecuteParamRec& param);
	/**  */
	const IntegerVar* ReadIMEM(ExecuteParamRec& param);
	/**  */
	const IntegerVar* ReadPMEM(ExecuteParamRec& param);
	/**  */
	const IntegerVar* SizeOfACL(ExecuteParamRec& param);
	/**  */
	const IntegerVar* SizeOfIMEM(ExecuteParamRec& param);
	/**  */
	const IntegerVar* SizeOfPMEM(ExecuteParamRec& param);
	/**  */
	const IntegerVar* ZeroACL(ExecuteParamRec& param);
	/**  */
	const IntegerVar* ZeroIMEM(ExecuteParamRec& param);
	/**  */
	const IntegerVar* ZeroPMEM(ExecuteParamRec& param);

	/** Write an Array.  Take the array from the PVSS parameters
	    and put them in @a data, masked by the bit mask in @a mask 
		@param mask Bit mask. 
		@param param PVSS parameters. 
		@param data Data array to be filled. 
		@param base Base address from PVSS arguments. 
	*/
	void WriteArray(const unsigned int          mask,
					ExecuteParamRec&            param, 
					std::vector<unsigned int>&  data, 
					unsigned int&               base);
    /** Read an Array.  Take the array from the PVSS parameters
	    and put them in @a data, masked by the bit mask in @a mask 
		@param mask Bit mask. 
		@param param PVSS parameters. 
		@param data Data array to be filled. 
	*/
	void ReadArray(const unsigned int                mask,
				   ExecuteParamRec&                  param, 
				   const std::vector<unsigned int>&  data);

    /** Pointer to low-level access */
    Rcuxx::Rcu* fRcu;
    /** Pointer to ALTRO inteface */
    Altro*      fAltro;
    /** Pointer to BC interface */
    Bc*         fBc;
  };
}

#endif
//
// EOF
//
