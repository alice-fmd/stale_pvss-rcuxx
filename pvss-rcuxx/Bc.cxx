//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    pvss-rcuxx/Bc.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of RCU interface 
*/
#include <stdexcept>
#include "Bc.h"
#include "Util.h"
#include <rcuxx/Bc.h>
#include <rcuxx/Fmd.h>

namespace 
{
#ifdef NO_CHECK
  bool fgDebug = true;
#else
  bool fgDebug = false;
#endif
}

//____________________________________________________________________
Pvss::Bc::Bc(Rcuxx::Bc* bc) 
  : fBc(bc)
{
  Rcuxx::DebugGuard g(fgDebug, "Bc::Bc(%p)", bc);
  if (!fBc) throw std::runtime_error("No BC interface");
}

//____________________________________________________________________
Pvss::Bc::~Bc()
{
  Rcuxx::DebugGuard g(fgDebug, "Bc::~Bc()");
  if (fBc) delete fBc;
}

//____________________________________________________________________
const Variable* 
Pvss::Bc::execute(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, 
		      "Bc::execute [# %d -> name: %s, # args %d]", 
		      param.funcNum, param.funcName.c_str(), 
		      (param.args ? param.args->getNumberOfItems() : 0));

  static IntegerVar ret;
  switch (param.funcNum) {
    /** Address interface */ 
  case kSetAddress:	return SetAddress(param);
  case kGetAddress:	return GetAddress(param);
    /** Command interface  */
  case kExecCNTLAT:	return ExecCNTLAT(param);
  case kExecCNTCLR:	return ExecCNTCLR(param);
  case kExecCSR1CLR:	return ExecCSR1CLR(param);
  case kExecALRST:	return ExecALRST(param);
  case kExecBCRST:	return ExecBCRST(param);
  case kExecSTCNV:	return ExecSTCNV(param);
  case kExecSCEVL:	return ExecSCEVL(param);
  case kExecEVLRDO:	return ExecEVLRDO(param);
  case kExecSTTSM:	return ExecSTTSM(param);
  case kExecACQRDO:	return ExecACQRDO(param);
    /** Register read access */
  case kReadTEMP_TH:	return ReadTEMP_TH(param);
  case kReadAV_TH:	return ReadAV_TH(param);
  case kReadAC_TH:	return ReadAC_TH(param);
  case kReadDV_TH:	return ReadDV_TH(param);
  case kReadDC_TH:	return ReadDC_TH(param);
  case kReadTEMP:	return ReadTEMP(param);
  case kReadAV:		return ReadAV(param);
  case kReadAC:		return ReadAC(param);
  case kReadDV:		return ReadDV(param);
  case kReadDC:		return ReadDC(param);
  case kReadL1CNT:	return ReadL1CNT(param);
  case kReadL2CNT:	return ReadL2CNT(param);
  case kReadDSTBCNT:	return ReadDSTBCNT(param);
  case kReadSCLKCNT:	return ReadSCLKCNT(param);
  case kReadTSMWORD:	return ReadTSMWORD(param);
  case kReadUSRATIO:	return ReadUSRATIO(param);
  case kReadCSR0:	return ReadCSR0(param);
  case kReadCSR1:	return ReadCSR1(param);
  case kReadCSR2:	return ReadCSR2(param);
  case kReadCSR3:	return ReadCSR3(param);
    /** Register write access */
  case kWriteTEMP_TH:	return WriteTEMP_TH(param);
  case kWriteAV_TH:	return WriteAV_TH(param);
  case kWriteAC_TH:	return WriteAC_TH(param);
  case kWriteDV_TH:	return WriteDV_TH(param);
  case kWriteDC_TH:	return WriteDC_TH(param);
  case kWriteTSMWORD:	return WriteTSMWORD(param);
  case kWriteUSRATIO:	return WriteUSRATIO(param);
  case kWriteCSR0:	return WriteCSR0(param);
  case kWriteCSR2:	return WriteCSR2(param);
  case kWriteCSR3:	return WriteCSR3(param);
  default:
    std::cerr << "Function # " << param.funcNum 
	      << ": " << param.funcName.c_str() 
	      << " is not a BC function [" << kSetAddress 
	      << "," << kWriteCSR3 << "]" << std::endl;
  }

  // In case we didn't get a valid call 
  ret.setValue(-1);
  return &ret;
}

//____________________________________________________________________
const IntegerVar*
Pvss::Bc::SetAddress(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Bc::SetAddress()");
  CHECK_CALL(param, kSetAddress, "bcSetAddress");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::DebugGuard::Message(fgDebug,"BC interface is %p", fBc); 
  if (!fBc) throw interface_missing("BC", "BC");

  UIntegerVar board;
  BitVar broadcast;
  CheckArg(board,			param.args->getNext(), param.thread);
  CheckArg(broadcast,		param.args->getNext(), param.thread);
	
  Rcuxx::DebugGuard::Message(fgDebug,"Address 0x%x, Broaqdcast %d", 
			     board.getValue(), broadcast.getValue()); 
  if(broadcast.getValue()) {
    Rcuxx::DebugGuard::Message(fgDebug, "Set to broadcast");
    fBc->SetBroadcast();
  } else{
    Rcuxx::DebugGuard::Message(fgDebug, "Set target 0x%x", board.getValue());
    fBc->SetAddress( board.getValue() );
  }
  Rcuxx::DebugGuard::Message(fgDebug,"Board address is now 0x%x",
			     fBc->BoardAddress() );
  ret.setValue(0);
  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Bc::GetAddress(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Bc::GetAddress()");
  CHECK_CALL(param, kGetAddress, "bcGetAddress");
  static IntegerVar ret;
  ret.setValue(-1);

  UIntegerVar board;
  CheckArg(board,	param.args->getNext(), param.thread);
	
  board.setValue( fBc->BoardAddress() );

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Bc::ExecCNTLAT(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Bc::ExecCNTLAT()");
  CHECK_CALL(param, kExecCNTLAT, "bcExecCNTLAT");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::AltroCommand* cmd = fBc->CNTLAT();
  if (!cmd) throw interface_missing("CNTLAT", "BC");

  int r = cmd->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Bc::ExecCNTCLR(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Bc::ExecCNTCLR()");
  CHECK_CALL(param, kExecCNTCLR, "bcExecCNTCLR");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::AltroCommand* cmd = fBc->CNTCLR();
  if (!cmd) throw interface_missing("CNTCLR", "BC");

  int r = cmd->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Bc::ExecCSR1CLR(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Bc::ExecCSR1CLR()");
  CHECK_CALL(param, kExecCSR1CLR, "bcExecCSR1CLR");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::AltroCommand* cmd = fBc->CSR1CLR();
  if (!cmd) throw interface_missing("CSR1CLR", "BC");

  int r = cmd->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Bc::ExecALRST(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Bc::ExecALRST()");
  CHECK_CALL(param, kExecALRST, "bcExecALRST");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::AltroCommand* cmd = fBc->ALRST();
  if (!cmd) throw interface_missing("ALRST", "BC");

  int r = cmd->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Bc::ExecBCRST(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Bc::ExecBCRST()");
  CHECK_CALL(param, kExecBCRST, "bcExecBCRST");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::AltroCommand* cmd = fBc->BCRST();
  if (!cmd) throw interface_missing("BCRST", "BC");

  int r = cmd->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Bc::ExecSTCNV(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Bc::ExecSTCNV()");
  CHECK_CALL(param, kExecSTCNV, "bcExecSTCNV");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::AltroCommand* cmd = fBc->STCNV();
  if (!cmd) throw interface_missing("STCNV", "BC");

  int r = cmd->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Bc::ExecSCEVL(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Bc::ExecSCEVL()");
  CHECK_CALL(param, kExecSCEVL, "bcExecSCEVL");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::AltroCommand* cmd = fBc->SCEVL();
  if (!cmd) throw interface_missing("SCEVL", "BC");

  int r = cmd->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Bc::ExecEVLRDO(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Bc::ExecEVLRDO()");
  CHECK_CALL(param, kExecEVLRDO, "bcExecEVLRDO");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::AltroCommand* cmd = fBc->EVLRDO();
  if (!cmd) throw interface_missing("EVLRDO", "BC");

  int r = cmd->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Bc::ExecSTTSM(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Bc::ExecSTTSM()");
  CHECK_CALL(param, kExecSTTSM, "bcExecSTTSM");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::AltroCommand* cmd = fBc->STTSM();
  if (!cmd) throw interface_missing("STTSM", "BC");

  int r = cmd->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Bc::ExecACQRDO(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Bc::ExecACQRDO()");
  CHECK_CALL(param, kExecACQRDO, "bcExecACQRDO");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::AltroCommand* cmd = fBc->ACQRDO();
  if (!cmd) throw interface_missing("ACQRDO", "BC");

  int r = cmd->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);

  return &ret;
}

//____________________________________________________________________
const IntegerVar*
Pvss::Bc::ReadTEMP_TH(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Bc::ReadTEMP_TH()");
  CHECK_CALL(param, kReadTEMP_TH, "bcReadTEMP_TH");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::BcTEMP_TH* reg = fBc->TEMP_TH();
  if (!reg) throw interface_missing("TEMP_TH", "BC");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Bc::ReadAV_TH(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Bc::ReadAV_TH()");
  CHECK_CALL(param, kReadAV_TH, "bcReadAV_TH");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::BcAV_TH* reg = fBc->AV_TH();
  if (!reg) throw interface_missing("AV_TH", "BC");

  FloatVar* avTh;
  CheckArg(avTh, param.args->getNext(), param.thread);

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  avTh->setValue( reg->MiliVolts() );

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Bc::ReadAC_TH(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Bc::ReadAC_TH()");
  CHECK_CALL(param, kReadAC_TH, "bcReadAC_TH");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::BcAC_TH* reg = fBc->AC_TH();
  if (!reg) throw interface_missing("AC_TH", "BC");

  FloatVar* acTh;
  CheckArg(acTh, param.args->getNext(), param.thread);

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  acTh->setValue( reg->MiliAmps() );
	
  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Bc::ReadDV_TH(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Bc::ReadDV_TH()");
  CHECK_CALL(param, kReadDV_TH, "bcReadDV_TH");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::BcDV_TH* reg = fBc->DV_TH();
  if (!reg) throw interface_missing("DV_TH", "BC");

  FloatVar* dvTh;
  CheckArg(dvTh, param.args->getNext(), param.thread);

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  dvTh->setValue( reg->MiliVolts() );
	
  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Bc::ReadDC_TH(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Bc::ReadDC_TH()");
  CHECK_CALL(param, kReadDC_TH, "bcReadDC_TH");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::BcDC_TH* reg = fBc->DC_TH();
  if (!reg) throw interface_missing("DC_TH", "BC");

  FloatVar* dcTh;
  CheckArg(dcTh, param.args->getNext(), param.thread);

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  dcTh->setValue( reg->MiliAmps() );

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Bc::ReadTEMP(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Bc::ReadTEMP()");
  CHECK_CALL(param, kReadTEMP, "bcReadTEMP");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::BcTEMP* reg = fBc->TEMP();
  if (!reg) throw interface_missing("TEMP", "BC");

  FloatVar* tempTh;
  CheckArg(tempTh, param.args->getNext(), param.thread);
		
  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  tempTh->setValue( reg->Centigrade() );
	
  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Bc::ReadAV(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Bc::ReadAV()");
  CHECK_CALL(param, kReadAV, "bcReadAV");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::BcAV* reg = fBc->AV();
  if (!reg) throw interface_missing("AV", "BC");

  FloatVar* av;
  CheckArg(av, param.args->getNext(), param.thread);

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  av->setValue( reg->MiliVolts() );

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Bc::ReadAC(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Bc::ReadAC()");
  CHECK_CALL(param, kReadAC, "bcReadAC");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::BcAC* reg = fBc->AC();
  if (!reg) throw interface_missing("AC", "BC");

  FloatVar* ac;
  CheckArg(ac, param.args->getNext(), param.thread);

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  ac->setValue( reg->MiliAmps() );

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Bc::ReadDV(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Bc::ReadDV()");
  CHECK_CALL(param, kReadDV, "bcReadDV");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::BcDV* reg = fBc->DV();
  if (!reg) throw interface_missing("DV", "BC");

  FloatVar* dv;
  CheckArg(dv, param.args->getNext(), param.thread);

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  dv->setValue( reg->MiliVolts() );

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Bc::ReadDC(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Bc::ReadDC()");
  CHECK_CALL(param, kReadDC, "bcReadDC");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::BcDC* reg = fBc->DC();
  if (!reg) throw interface_missing("DC", "BC");

  FloatVar* dc;
  CheckArg(dc, param.args->getNext(), param.thread);

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  dc->setValue( reg->MiliAmps() );

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Bc::ReadL1CNT(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Bc::ReadL1CNT()");
  CHECK_CALL(param, kReadL1CNT, "bcReadL1CNT");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::BcL1CNT* reg = fBc->L1CNT();
  if (!reg) throw interface_missing("L1CNT", "BC");

  UIntegerVar* c;
  CheckArg(c, param.args->getNext(), param.thread);

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  c->setValue( reg->Counts() );
	
  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Bc::ReadL2CNT(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Bc::ReadL2CNT()");
  CHECK_CALL(param, kReadL2CNT, "bcReadL2CNT");
  static IntegerVar ret;
  ret.setValue(-1);

  UIntegerVar* c;
  CheckArg(c, param.args->getNext(), param.thread);

  Rcuxx::BcL2CNT* reg = fBc->L2CNT();
  if (!reg) throw interface_missing("L2CNT", "BC");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  c->setValue( reg->Counts() );

  return &ret;
}

//____________________________________________________________________
const IntegerVar*
Pvss::Bc::ReadDSTBCNT(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Bc::ReadDSTBCNT()");
  CHECK_CALL(param, kReadDSTBCNT, "bcReadDSTBCNT");
  static IntegerVar ret;
  ret.setValue(-1);

  UIntegerVar* c;
  CheckArg(c, param.args->getNext(), param.thread);

  Rcuxx::BcDSTBCNT* reg = fBc->DSTBCNT();
  if (!reg) throw interface_missing("DSTBCNT", "BC");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  c->setValue( reg->Counts() );

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Bc::ReadSCLKCNT(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Bc::ReadSCLKCNT()");
  CHECK_CALL(param, kReadSCLKCNT, "bcReadSCLKCNT");
  static IntegerVar ret;
  ret.setValue(-1);

  UIntegerVar* c;
  CheckArg(c, param.args->getNext(), param.thread);

  Rcuxx::BcSCLKCNT* reg = fBc->SCLKCNT();
  if (!reg) throw interface_missing("SCLKCNT", "BC");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  c->setValue( reg->Counts() );

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Bc::ReadTSMWORD(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Bc::ReadTSMWORD()");
  CHECK_CALL(param, kReadTSMWORD, "bcReadTSMWORD");
  static IntegerVar ret;
  ret.setValue(-1);

  UIntegerVar* w;
  CheckArg(w, param.args->getNext(), param.thread);

  Rcuxx::BcTSMWORD* reg = fBc->TSMWORD();
  if (!reg) throw interface_missing("TSMWORD", "BC");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  w->setValue( reg->Number() );

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Bc::ReadUSRATIO(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Bc::ReadUSRATIO()");
  CHECK_CALL(param, kReadUSRATIO, "bcReadUSRATIO");
  static IntegerVar ret;
  ret.setValue(-1);

  UIntegerVar* v;
  CheckArg(v, param.args->getNext(), param.thread);

  Rcuxx::BcUSRATIO* reg = fBc->USRATIO();
  if (!reg) throw interface_missing("USRATIO", "BC");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  v->setValue( reg->Ratio() );

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Bc::ReadCSR0(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Bc::ReadCSR0()");
  CHECK_CALL(param, kReadCSR0, "bcReadCSR0");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::BcCSR0* reg = fBc->CSR0();
  if (!reg) throw interface_missing("CSR0", "BC");

  BitVar* iparity;
  BitVar* iinstr;
  BitVar* isclk;
  BitVar* ialps;
  BitVar* ipaps;
  BitVar* idc;
  BitVar* idv;
  BitVar* iac;
  BitVar* iav;
  BitVar* it;
  BitVar* icnv;

  CheckArg(iparity, param.args->getNext(), param.thread);
  CheckArg(iinstr, param.args->getNext(), param.thread);
  CheckArg(isclk, param.args->getNext(), param.thread);
  CheckArg(ialps, param.args->getNext(), param.thread);
  CheckArg(ipaps, param.args->getNext(), param.thread);
  CheckArg(idc, param.args->getNext(), param.thread);
  CheckArg(idv, param.args->getNext(), param.thread);
  CheckArg(iac, param.args->getNext(), param.thread);
  CheckArg(iav, param.args->getNext(), param.thread);
  CheckArg(it, param.args->getNext(), param.thread);
  CheckArg(icnv, param.args->getNext(), param.thread);

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);

  iparity->setValue( reg->IsParity() );
  iinstr->setValue( reg->IsInstruction() );
  isclk->setValue( reg->IsMissedSclk() );
  ialps->setValue( reg->IsAlps() );
  ipaps->setValue( reg->IsAlps() );	
  idc->setValue( reg->IsPaps() );
  idv->setValue( reg->IsDvOverTh() );
  iac->setValue( reg->IsAcOverTh() );
  iav->setValue( reg->IsAcOverTh() );
  it->setValue( reg->IsTempOverTh() );
  icnv->setValue( reg->IsCnv() );

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Bc::ReadCSR1(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Bc::ReadCSR1()");
  CHECK_CALL(param, kReadCSR1, "bcReadCSR1");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::BcCSR1* reg = fBc->CSR1();
  if (!reg) throw interface_missing("CSR1", "BC");

  BitVar* ierr;
  BitVar* isc;
  BitVar* ialtro;
  BitVar* iinter;
  BitVar* iparity;
  BitVar* iinstr;
  BitVar* isclk;
  BitVar* ialps;
  BitVar* ipaps;
  BitVar* idc;
  BitVar* idv;
  BitVar* iac;
  BitVar* iav;
  BitVar* it;

  //bool& ierr, bool &isc, bool &ialtro, bool &iinter ,bool &iparity, 
  //bool &iinstr, bool &isclk, bool &ialps,
  //bool &ipaps, bool &idc, bool &idv, bool &iac, bool &iav, bool &it
  CheckArg(ierr,		param.args->getNext(), param.thread);
  CheckArg(isc,		param.args->getNext(), param.thread);
  CheckArg(ialtro,		param.args->getNext(), param.thread);
  CheckArg(iinter,		param.args->getNext(), param.thread);
  CheckArg(iparity,		param.args->getNext(), param.thread);
  CheckArg(iinstr,		param.args->getNext(), param.thread);
  CheckArg(isclk,		param.args->getNext(), param.thread);
  CheckArg(ialps,		param.args->getNext(), param.thread);
  CheckArg(ipaps,		param.args->getNext(), param.thread);
  CheckArg(idc,		param.args->getNext(), param.thread);
  CheckArg(idv,		param.args->getNext(), param.thread);
  CheckArg(iac,		param.args->getNext(), param.thread);
  CheckArg(iav,		param.args->getNext(), param.thread);
  CheckArg(it,		param.args->getNext(), param.thread);

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);

  ierr->setValue( reg->IsError() );
  isc->setValue( reg->IsSlowControl() );
  ialtro->setValue( reg->IsAltroError()	);
  iinter->setValue( reg->IsInterrupt() );
  iparity->setValue( reg->IsParity() );
  iinstr->setValue( reg->IsInstruction() );
  isclk->setValue( reg->IsMissedSclk() );
  ialps->setValue( reg->IsAlps() );
  ipaps->setValue( reg->IsAlps() );	
  idc->setValue( reg->IsPaps() );
  idv->setValue( reg->IsDvOverTh() );
  iac->setValue( reg->IsAcOverTh() );
  iav->setValue( reg->IsAcOverTh() );
  it->setValue( reg->IsTempOverTh() );

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Bc::ReadCSR2(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Bc::ReadCSR2()");
  CHECK_CALL(param, kReadCSR2, "bcReadCSR2");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::BcCSR2* reg = fBc->CSR2();
  if (!reg) throw interface_missing("CSR2", "BC");

  UIntegerVar* hadd;
  UIntegerVar* altro_add;
  UIntegerVar* adc_add;
  BitVar* IsIso;
  BitVar* IsContTsm;
  BitVar* IsAdcClk;
  BitVar* IsRdoClk;
  BitVar* IsPasaSw;
  BitVar* IsAltrosSw;

  CheckArg(hadd,		param.args->getNext(), param.thread);
  CheckArg(altro_add, param.args->getNext(), param.thread);
  CheckArg(adc_add, param.args->getNext(), param.thread);
  CheckArg(IsIso,		param.args->getNext(), param.thread);
  CheckArg(IsContTsm, param.args->getNext(), param.thread);
  CheckArg(IsAdcClk,	param.args->getNext(), param.thread);
  CheckArg(IsRdoClk,	param.args->getNext(), param.thread);
  CheckArg(IsPasaSw,	param.args->getNext(), param.thread);
  CheckArg(IsAltrosSw,param.args->getNext(), param.thread);

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);

  hadd->setValue(			reg->HADD()			);
  altro_add->setValue(	reg->ALTROAddress()	);
  adc_add->setValue(		reg->ADCAddress()	);
  IsIso->setValue(		reg->IsCardIsolated() );
  IsContTsm->setValue(	reg->IsContinousTSM() );
  IsAdcClk->setValue(		reg->IsAdcClock()	);
  IsRdoClk->setValue(		reg->IsRdoClock()	);
  IsPasaSw->setValue(		reg->IsPASASwitch()	);
  IsAltrosSw->setValue(	reg->IsALTROSwitch());

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Bc::ReadCSR3(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Bc::ReadCSR3()");
  CHECK_CALL(param, kReadCSR3, "bcReadCSR3");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::BcCSR3* reg = fBc->CSR3();
  if (!reg) throw interface_missing("CSR3", "BC");
	
  UIntegerVar* war;
  UIntegerVar* watchdog;
  BitVar* IsCnv;

  CheckArg(war, param.args->getNext(), param.thread);
  CheckArg(watchdog, param.args->getNext(), param.thread);
  CheckArg(IsCnv, param.args->getNext(), param.thread);

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
	
  war->setValue( reg->WarnRatio() );
  watchdog->setValue( reg->WatchDog() );
  IsCnv->setValue( reg->IsCnvEnd() );

  return &ret;
}

//____________________________________________________________________
const IntegerVar*
Pvss::Bc::WriteTEMP_TH(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Bc::WriteTEMP_TH()");
  CHECK_CALL(param, kWriteTEMP_TH, "bcWriteTEMP_TH");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::BcTEMP_TH* reg = fBc->TEMP_TH();
  if (!reg) throw interface_missing("TEMP_TH", "BC");

  FloatVar t;
  CheckArg(t, param.args->getNext(), param.thread);
  reg->SetCentigrade( t.getValue() );

  int r = reg->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);
	
  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Bc::WriteAV_TH(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Bc::WriteAV_TH()");
  CHECK_CALL(param, kWriteAV_TH, "bcWriteAV_TH");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::BcAV_TH* reg = fBc->AV_TH();
  if (!reg) throw interface_missing("AV_TH", "BC");

  FloatVar th;
  CheckArg(th, param.args->getNext(), param.thread);
  reg->SetMiliVolts( th.getValue() );

  int r = reg->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);
	
  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Bc::WriteAC_TH(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Bc::WriteAC_TH()");
  CHECK_CALL(param, kWriteAC_TH, "bcWriteAC_TH");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::BcAC_TH* reg = fBc->AC_TH();
  if (!reg) throw interface_missing("AC_TH", "BC");

  FloatVar th;
  CheckArg(th, param.args->getNext(), param.thread);
  reg->SetMiliAmps( th.getValue() );

  int r = reg->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);
	
  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Bc::WriteDV_TH(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Bc::WriteDV_TH()");
  CHECK_CALL(param, kWriteDV_TH, "bcWriteDV_TH");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::BcDV_TH* reg = fBc->DV_TH();
  if (!reg) throw interface_missing("DV_TH", "BC");

  FloatVar th;
  CheckArg(th, param.args->getNext(), param.thread);
  reg->SetMiliVolts( th.getValue() );

  int r = reg->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);
	
  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Bc::WriteDC_TH(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Bc::WriteDC_TH()");
  CHECK_CALL(param, kWriteDC_TH, "bcWriteDC_TH");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::BcDC_TH* reg = fBc->DC_TH();
  if (!reg) throw interface_missing("DC_TH", "BC");

  FloatVar th;
  CheckArg(th, param.args->getNext(), param.thread);
  reg->SetMiliAmps( th.getValue() );

  int r = reg->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);
	
  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Bc::WriteTSMWORD(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Bc::WriteTSMWORD()");
  CHECK_CALL(param, kWriteTSMWORD, "bcWriteTSMWORD");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::BcTSMWORD* reg = fBc->TSMWORD();
  if (!reg) throw interface_missing("TSMWORD", "BC");

  UIntegerVar w;
  CheckArg(w, param.args->getNext(), param.thread);
  reg->SetNumber( w.getValue() );

  int r = reg->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);
	
  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Bc::WriteUSRATIO(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Bc::WriteUSRATIO()");
  CHECK_CALL(param, kWriteUSRATIO, "bcWriteUSRATIO");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::BcUSRATIO* reg = fBc->USRATIO();
  if (!reg) throw interface_missing("USRATIO", "BC");

  UIntegerVar v;
  CheckArg(v, param.args->getNext(), param.thread);
  reg->SetRatio( v.getValue() );

  int r = reg->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);
	
  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Bc::WriteCSR0(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Bc::WriteCSR0()");
  CHECK_CALL(param, kWriteCSR0, "bcWriteCSR0");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::BcCSR0* reg = fBc->CSR0();
  if (!reg) throw interface_missing("CSR0", "BC");

  BitVar* parity;
  BitVar* instr;
  BitVar* sclk;
  BitVar* alps;
  BitVar* paps;
  BitVar* dc;
  BitVar* dv;
  BitVar* ac;
  BitVar* av;
  BitVar* t;
  BitVar* cnv;

  CheckArg(parity, param.args->getNext(), param.thread);
  CheckArg(instr, param.args->getNext(), param.thread);
  CheckArg(sclk, param.args->getNext(), param.thread);
  CheckArg(alps, param.args->getNext(), param.thread);
  CheckArg(paps, param.args->getNext(), param.thread);
  CheckArg(dc, param.args->getNext(), param.thread);
  CheckArg(dv, param.args->getNext(), param.thread);
  CheckArg(ac, param.args->getNext(), param.thread);
  CheckArg(av, param.args->getNext(), param.thread);
  CheckArg(t, param.args->getNext(), param.thread);
  CheckArg(cnv, param.args->getNext(), param.thread);

  reg->SetParity(parity);
  reg->SetInstruction(instr);
  reg->SetMissedSclk(sclk);
  reg->SetAlps(alps);
  reg->SetPaps(paps);
  reg->SetDcOverTh(dc);
  reg->SetDvOverTh(dv);
  reg->SetAcOverTh(av);
  reg->SetAcOverTh(ac);
  reg->SetTempOverTh(t);
  reg->SetCnv(cnv);

  int r = reg->Commit();
  if (r != 0) throw ret;
  ret.setValue(ret);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Bc::WriteCSR2(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Bc::WriteCSR2()");
  CHECK_CALL(param, kWriteCSR2, "bcWriteCSR2");
  static IntegerVar ret;
  ret.setValue(-1);
	
  //UIntegerVar hadd,
  UIntegerVar altro_add;
  UIntegerVar adc_add;
  UIntegerVar IsIso;
  BitVar IsContTsm;
  BitVar IsAdcClk; 
  BitVar IsRdoClk;
  BitVar IsPasaSw;
  BitVar IsAltrosSw;
	
  //CheckArg(hadd,		param.args->getNext(), param.thread);
  CheckArg(altro_add, param.args->getNext(), param.thread);
  CheckArg(adc_add,   param.args->getNext(), param.thread);
  CheckArg(IsIso,		param.args->getNext(), param.thread);
  CheckArg(IsContTsm, param.args->getNext(), param.thread);
  CheckArg(IsAdcClk,	param.args->getNext(), param.thread);
  CheckArg(IsRdoClk,	param.args->getNext(), param.thread);
  CheckArg(IsPasaSw,	param.args->getNext(), param.thread);
  CheckArg(IsAltrosSw,param.args->getNext(), param.thread);

  Rcuxx::BcCSR2* reg = fBc->CSR2();
  if (!reg) throw interface_missing("CSR2", "BC");

  reg->SetALTROAddress(	altro_add.getValue() );
  reg->SetADCAddress(		adc_add.getValue() );
  reg->SetCardIsolated( 	IsIso.getValue() );
  reg->SetContinousTSM(	IsContTsm.getValue() );
  reg->SetAdcClock(		IsAdcClk.getValue() );
  reg->SetRdoClock(		IsRdoClk.getValue() );
  reg->SetPASASwitch(		IsPasaSw.getValue() );
  reg->SetALTROSwitch(	IsAltrosSw.getValue() );

  int r = reg->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);
	
  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Bc::WriteCSR3(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Bc::WriteCSR3()");
  CHECK_CALL(param, kWriteCSR3, "bcWriteCSR3");
  static IntegerVar ret;
  ret.setValue(-1);
	
  UIntegerVar war;
  UIntegerVar watchdog;

  CheckArg(war, param.args->getNext(), param.thread);
  CheckArg(watchdog, param.args->getNext(), param.thread);

  Rcuxx::BcCSR3* reg = fBc->CSR3();
  if (!reg) throw interface_missing("CSR3", "BC");

  reg->SetWarnRatio( war.getValue() );
  reg->SetWatchDog( watchdog.getValue() );
	
  int r = reg->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);
	
  return &ret;
}
//____________________________________________________________________
//
// EOF
// 
