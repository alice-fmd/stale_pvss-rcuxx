//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    pvss-rcuxx/Fmd.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of RCU interface 
*/
#include <stdexcept>
#include "Fmd.h"
#include "Util.h"
#include <rcuxx/Bc.h>
#include <rcuxx/Fmd.h>

namespace 
{
#ifdef NO_CHECK
  bool fgDebug = true;
#else
  bool fgDebug = false;
#endif
}

//____________________________________________________________________
Pvss::Fmd::Fmd(Rcuxx::Bc* bc) 
  : Bc(bc)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::Fmd(%p)", bc);
  fFmd = static_cast<Rcuxx::Fmd*>(fBc);
}

//____________________________________________________________________
Pvss::Fmd::~Fmd()
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::~Fmd()");
  fFmd = 0;
}

//____________________________________________________________________
const Variable* 
Pvss::Fmd::execute(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, 
		      "Fmd::execute [# %d -> name: %s, # args %d]", 
		      param.funcNum, param.funcName.c_str(), 
		      (param.args ? param.args->getNumberOfItems() : 0));

  static IntegerVar ret;
  switch (param.funcNum) {
    /** Address interface */ 
  case kReadTEMP_TH:    break; // Invalid for FMDD
  case kReadAV_TH:	break; // Invalid for FMDD
  case kReadAC_TH:	break; // Invalid for FMDD
  case kReadDV_TH:	break; // Invalid for FMDD
  case kReadDC_TH:	break; // Invalid for FMDD
  case kReadTEMP:	break; // Invalid for FMDD
  case kReadAV:		break; // Invalid for FMDD
  case kReadAC:		break; // Invalid for FMDD
  case kReadDV:		break; // Invalid for FMDD
  case kReadDC:		break; // Invalid for FMDD
    /** Command fmdd */
  case kExecFakeTrigger:	return  ExecFakeTrigger(param);
  case kExecSoftReset:		return  ExecSoftReset(param);
  case kExecChangeDacs:		return  ExecChangeDacs(param);
  case kExecPulserOn:		return  ExecPulserOn(param);
  case kExecPulserOff:		return  ExecPulserOff(param);
  case kExecTestOn:		return  ExecTestOn(param);
  case kExecTestOff:		return  ExecTestOff(param);
  case kExecCalibrationRun:	return  ExecCalibrationRun(param);
    /** Register fmdd read access */
  case kReadL0CNT:		return  ReadL0CNT(param);
  case kReadAL_DIG_I:		return  ReadAL_DIG_I(param);
  case kReadAL_DIG_I_TH:	return	ReadAL_DIG_I_TH(param);
  case kReadAL_ANA_I:		return	ReadAL_ANA_I(param);
  case kReadAL_ANA_I_TH:	return	ReadAL_ANA_I_TH(param);
  case kReadAL_DIG_U:		return	ReadAL_DIG_U(param);
  case kReadAL_DIG_U_TH:	return	ReadAL_DIG_U_TH(param);
  case kReadAL_ANA_U:		return	ReadAL_ANA_U(param);
  case kReadAL_ANA_U_TH:	return	ReadAL_ANA_U_TH(param);
  case kReadT1SENS:		return	ReadT1SENS(param);
  case kReadT1SENS_TH:		return	ReadT1SENS_TH(param);
  case kReadT2SENS:		return  ReadT2SENS(param);
  case kReadT2SENS_TH:		return	ReadT2SENS_TH(param);
  case kReadT1:			return	ReadT1(param);
  case kReadT1_TH:		return	ReadT1_TH(param);
  case kReadT2:			return	ReadT2(param);
  case kReadT2_TH:		return	ReadT2_TH(param);
  case kReadT3:			return	ReadT3(param);
  case kReadT3_TH:		return	ReadT3_TH(param);
  case kReadT4:			return	ReadT4(param);
  case kReadT4_TH:		return	ReadT4_TH(param);	 
  case kReadVA_REC_IP:		return	ReadVA_REC_IP(param);
  case kReadVA_REC_IP_TH:	return  ReadVA_REC_IP_TH(param);
  case kReadVA_REC_UP:		return	ReadVA_REC_UP(param);
  case kReadVA_REC_UP_TH:	return  ReadVA_REC_UP_TH(param);
  case kReadVA_REC_IM:		return	ReadVA_REC_IM(param);
  case kReadVA_REC_IM_TH:	return  ReadVA_REC_IM_TH(param);
  case kReadVA_REC_UM:		return	ReadVA_REC_UM(param);
  case kReadVA_REC_UM_TH:	return  ReadVA_REC_UM_TH(param);
  case kReadVA_SUP_IP:		return  ReadVA_SUP_IP(param);
  case kReadVA_SUP_IP_TH:	return  ReadVA_SUP_IP_TH(param);
  case kReadVA_SUP_UP:		return	ReadVA_SUP_UP(param);
  case kReadVA_SUP_UP_TH:	return  ReadVA_SUP_UP_TH(param);
  case kReadVA_SUP_IM:		return	ReadVA_SUP_IM(param);
  case kReadVA_SUP_IM_TH:	return  ReadVA_SUP_IM_TH(param);
  case kReadVA_SUP_UM:		return	ReadVA_SUP_UM(param);
  case kReadVA_SUP_UM_TH:	return  ReadVA_SUP_UM_TH(param);
  case kReadGTL_U:		return  ReadGTL_U(param);
  case kReadGTL_U_TH:		return	ReadGTL_U_TH(param);
  case kReadFLASH_I:		return	ReadFLASH_I(param);
  case kReadFLASH_I_TH:		return	ReadFLASH_I_TH(param);
  case kReadShiftClock:		return	ReadShiftClock(param);
  case kReadRange:		return	ReadRange(param);
  case kReadSampleClock:	return	ReadSampleClock(param);
  case kReadHoldWait:		return	ReadHoldWait(param);
  case kReadL0Timeout:		return	ReadL0Timeout(param);
  case kReadL1Timeout:		return	ReadL1Timeout(param);
  case kReadStatus:		return	ReadStatus(param);
    /** Register fmdd write access */
  case kWriteAL_DIG_I_TH:	return	WriteAL_DIG_I_TH(param);
  case kWriteAL_ANA_I_TH:	return	WriteAL_ANA_I_TH(param);
  case kWriteAL_DIG_U_TH:	return	WriteAL_DIG_U_TH(param);
  case kWriteAL_ANA_U_TH:	return	WriteAL_ANA_U_TH(param);
  case kWriteT1SENS_TH:		return	WriteT1SENS_TH(param);
  case kWriteT2SENS_TH:		return	WriteT2SENS_TH(param);
  case kWriteT1_TH:		return	WriteT1_TH(param);
  case kWriteT2_TH:		return	WriteT2_TH(param);
  case kWriteT3_TH:		return	WriteT3_TH(param);
  case kWriteT4_TH:		return	WriteT4_TH(param);
  case kWriteVA_REC_IP_TH:	return	WriteVA_REC_IP_TH(param);
  case kWriteVA_REC_UP_TH:	return	WriteVA_REC_UP_TH(param);
  case kWriteVA_REC_IM_TH:	return	WriteVA_REC_IM_TH(param);
  case kWriteVA_REC_UM_TH:	return	WriteVA_REC_UM_TH(param);
  case kWriteVA_SUP_IP_TH:	return	WriteVA_SUP_IP_TH(param);
  case kWriteVA_SUP_UP_TH:	return	WriteVA_SUP_UP_TH(param);
  case kWriteVA_SUP_IM_TH:	return	WriteVA_SUP_IM_TH(param);
  case kWriteVA_SUP_UM_TH:	return	WriteVA_SUP_UM_TH(param);
  case kWriteGTL_U_TH:		return	WriteGTL_U_TH(param);
  case kWriteFLASH_I_TH:	return	WriteFLASH_I_TH(param);
  case kWriteShiftClock:	return	WriteShiftClock(param);
  case kWriteRange:		return	WriteRange(param);
  case kWriteSampleClock:	return	WriteSampleClock(param);
  case kWriteHoldWait:		return	WriteHoldWait(param);
  case kWriteL0Timeout:		return	WriteL0Timeout(param);
  case kWriteL1Timeout:		return	WriteL1Timeout(param);
  case kWriteShapeBias:		return	WriteShapeBias(param);
  case kWritePulser:		return	WritePulser(param);
  case kWriteCalIter:		return	WriteCalIter(param);
  case kWriteVFP:		return	WriteVFP(param);
  case kWriteVFS:		return	WriteVFS(param);
  default:                      return  Bc::execute(param);
  }

  // In case we didn't get a valid call 
  ret.setValue(-1);
  return &ret;
}

//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ExecFakeTrigger(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::ExecFakeTrigger()");
  CHECK_CALL(param, kExecFakeTrigger, "fmddExecFakeTrigger");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::AltroCommand* cmd = fFmd->FakeTrigger();
	
  if (!cmd) throw interface_missing("FakeTrigger", "FMD");

  int r = cmd->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ExecSoftReset(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::fmddSoftReset()");
  CHECK_CALL(param, kExecSoftReset, "fmddExecSoftReset");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::AltroCommand* cmd = fFmd->SoftReset();
	
  if (!cmd) throw interface_missing("SoftReset", "FMD");

  int r = cmd->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ExecChangeDacs(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::fmddChangeDacs()");
  CHECK_CALL(param, kExecChangeDacs, "fmddExecChangeDacs");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::AltroCommand* cmd = fFmd->ChangeDacs();
	
  if (!cmd) throw interface_missing("ChangeDacs", "FMD");

  int r = cmd->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ExecPulserOn(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::fmddPulserOn()");
  CHECK_CALL(param, kExecPulserOn, "fmddExecPulserOn");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::AltroCommand* cmd = fFmd->PulserOn();
	
  if (!cmd) throw interface_missing("PulserOn", "FMD");

  int r = cmd->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ExecPulserOff(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::fmddPulserOff()");
  CHECK_CALL(param, kExecPulserOff, "fmddExecPulserOff");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::AltroCommand* cmd = fFmd->PulserOff();
	
  if (!cmd) throw interface_missing("PulserOff", "FMD");

  int r = cmd->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ExecTestOn(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::fmddTestOn()");
  CHECK_CALL(param, kExecTestOn, "fmddExecTestOn");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::AltroCommand* cmd = fFmd->TestOn();
	
  if (!cmd) throw interface_missing("TestOn", "FMD");

  int r = cmd->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);

  return &ret;
}	
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ExecTestOff(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::fmddTesOff()");
  CHECK_CALL(param, kExecTestOff, "fmddExecTestOff");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::AltroCommand* cmd = fFmd->TestOff();
	
  if (!cmd) throw interface_missing("TestOff", "FMD");

  int r = cmd->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);

  return &ret;
}	
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ExecCalibrationRun(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::ExecCalibrationRun()");
  CHECK_CALL(param, kExecCalibrationRun, "fmddExecCalibrationRun");
  static IntegerVar ret;
  ret.setValue(-1);

  Rcuxx::AltroCommand* cmd = fFmd->CalibrationRun();
	
  if (!cmd) throw interface_missing("CalibrationRun", "FMD");

  int r = cmd->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ReadL0CNT(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::ReadL0CNT()");
  CHECK_CALL(param, kReadL0CNT, "fmddReadL0CNT");
  static IntegerVar ret;
  ret.setValue(-1);

  UIntegerVar* c;
  CheckArg(c, param.args->getNext(), param.thread);

  Rcuxx::FmdL0Triggers* reg = fFmd->L0Triggers();
  if (!reg) throw interface_missing("L0Triggers", "FMD");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  c->setValue( reg->Recieved() );

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ReadAL_DIG_I(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::ReadAL_DIG_I()");
  CHECK_CALL(param, kReadAL_DIG_I, "fmddReadAL_DIG_I");
  static IntegerVar ret;
  ret.setValue(-1);

  FloatVar* c;
  CheckArg(c, param.args->getNext(), param.thread);

  Rcuxx::FmdAL_DIG_I* reg = fFmd->AL_DIG_I();
  if (!reg) throw interface_missing("AL_DIG_I", "FMD");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  c->setValue( reg->MiliAmps() );

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ReadAL_DIG_I_TH(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::ReadAL_DIG_I_TH()");
  CHECK_CALL(param, kReadAL_DIG_I_TH, "fmddReadAL_DIG_I_TH");
  static IntegerVar ret;
  ret.setValue(-1);

  FloatVar* c;
  CheckArg(c, param.args->getNext(), param.thread);

  Rcuxx::FmdAL_DIG_I_TH* reg = fFmd->AL_DIG_I_TH();
  if (!reg) throw interface_missing("AL_DIG_I_TH", "FMD");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  c->setValue( reg->MiliAmps() );

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ReadAL_ANA_I(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::ReadAL_ANA_I()");
  CHECK_CALL(param, kReadAL_ANA_I, "fmddReadAL_ANA_I");
  static IntegerVar ret;
  ret.setValue(-1);
	
  FloatVar* c;
  CheckArg(c, param.args->getNext(), param.thread);

  Rcuxx::FmdAL_ANA_I* reg = fFmd->AL_ANA_I();
  if (!reg) throw interface_missing("AL_ANA_I", "FMD");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  c->setValue( reg->MiliAmps());

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ReadAL_ANA_I_TH(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::ReadAL_ANA_I_TH()");
  CHECK_CALL(param, kReadAL_ANA_I_TH, "fmddReadAL_ANA_I_TH");
  static IntegerVar ret;
  ret.setValue(-1);

  FloatVar* c;
  CheckArg(c, param.args->getNext(), param.thread);

  Rcuxx::FmdAL_ANA_I_TH* reg = fFmd->AL_ANA_I_TH();
  if (!reg) throw interface_missing("AL_ANA_I_TH", "FMD");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  c->setValue( reg->MiliAmps() );

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ReadAL_DIG_U(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::ReadAL_DIG_U()");
  CHECK_CALL(param, kReadAL_ANA_I_TH, "fmddReadAL_DIG_U");
  static IntegerVar ret;
  ret.setValue(-1);

  FloatVar* c;
  CheckArg(c, param.args->getNext(), param.thread);

  Rcuxx::FmdAL_DIG_U* reg = fFmd->AL_DIG_U();
  if (!reg) throw interface_missing("AL_DIG_U", "FMD");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  c->setValue( reg->MiliVolts() );

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ReadAL_DIG_U_TH(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::ReadAL_DIG_U_TH()");
  CHECK_CALL(param, kReadAL_DIG_U_TH, "fmddReadAL_DIG_U_TH");
  static IntegerVar ret;
  ret.setValue(-1);

  FloatVar* c;
  CheckArg(c, param.args->getNext(), param.thread);

  Rcuxx::FmdAL_DIG_U_TH* reg = fFmd->AL_DIG_U_TH();
  if (!reg) throw interface_missing("AL_DIG_U_TH", "FMD");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  c->setValue( reg->MiliVolts() );

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ReadAL_ANA_U(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::ReadAL_ANA_U()");
  CHECK_CALL(param, kReadAL_ANA_U, "fmddReadAL_ANA_U");
  static IntegerVar ret;
  ret.setValue(-1);

  FloatVar* c;
  CheckArg(c, param.args->getNext(), param.thread);

  Rcuxx::FmdAL_ANA_U* reg = fFmd->AL_ANA_U();
  if (!reg) throw interface_missing("AL_ANA_U", "FMD");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  c->setValue( reg->MiliVolts() );

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ReadAL_ANA_U_TH(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::ReadAL_ANA_U_TH()");
  CHECK_CALL(param, kReadAL_ANA_U_TH, "fmddReadAL_ANA_U_TH");
  static IntegerVar ret;
  ret.setValue(-1);

  FloatVar* c;
  CheckArg(c, param.args->getNext(), param.thread);

  Rcuxx::FmdAL_ANA_U_TH* reg = fFmd->AL_ANA_U_TH();
  if (!reg) throw interface_missing("AL_ANA_U_TH", "FMD");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  c->setValue( reg->MiliVolts() );

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ReadT1SENS(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::ReadT1SENS()");
  CHECK_CALL(param, kReadT1SENS, "fmddReadT1SENS");
  static IntegerVar ret;
  ret.setValue(-1);

  FloatVar* c;
  CheckArg(c, param.args->getNext(), param.thread);

  Rcuxx::FmdT1SENS* reg = fFmd->T1SENS();
  if (!reg) throw interface_missing("T1SENS", "FMD");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  c->setValue( reg->Centrigrades() );

  return &ret;
}	
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ReadT1SENS_TH(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::ReadT1SENS_TH()");
  CHECK_CALL(param, kReadT1SENS_TH, "fmddReadT1SENS_TH");
  static IntegerVar ret;
  ret.setValue(-1);
	
  FloatVar* c;
  CheckArg(c, param.args->getNext(), param.thread);

  Rcuxx::FmdT1SENS_TH* reg = fFmd->T1SENS_TH();
  if (!reg) throw interface_missing("T1SENS_TH", "FMD");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  c->setValue( reg->GetNatural() );

  return &ret;
}		
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ReadT2SENS(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::ReadT2SENS()");
  CHECK_CALL(param, kReadT2SENS, "fmddReadT2SENS");
  static IntegerVar ret;
  ret.setValue(-1);

  FloatVar* c;
  CheckArg(c, param.args->getNext(), param.thread);

  Rcuxx::FmdT2SENS* reg = fFmd->T2SENS();
  if (!reg) throw interface_missing("T2SENS", "FMD");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  c->setValue( reg->Centrigrades() );

  return &ret;
}	
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ReadT2SENS_TH(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::ReadT2SENS_TH()");
  CHECK_CALL(param, kReadT2SENS_TH, "fmddReadT2SENS_TH");
  static IntegerVar ret;
  ret.setValue(-1);

  FloatVar* c;
  CheckArg(c, param.args->getNext(), param.thread);

  Rcuxx::FmdT2SENS_TH* reg = fFmd->T2SENS_TH();
  if (!reg) throw interface_missing("T2SENS_TH", "FMD");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  c->setValue( reg->GetNatural() );

  return &ret;
}		
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ReadT1(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::ReadT1()");
  CHECK_CALL(param, kReadT1, "fmddReadT1");
  static IntegerVar ret;
  ret.setValue(-1);

  FloatVar* c;
  CheckArg(c, param.args->getNext(), param.thread);

  Rcuxx::FmdT1* reg = fFmd->T1();
  if (!reg) throw interface_missing("T1", "FMD");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  c->setValue( reg->Centigrades() );

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ReadT1_TH(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::ReadT1_TH()");
  CHECK_CALL(param, kReadT1_TH, "fmddReadT1_TH");
  static IntegerVar ret;
  ret.setValue(-1);

  FloatVar* c;
  CheckArg(c, param.args->getNext(), param.thread);

  Rcuxx::FmdT1_TH* reg = fFmd->T1_TH();
  if (!reg) throw interface_missing("T1_TH", "FMD");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  c->setValue( reg->GetNatural() );

  return &ret;
}		
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ReadT2(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::ReadT2()");
  CHECK_CALL(param, kReadT2, "fmddReadT2");
  static IntegerVar ret;
  ret.setValue(-1);

  FloatVar* c;
  CheckArg(c, param.args->getNext(), param.thread);

  Rcuxx::FmdT2* reg = fFmd->T2();
  if (!reg) throw interface_missing("T2", "FMD");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  c->setValue( reg->Centigrades() );

  return &ret;
}	
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ReadT2_TH(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::ReadT2_TH()");
  CHECK_CALL(param, kReadT2_TH, "fmddReadT2_TH");
  static IntegerVar ret;
  ret.setValue(-1);

  FloatVar* c;
  CheckArg(c, param.args->getNext(), param.thread);

  Rcuxx::FmdT2_TH* reg = fFmd->T2_TH();
  if (!reg) throw interface_missing("T2_TH", "FMD");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  c->setValue( reg->GetNatural());

  return &ret;
}			
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ReadT3(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::ReadT3()");
  CHECK_CALL(param, kReadT3, "fmddReadT3");
  static IntegerVar ret;
  ret.setValue(-1);

  FloatVar* c;
  CheckArg(c, param.args->getNext(), param.thread);

  Rcuxx::FmdT3* reg = fFmd->T3();
  if (!reg) throw interface_missing("T3", "FMD");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  c->setValue( reg->Centrigrades() );

  return &ret;
}	
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ReadT3_TH(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::ReadT3_TH()");
  CHECK_CALL(param, kReadT3_TH, "fmddReadT3_TH");
  static IntegerVar ret;
  ret.setValue(-1);

  FloatVar* c;
  CheckArg(c, param.args->getNext(), param.thread);

  Rcuxx::FmdT3_TH* reg = fFmd->T3_TH();
  if (!reg) throw interface_missing("T3_TH", "FMD");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  c->setValue( reg->GetNatural() );

  return &ret;
}			
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ReadT4(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::ReadT4()");
  CHECK_CALL(param, kReadT4, "fmddReadT4");
  static IntegerVar ret;
  ret.setValue(-1);

  FloatVar* c;
  CheckArg(c, param.args->getNext(), param.thread);

  Rcuxx::FmdT4* reg = fFmd->T4();
  if (!reg) throw interface_missing("T4", "FMD");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  c->setValue( reg->Centrigrades() );

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ReadT4_TH(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::ReadT4_TH()");
  CHECK_CALL(param, kReadT4_TH, "fmddReadT4_TH");
  static IntegerVar ret;
  ret.setValue(-1);

  FloatVar* c;
  CheckArg(c, param.args->getNext(), param.thread);

  Rcuxx::FmdT4_TH* reg = fFmd->T4_TH();
  if (!reg) throw interface_missing("T4_TH", "FMD");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  c->setValue( reg->GetNatural() );

  return &ret;
}		
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ReadVA_REC_IP(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::ReadVA_REC_IP()");
  CHECK_CALL(param, kReadVA_REC_IP, "fmddReadVA_REC_IP");
  static IntegerVar ret;
  ret.setValue(-1);

  FloatVar* c;
  CheckArg(c, param.args->getNext(), param.thread);

  Rcuxx::FmdVA_REC_IP* reg = fFmd->VA_REC_IP();
  if (!reg) throw interface_missing("VA_REC_IP", "FMD");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  c->setValue( reg->MiliAmps() );

  return &ret;
}	
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ReadVA_REC_IP_TH(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::ReadVA_REC_IP_TH()");
  CHECK_CALL(param, kReadVA_REC_IP_TH, "fmddReadVA_REC_IP_TH");
  static IntegerVar ret;
  ret.setValue(-1);

  FloatVar* c;
  CheckArg(c, param.args->getNext(), param.thread);

  Rcuxx::FmdVA_REC_IP_TH* reg = fFmd->VA_REC_IP_TH();
  if (!reg) throw interface_missing("VA_REC_IP_TH", "FMD");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  c->setValue( reg->MiliAmps() );

  return &ret;
}			
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ReadVA_REC_UP(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::ReadVA_REC_UP()");
  CHECK_CALL(param, kReadT4, "fmddReadVA_REC_UP");
  static IntegerVar ret;
  ret.setValue(-1);

  FloatVar* c;
  CheckArg(c, param.args->getNext(), param.thread);

  Rcuxx::FmdVA_REC_UP* reg = fFmd->VA_REC_UP();
  if (!reg) throw interface_missing("VA_REC_UP", "FMD");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  c->setValue( reg->MiliVolts() );

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ReadVA_REC_UP_TH(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::ReadVA_REC_UP_TH()");
  CHECK_CALL(param, kReadVA_REC_UP_TH, "fmddReadVA_REC_UP_TH");
  static IntegerVar ret;
  ret.setValue(-1);

  FloatVar* c;
  CheckArg(c, param.args->getNext(), param.thread);

  Rcuxx::FmdVA_REC_UP_TH* reg = fFmd->VA_REC_UP_TH();
  if (!reg) throw interface_missing("VA_REC_UP_TH", "FMD");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  c->setValue( reg->MiliVolts() );

  return &ret;
}		
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ReadVA_REC_IM(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::ReadVA_REC_IM()");
  CHECK_CALL(param, kReadVA_REC_IM, "fmddReadVA_REC_IM");
  static IntegerVar ret;
  ret.setValue(-1);

  FloatVar* c;
  CheckArg(c, param.args->getNext(), param.thread);

  Rcuxx::FmdVA_REC_IM* reg = fFmd->VA_REC_IM();
  if (!reg) throw interface_missing("VA_REC_IM", "FMD");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  c->setValue( reg->MiliAmps() );

  return &ret;
}	
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ReadVA_REC_IM_TH(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::ReadVA_REC_IM_TH()");
  CHECK_CALL(param, kReadVA_REC_IM_TH, "fmddReadVA_REC_IM_TH");
  static IntegerVar ret;
  ret.setValue(-1);

  FloatVar* c;
  CheckArg(c, param.args->getNext(), param.thread);

  Rcuxx::FmdVA_REC_IM_TH* reg = fFmd->VA_REC_IM_TH();
  if (!reg) throw interface_missing("VA_REC_IM_TH", "FMD");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  c->setValue( reg->MiliAmps() );

  return &ret;
}				
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ReadVA_REC_UM(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::ReadVA_REC_UM()");
  CHECK_CALL(param, kReadVA_REC_UM, "fmddReadVA_REC_UM");
  static IntegerVar ret;
  ret.setValue(-1);

  FloatVar* c;
  CheckArg(c, param.args->getNext(), param.thread);

  Rcuxx::FmdVA_REC_UM* reg = fFmd->VA_REC_UM();
  if (!reg) throw interface_missing("VA_REC_UM", "FMD");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  c->setValue( reg->MiliVolts() );

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ReadVA_REC_UM_TH(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::ReadVA_REC_UM_TH()");
  CHECK_CALL(param, kReadVA_REC_UM_TH, "fmddReadVA_REC_UM_TH");
  static IntegerVar ret;
  ret.setValue(-1);

  FloatVar* c;
  CheckArg(c, param.args->getNext(), param.thread);

  Rcuxx::FmdVA_REC_UM_TH* reg = fFmd->VA_REC_UM_TH();
  if (!reg) throw interface_missing("VA_REC_UM_TH", "FMD");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  c->setValue( reg->MiliVolts() );

  return &ret;
}			
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ReadVA_SUP_IP(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::ReadVA_SUP_IP()");
  CHECK_CALL(param, kReadVA_SUP_IP, "fmddReadVA_SUP_IP");
  static IntegerVar ret;
  ret.setValue(-1);

  FloatVar* c;
  CheckArg(c, param.args->getNext(), param.thread);

  Rcuxx::FmdVA_SUP_IP* reg = fFmd->VA_SUP_IP();
  if (!reg) throw interface_missing("VA_SUP_IP", "FMD");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  c->setValue( reg->MiliAmps() );

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ReadVA_SUP_IP_TH(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::ReadVA_SUP_IP_TH()");
  CHECK_CALL(param, kReadVA_SUP_IP_TH, "fmddReadVA_SUP_IP_TH");
  static IntegerVar ret;
  ret.setValue(-1);

  FloatVar* c;
  CheckArg(c, param.args->getNext(), param.thread);

  Rcuxx::FmdVA_SUP_IP_TH* reg = fFmd->VA_SUP_IP_TH();
  if (!reg) throw interface_missing("VA_SUP_IP_TH", "FMD");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  c->setValue( reg->MiliAmps() );

  return &ret;
}			
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ReadVA_SUP_UP(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::ReadVA_SUP_UP()");
  CHECK_CALL(param, kReadVA_SUP_UP, "fmddReadVA_SUP_UP");
  static IntegerVar ret;
  ret.setValue(-1);

  FloatVar* c;
  CheckArg(c, param.args->getNext(), param.thread);

  Rcuxx::FmdVA_SUP_UP* reg = fFmd->VA_SUP_UP();
  if (!reg) throw interface_missing("VA_SUP_UP", "FMD");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  c->setValue( reg->MiliVolts() );

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ReadVA_SUP_UP_TH(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::ReadVA_SUP_UP_TH()");
  CHECK_CALL(param, kReadVA_SUP_UP_TH, "fmddReadVA_SUP_UP_TH");
  static IntegerVar ret;
  ret.setValue(-1);

  FloatVar* c;
  CheckArg(c, param.args->getNext(), param.thread);

  Rcuxx::FmdVA_SUP_UP_TH* reg = fFmd->VA_SUP_UP_TH();
  if (!reg) throw interface_missing("VA_SUP_UP_TH", "FMD");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  c->setValue( reg->MiliVolts() );

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ReadVA_SUP_IM(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::ReadVA_SUP_IM()");
  CHECK_CALL(param, kReadVA_SUP_IM, "fmddReadVA_SUP_IM");
  static IntegerVar ret;
  ret.setValue(-1);

  FloatVar* c;
  CheckArg(c, param.args->getNext(), param.thread);

  Rcuxx::FmdVA_SUP_IM* reg = fFmd->VA_SUP_IM();
  if (!reg) throw interface_missing("VA_SUP_IM", "FMD");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  c->setValue( reg->MiliAmps() );

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ReadVA_SUP_IM_TH(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::ReadVA_SUP_IM_TH()");
  CHECK_CALL(param, kReadVA_SUP_IM_TH, "fmddReadVA_SUP_IM_TH");
  static IntegerVar ret;
  ret.setValue(-1);

  FloatVar* c;
  CheckArg(c, param.args->getNext(), param.thread);

  Rcuxx::FmdVA_SUP_IM_TH* reg = fFmd->VA_SUP_IM_TH();
  if (!reg) throw interface_missing("VA_SUP_IM_TH", "FMD");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  c->setValue( reg->MiliAmps() );

  return &ret;
}	
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ReadVA_SUP_UM(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::ReadVA_SUP_UM()");
  CHECK_CALL(param, kReadVA_SUP_UM, "fmddReadVA_SUP_UM");
  static IntegerVar ret;
  ret.setValue(-1);

  FloatVar* c;
  CheckArg(c, param.args->getNext(), param.thread);

  Rcuxx::FmdVA_SUP_UM* reg = fFmd->VA_SUP_UM();
  if (!reg) throw interface_missing("VA_SUP_UM", "FMD");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  c->setValue( reg->MiliVolts() );

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ReadVA_SUP_UM_TH(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::ReadVA_SUP_UM_TH()");
  CHECK_CALL(param, kReadVA_SUP_UM_TH, "fmddReadVA_SUP_UM_TH");
  static IntegerVar ret;
  ret.setValue(-1);

  FloatVar* c;
  CheckArg(c, param.args->getNext(), param.thread);

  Rcuxx::FmdVA_SUP_UM_TH* reg = fFmd->VA_SUP_UM_TH();
  if (!reg) throw interface_missing("VA_SUP_UM_TH", "FMD");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  c->setValue( reg->MiliVolts() );

  return &ret;
}		
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ReadGTL_U(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::ReadGTL_U()");
  CHECK_CALL(param, kReadGTL_U, "fmddReadGTL_U");
  static IntegerVar ret;
  ret.setValue(-1);

  FloatVar* c;
  CheckArg(c, param.args->getNext(), param.thread);

  Rcuxx::FmdGTL_U* reg = fFmd->GTL_U();
  if (!reg) throw interface_missing("GTL_U", "FMD");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  c->setValue( reg->MiliVolts() );

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ReadGTL_U_TH(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::ReadGTL_U_TH()");
  CHECK_CALL(param, kReadGTL_U_TH, "fmddReadGTL_U_TH");
  static IntegerVar ret;
  ret.setValue(-1);
	
  FloatVar* c;
  CheckArg(c, param.args->getNext(), param.thread);

  Rcuxx::FmdGTL_U_TH* reg = fFmd->GTL_U_TH();
  if (!reg) throw interface_missing("GTL_U_TH", "FMD");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  c->setValue( reg->MiliVolts() );

  return &ret;
}		
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ReadFLASH_I(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::ReadFLASH_I()");
  CHECK_CALL(param, kReadFLASH_I, "fmddReadFLASH_I");
  static IntegerVar ret;
  ret.setValue(-1);

  FloatVar* c;
  CheckArg(c, param.args->getNext(), param.thread);

  Rcuxx::FmdFLASH_I* reg = fFmd->FLASH_I();
  if (!reg) throw interface_missing("FLASH_I", "FMD");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  c->setValue( reg->MiliAmps() );

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ReadFLASH_I_TH(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::ReadFLASH_I_TH()");
  CHECK_CALL(param, kReadFLASH_I_TH, "fmddReadFLASH_I_TH");
  static IntegerVar ret;
  ret.setValue(-1);

  FloatVar* c;
  CheckArg(c, param.args->getNext(), param.thread);

  Rcuxx::FmdFLASH_I_TH* reg = fFmd->FLASH_I_TH();
  if (!reg) throw interface_missing("FLASH_I_TH", "FMD");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  c->setValue( reg->MiliAmps() );

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ReadShiftClock(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::ReadShiftClock()");
  CHECK_CALL(param, kReadShiftClock, "fmddReadShiftClock");
  static IntegerVar ret;
  ret.setValue(-1);

  UIntegerVar* d;
  UIntegerVar* p;
  CheckArg(d, param.args->getNext(), param.thread);
  CheckArg(p, param.args->getNext(), param.thread);

  Rcuxx::FmdShiftClock* reg = fFmd->ShiftClock();
  if (!reg) throw interface_missing("ShiftClock", "FMD");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  d->setValue( reg->Division() );
  p->setValue( reg->Phase() );

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ReadRange(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::ReadRange()");
  CHECK_CALL(param, kReadRange, "fmddReadRange");
  static IntegerVar ret;
  ret.setValue(-1);

  UIntegerVar* lo;
  UIntegerVar* hi;
  CheckArg(lo, param.args->getNext(), param.thread);
  CheckArg(hi, param.args->getNext(), param.thread);

  Rcuxx::FmdRange* reg = fFmd->Range();
  if (!reg) throw interface_missing("Range", "FMD");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  lo->setValue( reg->Min() );
  hi->setValue( reg->Max() );

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ReadSampleClock(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::ReadSampleClock()");
  CHECK_CALL(param, kReadSampleClock, "fmddReadSampleClock");
  static IntegerVar ret;
  ret.setValue(-1);

  UIntegerVar* d;
  UIntegerVar* p;
  CheckArg(d, param.args->getNext(), param.thread);
  CheckArg(p, param.args->getNext(), param.thread);

  Rcuxx::FmdSampleClock* reg = fFmd->SampleClock();
  if (!reg) throw interface_missing("SampleClock", "FMD");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  d->setValue( reg->Division() );
  p->setValue( reg->Phase() );

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ReadHoldWait(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::ReadHoldWait()");
  CHECK_CALL(param, kReadHoldWait, "fmddReadHoldWait");
  static IntegerVar ret;
  ret.setValue(-1);

  UIntegerVar* v;
  CheckArg(v, param.args->getNext(), param.thread);

  Rcuxx::FmdHoldWait* reg = fFmd->HoldWait();
  if (!reg) throw interface_missing("HoldWait", "FMD");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  v->setValue( reg->Clocks() );

  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ReadL0Timeout(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::ReadL0Timeout()");
  CHECK_CALL(param, kReadL0Timeout, "fmddReadL0Timeout");
  static IntegerVar ret;
  ret.setValue(-1);

  UIntegerVar* v;
  CheckArg(v, param.args->getNext(), param.thread);

  Rcuxx::FmdL0Timeout* reg = fFmd->L0Timeout();
  if (!reg) throw interface_missing("L0Timeout", "FMD");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  v->setValue( reg->Clocks() );

  return &ret;
}	
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ReadL1Timeout(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::ReadL1Timeout()");
  CHECK_CALL(param, kReadL0Timeout, "fmddReadL1Timeout");
  static IntegerVar ret;
  ret.setValue(-1);

  UIntegerVar* v;
  CheckArg(v, param.args->getNext(), param.thread);

  Rcuxx::FmdL1Timeout* reg = fFmd->L1Timeout();
  if (!reg) throw interface_missing("L1Timeout", "FMD");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  v->setValue( reg->Clocks() );

  return &ret;
}				
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ReadStatus(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::ReadStatus()");
  CHECK_CALL(param, kReadL0Timeout, "fmddReadStatus");
  static IntegerVar ret;
  ret.setValue(-1);

  BitVar* IsCalOn;
  BitVar* IsTriggerBusy;
  BitVar* IsDacBusy;
  BitVar* IsReadoutBusy;
  BitVar* IsBoxBusy;
  BitVar* IsIncompleteRo;
  BitVar* IsTriggerOverlap;
  BitVar* IsTriggerTimeout;
  BitVar* IsTestOn;
  BitVar* IsCalManOn;
	
  CheckArg(IsCalOn, param.args->getNext(), param.thread);
  CheckArg(IsTriggerBusy, param.args->getNext(), param.thread);
  CheckArg(IsDacBusy, param.args->getNext(), param.thread);
  CheckArg(IsReadoutBusy, param.args->getNext(), param.thread);
  CheckArg(IsBoxBusy, param.args->getNext(), param.thread);
  CheckArg(IsIncompleteRo, param.args->getNext(), param.thread);
  CheckArg(IsTriggerOverlap, param.args->getNext(), param.thread);
  CheckArg(IsTriggerTimeout, param.args->getNext(), param.thread);
  CheckArg(IsTestOn, param.args->getNext(), param.thread);
  CheckArg(IsCalManOn, param.args->getNext(), param.thread);

  Rcuxx::FmdStatus* reg = fFmd->Status();
  if (!reg) throw interface_missing("Status", "FMD");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
	
  IsCalOn->setValue(			reg->IsCalOn()			);
  IsTriggerBusy->setValue(	reg->IsTriggerBusy()	);
  IsDacBusy->setValue(		reg->IsDacBusy()		);
  IsReadoutBusy->setValue(	reg->IsReadoutBusy()	);
  IsBoxBusy->setValue(		reg->IsBoxBusy()		);
  IsIncompleteRo->setValue(	reg->IsIncompleteRo()   );
  IsTriggerOverlap->setValue( reg->IsTriggerOverlap() );
  IsTriggerTimeout->setValue( reg->IsTriggerTimeout() );
  IsTestOn->setValue(			reg->IsTestOn()			);
  IsCalManOn->setValue(		reg->IsCalManOn()		);

  return &ret;
}							

//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ReadShapeBias(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::ReadShapeBias()");
  CHECK_CALL(param, kReadShapeBias, "fmddReadShapeBias");
  static IntegerVar ret;
  ret.setValue(-1);

  UIntegerVar* t;
  UIntegerVar* b;
  CheckArg(t, param.args->getNext(), param.thread);
  CheckArg(b, param.args->getNext(), param.thread);

  Rcuxx::FmdShapeBias* reg = fFmd->ShapeBias();
  if (!reg) throw interface_missing("ShapeBias", "FMD");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  t->setValue( reg->Top() );
  b->setValue( reg->Bottom() );

  return &ret;
}				
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ReadPulser(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::ReadPulser()");
  CHECK_CALL(param, kReadPulser, "fmddReadPulser");
  static IntegerVar ret;
  ret.setValue(-1);

  UIntegerVar* l;
  UIntegerVar* s;
  CheckArg(l, param.args->getNext(), param.thread);
  CheckArg(s, param.args->getNext(), param.thread);

  Rcuxx::FmdPulser* reg = fFmd->Pulser();
  if (!reg) throw interface_missing("Pulser", "FMD");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  l->setValue( reg->Value() );
  s->setValue( reg->Test() );

  return &ret;
}				
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ReadCalIter(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::ReadCalIter()");
  CHECK_CALL(param, kReadCalIter, "fmddReadCalIter");
  static IntegerVar ret;
  ret.setValue(-1);

  UIntegerVar* v;
  CheckArg(v, param.args->getNext(), param.thread);

  Rcuxx::FmdCalIter* reg = fFmd->CalIter();
  if (!reg) throw interface_missing("CalIter", "FMD");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  v->setValue( reg->Value() );

  return &ret;
}				
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ReadVFP(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::ReadVFP()");
  CHECK_CALL(param, kReadVFP, "fmddReadVFP");
  static IntegerVar ret;
  ret.setValue(-1);

  UIntegerVar* t;
  UIntegerVar* b;
  CheckArg(t, param.args->getNext(), param.thread);
  CheckArg(b, param.args->getNext(), param.thread);

  Rcuxx::FmdVFP* reg = fFmd->VFP();
  if (!reg) throw interface_missing("VFP", "FMD");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  t->setValue( reg->Top() );
  b->setValue( reg->Bottom() );

  return &ret;
}				
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::ReadVFS(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::ReadVFS()");
  CHECK_CALL(param, kReadVFS, "fmddReadVFS");
  static IntegerVar ret;
  ret.setValue(-1);

  UIntegerVar* t;
  UIntegerVar* b;
  CheckArg(t, param.args->getNext(), param.thread);
  CheckArg(b, param.args->getNext(), param.thread);

  Rcuxx::FmdVFS* reg = fFmd->VFS();
  if (!reg) throw interface_missing("VFS", "FMD");

  int r = reg->Update();
  if (r != 0) throw ret;
  ret.setValue(r);
  t->setValue( reg->Top() );
  b->setValue( reg->Bottom() );

  return &ret;
}				
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::WriteAL_DIG_I_TH(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::WriteAL_DIG_I_TH()");
  CHECK_CALL(param, kWWriteAL_DIG_I_TH, "fmddWriteAL_DIG_I_TH");
  static IntegerVar ret;
  ret.setValue(-1);
	
  FloatVar w;
  CheckArg(w, param.args->getNext(), param.thread);

  Rcuxx::FmdAL_DIG_I_TH* reg = fFmd->AL_DIG_I_TH();
  if (!reg) throw interface_missing("AL_DIG_I_TH", "FMD");

  reg->SetMiliAmps( w.getValue() );
	
  int r = reg->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);
	
  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::WriteAL_ANA_I_TH(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::WriteAL_ANA_I_TH()");
  CHECK_CALL(param, kWWriteAL_ANA_I_TH, "fmddWriteAL_ANA_I_TH");
  static IntegerVar ret;
  ret.setValue(-1);
	
  FloatVar w;
  CheckArg(w, param.args->getNext(), param.thread);

  Rcuxx::FmdAL_ANA_I_TH* reg = fFmd->AL_ANA_I_TH();
  if (!reg) throw interface_missing("AL_ANA_I_TH", "FMD");

  reg->SetMiliAmps( w.getValue() );
	
  int r = reg->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);
	
  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::WriteAL_DIG_U_TH(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::WriteAL_DIG_U_TH()");
  CHECK_CALL(param, kWWriteAL_DIG_U_TH, "fmddWriteAL_DIG_U_TH");
  static IntegerVar ret;
  ret.setValue(-1);
	
  FloatVar w;
  CheckArg(w, param.args->getNext(), param.thread);

  Rcuxx::FmdAL_DIG_U_TH* reg = fFmd->AL_DIG_U_TH();
  if (!reg) throw interface_missing("AL_DIG_U_TH", "FMD");

  reg->SetMiliVolts( w.getValue() );
	
  int r = reg->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);
	
  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::WriteAL_ANA_U_TH(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::WriteAL_ANA_U_TH()");
  CHECK_CALL(param, kWWriteAL_ANA_U_TH, "fmddWriteAL_ANA_U_TH");
  static IntegerVar ret;
  ret.setValue(-1);
	
  FloatVar w;
  CheckArg(w, param.args->getNext(), param.thread);

  Rcuxx::FmdAL_ANA_U_TH* reg = fFmd->AL_ANA_U_TH();
  if (!reg) throw interface_missing("AL_ANA_U_TH", "FMD");

  reg->SetMiliVolts( w.getValue() );
	
  int r = reg->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);
	
  return &ret;
}			
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::WriteT1SENS_TH(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::WriteT1SENS_TH()");
  CHECK_CALL(param, kWWriteT1SENS_TH, "fmddWriteT1SENS_TH");
  static IntegerVar ret;
  ret.setValue(-1);
	
  FloatVar w;
  CheckArg(w, param.args->getNext(), param.thread);

  Rcuxx::FmdT1SENS_TH* reg = fFmd->T1SENS_TH();
  if (!reg) throw interface_missing("T1SENS_TH", "FMD");

  reg->SetCentigrade( w.getValue() );
	
  int r = reg->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);
	
  return &ret;
}			
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::WriteT2SENS_TH(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::WriteT2SENS_TH()");
  CHECK_CALL(param, kWWriteT2SENS_TH, "fmddWriteT2SENS_TH");
  static IntegerVar ret;
  ret.setValue(-1);
	
  FloatVar w;
  CheckArg(w, param.args->getNext(), param.thread);

  Rcuxx::FmdT2SENS_TH* reg = fFmd->T2SENS_TH();
  if (!reg) throw interface_missing("T2SENS_TH", "FMD");

  reg->SetCentigrade( w.getValue() );
	
  int r = reg->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);
	
  return &ret;
}						
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::WriteT1_TH(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::WriteT1_TH()");
  CHECK_CALL(param, kWWriteT1_TH, "fmddWriteT1_TH");
  static IntegerVar ret;
  ret.setValue(-1);
	
  FloatVar w;
  CheckArg(w, param.args->getNext(), param.thread);

  Rcuxx::FmdT1_TH* reg = fFmd->T1_TH();
  if (!reg) throw interface_missing("T1_TH", "FMD");

  reg->SetCentigrade( w.getValue() );
	
  int r = reg->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);
	
  return &ret;
}					
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::WriteT2_TH(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::WriteT2_TH()");
  CHECK_CALL(param, kWWriteT2_TH, "fmddWriteT2_TH");
  static IntegerVar ret;
  ret.setValue(-1);
	
  FloatVar w;
  CheckArg(w, param.args->getNext(), param.thread);

  Rcuxx::FmdT2_TH* reg = fFmd->T2_TH();
  if (!reg) throw interface_missing("T2_TH", "FMD");

  reg->SetCentigrade( w.getValue() );
	
  int r = reg->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);
	
  return &ret;
}			
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::WriteT3_TH(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::WriteT3_TH()");
  CHECK_CALL(param, kWWriteT3_TH, "fmddWriteT3_TH");
  static IntegerVar ret;
  ret.setValue(-1);
	
  FloatVar w;
  CheckArg(w, param.args->getNext(), param.thread);

  Rcuxx::FmdT3_TH* reg = fFmd->T3_TH();
  if (!reg) throw interface_missing("T3_TH", "FMD");

  reg->SetCentigrade( w.getValue() );
	
  int r = reg->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);
	
  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::WriteT4_TH(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::WriteT4_TH()");
  CHECK_CALL(param, kWWriteT4_TH, "fmddWriteT4_TH");
  static IntegerVar ret;
  ret.setValue(-1);
	
  FloatVar w;
  CheckArg(w, param.args->getNext(), param.thread);

  Rcuxx::FmdT4_TH* reg = fFmd->T4_TH();
  if (!reg) throw interface_missing("T4_TH", "FMD");

  reg->SetCentigrade( w.getValue() );
	
  int r = reg->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);
	
  return &ret;
}		
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::WriteVA_REC_IP_TH(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::WriteVA_REC_IP_TH()");
  CHECK_CALL(param, kWWriteVA_REC_IP_TH, "fmddWriteVA_REC_IP_TH");
  static IntegerVar ret;
  ret.setValue(-1);
	
  FloatVar w;
  CheckArg(w, param.args->getNext(), param.thread);

  Rcuxx::FmdVA_REC_IP_TH* reg = fFmd->VA_REC_IP_TH();
  if (!reg) throw interface_missing("VA_REC_IP_TH", "FMD");

  reg->SetMiliAmps( w.getValue() );
	
  int r = reg->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);
	
  return &ret;
}		
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::WriteVA_REC_UP_TH(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::WriteVA_REC_UP_TH()");
  CHECK_CALL(param, kWWriteVA_REC_UP_TH, "fmddWriteVA_REC_UP_TH");
  static IntegerVar ret;
  ret.setValue(-1);
	
  FloatVar w;
  CheckArg(w, param.args->getNext(), param.thread);

  Rcuxx::FmdVA_REC_UP_TH* reg = fFmd->VA_REC_UP_TH();
  if (!reg) throw interface_missing("VA_REC_UP_TH", "FMD");

  reg->SetMiliVolts( w.getValue() );
	
  int r = reg->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);
	
  return &ret;
}			
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::WriteVA_REC_IM_TH(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::WriteVA_REC_IM_TH()");
  CHECK_CALL(param, kWWriteVA_REC_IM_TH, "fmddWriteVA_REC_IM_TH");
  static IntegerVar ret;
  ret.setValue(-1);
	
  FloatVar w;
  CheckArg(w, param.args->getNext(), param.thread);

  Rcuxx::FmdVA_REC_IM_TH* reg = fFmd->VA_REC_IM_TH();
  if (!reg) throw interface_missing("VA_REC_IM_TH", "FMD");

  reg->SetMiliAmps( w.getValue() );
	
  int r = reg->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);
	
  return &ret;
}					
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::WriteVA_REC_UM_TH(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::WriteVA_REC_UM_TH()");
  CHECK_CALL(param, kWWriteVA_REC_UM_TH, "fmddWriteVA_REC_UM_TH");
  static IntegerVar ret;
  ret.setValue(-1);
	
  FloatVar w;
  CheckArg(w, param.args->getNext(), param.thread);

  Rcuxx::FmdVA_REC_UM_TH* reg = fFmd->VA_REC_UM_TH();
  if (!reg) throw interface_missing("VA_REC_UM_TH", "FMD");

  reg->SetMiliVolts( w.getValue() );
	
  int r = reg->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);
	
  return &ret;
}			
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::WriteVA_SUP_IP_TH(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::WriteVA_SUP_IP_TH()");
  CHECK_CALL(param, kWWriteVA_SUP_IP_TH, "fmddWriteVA_SUP_IP_TH");
  static IntegerVar ret;
  ret.setValue(-1);
	
  FloatVar w;
  CheckArg(w, param.args->getNext(), param.thread);

  Rcuxx::FmdVA_SUP_IP_TH* reg = fFmd->VA_SUP_IP_TH();
  if (!reg) throw interface_missing("VA_SUP_IP_TH", "FMD");

  reg->SetMiliAmps( w.getValue() );
	
  int r = reg->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);
	
  return &ret;
}					
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::WriteVA_SUP_UP_TH(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::WriteVA_SUP_UP_TH()");
  CHECK_CALL(param, kWWriteVA_SUP_UP_TH, "fmddWriteVA_SUP_UP_TH");
  static IntegerVar ret;
  ret.setValue(-1);
	
  FloatVar w;
  CheckArg(w, param.args->getNext(), param.thread);

  Rcuxx::FmdVA_SUP_UP_TH* reg = fFmd->VA_SUP_UP_TH();
  if (!reg) throw interface_missing("VA_SUP_UP_TH", "FMD");

  reg->SetMiliVolts( w.getValue() );
	
  int r = reg->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);
	
  return &ret;
}
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::WriteVA_SUP_IM_TH(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::WriteVA_SUP_IM_TH()");
  CHECK_CALL(param, kWWriteVA_SUP_IM_TH, "fmddWriteVA_SUP_IM_TH");
  static IntegerVar ret;
  ret.setValue(-1);
	
  FloatVar w;
  CheckArg(w, param.args->getNext(), param.thread);

  Rcuxx::FmdVA_SUP_IM_TH* reg = fFmd->VA_SUP_IM_TH();
  if (!reg) throw interface_missing("VA_SUP_IM_TH", "FMD");

  reg->SetMiliAmps( w.getValue() );
	
  int r = reg->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);
	
  return &ret;
}					
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::WriteVA_SUP_UM_TH(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::WriteVA_SUP_UM_TH()");
  CHECK_CALL(param, kWWriteVA_SUP_UM_TH, "fmddWriteVA_SUP_UM_TH");
  static IntegerVar ret;
  ret.setValue(-1);
	
  FloatVar w;
  CheckArg(w, param.args->getNext(), param.thread);

  Rcuxx::FmdVA_SUP_UM_TH* reg = fFmd->VA_SUP_UM_TH();
  if (!reg) throw interface_missing("VA_SUP_UM_TH", "FMD");

  reg->SetMiliVolts( w.getValue() );
	
  int r = reg->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);
	
  return &ret;
}		
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::WriteGTL_U_TH(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::WriteGTL_U_TH()");
  CHECK_CALL(param, kWWriteGTL_U_TH, "fmddWriteGTL_U_TH");
  static IntegerVar ret;
  ret.setValue(-1);
	
  FloatVar w;
  CheckArg(w, param.args->getNext(), param.thread);

  Rcuxx::FmdGTL_U_TH* reg = fFmd->GTL_U_TH();
  if (!reg) throw interface_missing("GTL_U_TH", "FMD");

  reg->SetMiliVolts( w.getValue() );
	
  int r = reg->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);
	
  return &ret;
}		
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::WriteFLASH_I_TH(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::WriteFLASH_I_TH()");
  CHECK_CALL(param, kWWriteFLASH_I_TH, "fmddWriteFLASH_I_TH");
  static IntegerVar ret;
  ret.setValue(-1);
	
  FloatVar w;
  CheckArg(w, param.args->getNext(), param.thread);

  Rcuxx::FmdFLASH_I_TH* reg = fFmd->FLASH_I_TH();
  if (!reg) throw interface_missing("FLASH_I_TH", "FMD");

  reg->SetMiliAmps( w.getValue() );
	
  int r = reg->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);
	
  return &ret;
}	
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::WriteShiftClock(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::WriteShiftClock()");
  CHECK_CALL(param, kWWriteShiftClock, "fmddWriteShiftClock");
  static IntegerVar ret;
  ret.setValue(-1);
	
  UIntegerVar d;
  UIntegerVar p;
  CheckArg(d, param.args->getNext(), param.thread);
  CheckArg(p, param.args->getNext(), param.thread);

  Rcuxx::FmdShiftClock* reg = fFmd->ShiftClock();
  if (!reg) throw interface_missing("ShiftClock", "FMD");

  reg->SetDivision(	d.getValue() );
  reg->SetPhase(		p.getValue() );

  int r = reg->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);
	
  return &ret;
}			
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::WriteRange(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::WriteRange()");
  CHECK_CALL(param, kWWriteRange, "fmddWriteRange");
  static IntegerVar ret;
  ret.setValue(-1);
	
  UIntegerVar lo;
  UIntegerVar hi;
  CheckArg(lo, param.args->getNext(), param.thread);
  CheckArg(hi, param.args->getNext(), param.thread);

  Rcuxx::FmdRange* reg = fFmd->Range();
  if (!reg) throw interface_missing("Range", "FMD");

  reg->SetMin(	lo.getValue() );
  reg->SetMax(	hi.getValue() );

  int r = reg->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);
	
  return &ret;
}				
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::WriteSampleClock(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::WriteSampleClock()");
  CHECK_CALL(param, kWWriteSampleClock, "fmddWriteSampleClock");
  static IntegerVar ret;
  ret.setValue(-1);
	
  UIntegerVar d;
  UIntegerVar p;
  CheckArg(d, param.args->getNext(), param.thread);
  CheckArg(p, param.args->getNext(), param.thread);

  Rcuxx::FmdSampleClock* reg = fFmd->SampleClock();
  if (!reg) throw interface_missing("SampleClock", "FMD");

  reg->SetDivision(	d.getValue() );
  reg->SetPhase(		p.getValue() );

  int r = reg->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);
	
  return &ret;
}				
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::WriteHoldWait(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::WriteHoldWait()");
  CHECK_CALL(param, kWWriteHoldWait, "fmddWriteHoldWait");
  static IntegerVar ret;
  ret.setValue(-1);
	
  UIntegerVar v;
  CheckArg(v, param.args->getNext(), param.thread);
	
  Rcuxx::FmdHoldWait* reg = fFmd->HoldWait();
  if (!reg) throw interface_missing("HoldWait", "FMD");

  reg->SetClocks( v.getValue() );
	
  int r = reg->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);
	
  return &ret;
}				
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::WriteL0Timeout(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::WriteL0Timeout()");
  CHECK_CALL(param, kWWriteL0Timeout, "fmddWriteL0Timeout");
  static IntegerVar ret;
  ret.setValue(-1);
	
  UIntegerVar v;
  CheckArg(v, param.args->getNext(), param.thread);
	
  Rcuxx::FmdL0Timeout* reg = fFmd->L0Timeout();
  if (!reg) throw interface_missing("L0Timeout", "FMD");

  reg->SetClocks( v.getValue() );
	
  int r = reg->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);
	
  return &ret;
}				
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::WriteL1Timeout(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::WriteL1Timeout()");
  CHECK_CALL(param, kWWriteL1Timeout, "fmddWriteL1Timeout");
  static IntegerVar ret;
  ret.setValue(-1);
	
  UIntegerVar v;
  CheckArg(v, param.args->getNext(), param.thread);
	
  Rcuxx::FmdL1Timeout* reg = fFmd->L1Timeout();
  if (!reg) throw interface_missing("L1Timeout", "FMD");

  reg->SetClocks( v.getValue() );
	
  int r = reg->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);
	
  return &ret;
}	
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::WriteShapeBias(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::WriteShapeBias()");
  CHECK_CALL(param, kWWriteShapeBias, "fmddWriteShapeBias");
  static IntegerVar ret;
  ret.setValue(-1);
	
  UIntegerVar t;
  UIntegerVar b;
  CheckArg(t, param.args->getNext(), param.thread);
  CheckArg(b, param.args->getNext(), param.thread);
	
  Rcuxx::FmdShapeBias* reg = fFmd->ShapeBias();
  if (!reg) throw interface_missing("ShapeBias", "FMD");

  reg->SetTop(	t.getValue() );
  reg->SetBottom( b.getValue() );
	
  int r = reg->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);
	
  return &ret;
}				
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::WritePulser(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::WritePulser()");
  CHECK_CALL(param, kWWritePulser, "fmddWritePulser");
  static IntegerVar ret;
  ret.setValue(-1);
	
  UIntegerVar l;
  UIntegerVar s;
  CheckArg(l, param.args->getNext(), param.thread);
  CheckArg(s, param.args->getNext(), param.thread);
	
  Rcuxx::FmdPulser* reg = fFmd->Pulser();
  if (!reg) throw interface_missing("Pulser", "FMD");

  reg->SetValue( l.getValue() );
  reg->SetTest( s.getValue() );
	
  int r = reg->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);
	
  return &ret;
}				
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::WriteCalIter(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::WriteCalIter()");
  CHECK_CALL(param, kWWriteCalIter, "fmddWriteCalIter");
  static IntegerVar ret;
  ret.setValue(-1);
	
  UIntegerVar v;
  CheckArg(v, param.args->getNext(), param.thread);
	
  Rcuxx::FmdCalIter* reg = fFmd->CalIter();
  if (!reg) throw interface_missing("CalIter", "FMD");

  reg->SetValue( v.getValue() );
	
  int r = reg->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);
	
  return &ret;
}				
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::WriteVFP(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::WriteVFP()");
  CHECK_CALL(param, kWWriteVFP, "fmddWriteVFP");
  static IntegerVar ret;
  ret.setValue(-1);
	
  UIntegerVar t;
  UIntegerVar b;
  CheckArg(t, param.args->getNext(), param.thread);
  CheckArg(b, param.args->getNext(), param.thread);
	
  Rcuxx::FmdVFP* reg = fFmd->VFP();
  if (!reg) throw interface_missing("VFP", "FMD");

  reg->SetTop(	t.getValue() );
  reg->SetBottom( b.getValue() );
	
  int r = reg->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);
	
  return &ret;
}		
//____________________________________________________________________
const IntegerVar*
Pvss::Fmd::WriteVFS(ExecuteParamRec& param)
{
  Rcuxx::DebugGuard g(fgDebug, "Fmd::WriteVFS()");
  CHECK_CALL(param, kWWriteVFS, "fmddWriteVFS");
  static IntegerVar ret;
  ret.setValue(-1);
	
  UIntegerVar t;
  UIntegerVar b;
  CheckArg(t, param.args->getNext(), param.thread);
  CheckArg(b, param.args->getNext(), param.thread);
	
  Rcuxx::FmdVFS* reg = fFmd->VFS();
  if (!reg) throw interface_missing("VFS", "FMD");

  reg->SetTop(	t.getValue() );
  reg->SetBottom( b.getValue() );
	
  int r = reg->Commit();
  if (r != 0) throw ret;
  ret.setValue(r);
	
  return &ret;
}		
//____________________________________________________________________
//
// EOF
// 
