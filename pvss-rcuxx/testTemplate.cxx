#ifdef HAVE_CONFIG_H // Don't define HAVE_CONFIG_H with MSVC++
# include "config.h"
#endif
#include <vector>
#include <map>
#include <BaseExternHdl.hxx>
#include <iostream>
#include <sstream>

//____________________________________________________________________
template <typename T> const char* type2param();
template <> const char* type2param<bool>()          { return "bool"; }
template <> const char* type2param<int>()           { return "int"; }
template <> const char* type2param<unsigned int>()  { return "unsigned"; }
template <> const char* type2param<float>()         { return "float"; }
template <> const char* type2param<unsigned int*>() { return "dyn_uint"; }

//____________________________________________________________________
struct InterfaceDeclBase 
{
public:
  /** Constructor 
      @param ret        Return type 
      @param name       Name of interface function 
      @param threadSafe Not used */
  InterfaceDeclBase(VariableType ret, 
		    const char*  name,
		    bool         threadSafe=true)
  {
    fRec.retType            = ret;
    fRec.name               = const_cast<char*>(name);
    fRec.threadCallPossible = threadSafe;
  }
  /** Get the function record */
  const FunctionListRec& GetRecord() const { return fRec; }
  /** Get the function record */
  FunctionListRec& GetRecord() { return fRec; }
protected:
  /** The function list record */
  FunctionListRec fRec;
};

//____________________________________________________________________
template <typename T>
struct InterfaceDeclT : public InterfaceDeclBase
{
public:
  /** Type of interface */
  typedef T InterfaceType;
  /** Constructor 
      @param ret        Return type 
      @param name       Name of interface function 
      @param threadSafe Not used */      
  InterfaceDeclT(VariableType type, 
		 const char*  name, 
		 bool         threadSafe=false)
    : InterfaceDeclBase(type, name, threadSafe)
  {}
  virtual const Variable* operator()(InterfaceType* obj) = 0;
};

//____________________________________________________________________
template <typename T, 
	  typename Arg01=void*, 
	  typename Arg02=void*, 
	  typename Arg03=void*>
struct ReadInterfaceDecl : public InterfaceDeclT<T>
{
  typedef typename InterfaceDeclT<T>::InterfaceType InterfaceType;
  typedef Arg01 Arg01Type;
  typedef Arg02 Arg02Type;
  typedef Arg03 Arg03Type;
  typedef Arg01Type (InterfaceType::* Get01Type)() const;
  typedef Arg02Type (InterfaceType::* Get02Type)() const;
  typedef Arg03Type (InterfaceType::* Get03Type)() const;

  ReadInterfaceDecl(VariableType type,
		    const char* name, 
		    Get01Type   get01=0, 
		    Get02Type   get02=0, 
		    Get03Type   get03=0) 
    : InterfaceDeclT<T>(type, name),
      fGet01(get01), 
      fGet02(get02),
      fGet03(get03)
  {
    MakeParams();
    InterfaceDeclBase::GetRecord().paramList = const_cast<char*>(fParams.c_str());
  }
  const Variable* operator()(InterfaceType* obj) 
  {
    Arg01Type x1 = Arg01Type();
    Arg02Type x2 = Arg02Type();
    Arg03Type x3 = Arg03Type();
    if (fGet01) x1 = (obj->*fGet01)();
    if (fGet02) x2 = (obj->*fGet02)();
    if (fGet03) x3 = (obj->*fGet03)();
    std::cout << this->fRec.name << ": " 
	      << x1 << "," << x2 << "," << x3 << std::endl;
    Variable* ret = Variable::allocate(this->fRec.retType);
    return ret;
  }
protected:
  std::string fParams;
  void MakeParams() { 
    std::stringstream s(fParams);
    s << "(";
    if (fGet01) { s << type2param<Arg01Type>() << " p1";
      if (fGet01) { s << ", " << type2param<Arg01Type>() << " p2";
	if (fGet01) { s << ", " << type2param<Arg01Type>() << " p3"; } 
      }
    }
    s << ")";
  }
  Get01Type fGet01;
  Get02Type fGet02;
  Get03Type fGet03;
};
//____________________________________________________________________
template <typename     T,
	  typename     Arg01,
	  typename     Arg02,
	  typename     Arg03>
InterfaceDeclT<T>*
MakeReadInterface3(const char* name,
		   VariableType type,
		   Arg01 (T::*g1)() const, 
		   Arg02 (T::*g2)() const,
		   Arg03 (T::*g3)() const)
{
  return new ReadInterfaceDecl<T,Arg01,Arg02,Arg03>(type, name, g1, g2, g3);
}
//____________________________________________________________________
template <typename     T,
	  typename     Arg01>
InterfaceDeclT<T>*
MakeReadInterface1(const char* name,
		   VariableType type,
		   Arg01 (T::*g1)() const)
{
  return new ReadInterfaceDecl<T,Arg01,void*,void*>(type, name, g1, 0, 0);
}
		 

//____________________________________________________________________
struct Test
{
  int Foo() const 
  { 
    std::cout << "Test::Foo" << std::endl;
    return 1;
  }
  float Bar() const 
  { 
    std::cout << "Test::Bar" << std::endl;
    return 2;
  }
  unsigned int Baz() const 
  { 
    std::cout << "Test::Baz" << std::endl;
    return 3;
  }
};

int 
main()
{
  InterfaceDeclT<Test>* rd1 = 
    MakeReadInterface3("Test", INTEGER_VAR, &Test::Foo, &Test::Bar, &Test::Baz);
  
  Test t;
  rd1->operator()(&t);
  
  InterfaceDeclT<Test>* rd2 = 
    MakeReadInterface1("Test", INTEGER_VAR, &Test::Foo);
  
  rd2->operator()(&t);

  return 0;
}

  

  
      

      
  
  
    
