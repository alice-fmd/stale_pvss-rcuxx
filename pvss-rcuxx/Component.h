// -*- mode: C++ -*-
#ifndef PVSS_RCUXX_COMPONENT_H
#define PVSS_RCUXX_COMPONENT_H
#include <pvss-rcuxx/Util.h>

namespace Pvss
{
  
  /** Component in the system, like RCU, BC, FMDD, or ALTRO.  It holds
      a list of interfaces. */ 
  template <typename Low>
  class Component 
  {
  public:
    /** Type of low-level interface */ 
    typedef Low LowType;
    /** Type of list of interfaces */ 
    typedef std::vector<InterfaceDeclBase*> InterfaceVector;
    /** Constructor */
    Component() : {}
    /** Get the declared interfaces */ 
    const InterfaceVector& Interfaces() const { return fInterfaces; }
  protected:
    /** Vector of interfaces */
    InterfaceVector fInterfaces;
    /** Pointer to low level object */
    LowType* fLow;
  };
}

#endif
//____________________________________________________________________
//
// EOF
// 
