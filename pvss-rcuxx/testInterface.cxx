#include "config.h"
#include "Util.h"
#include <Ctrl/CtrlVar.hxx>

//____________________________________________________________________
struct Test
{
  Test() : fFoo(42), fBar(3.14), fBaz(0xf) 
  {
  }
  
  int Foo() const { 
    std::cout << "Test::Foo() const -> " << fFoo << std::endl;
    return fFoo;
  }
  float Bar() const 
  { 
    std::cout << "Test::Bar() const -> " << fBar << std::endl;
    return fBar;
  }
  unsigned int Baz() const 
  { 
    std::cout << "Test::Baz() const -> 0x" 
	      << std::hex << fBaz << std::dec << std::endl;
    return fBaz;
  }
  void SetFoo(int x)
  { 
    fFoo = x;
    std::cout << "Test::SetFoo(" << x << ") -> " << fFoo << std::endl;
  }
  void SetBar(float x) 
  { 
    fBar = x;
    std::cout << "Test::SetBar(" << x << ") -> " << fBar << std::endl;
  }
  void SetBaz(unsigned int x) 
  { 
    fBaz = x;
    std::cout << "Test::SetBaz(0x" << std::hex << x << ") -> 0x" << fBaz 
	      << std::dec << std::endl;
  }
  int fFoo;
  float fBar;
  unsigned int fBaz;
};

//____________________________________________________________________
int
main()
{
  try {
      
    Pvss::InterfaceDeclT<Test>* i1 
      = Pvss::MakeReadIF<Test,int>("readTest",&Test::Foo,&Test::Bar,&Test::Baz);
    Pvss::InterfaceDeclT<Test>* i2
      = Pvss::MakeWriteIF<Test,int>("writeTest",&Test::SetFoo,
				    &Test::SetBar,&Test::SetBaz);

    Test t;
    i1->SetObject(&t);
    i2->SetObject(&t);
    
    ExprList args;
    IntegerVar*  dummy    = 0;
    IntegerVar*  intVar   = 0;
    FloatVar*    floatVar = 0;
    UIntegerVar* uintVar  = 0;
    args.append(new CtrlVar(dummy    = new IntegerVar));
    args.append(new CtrlVar(intVar   = new IntegerVar));
    args.append(new CtrlVar(floatVar = new FloatVar));
    args.append(new CtrlVar(uintVar  = new UIntegerVar));
    
    BaseExternHdl::ExecuteParamRec param = { "Test", 0, &args, 0, 0 };
    param.args->getFirst();
    i1->operator()(param);

    CtrlExpr* var = args.getFirst();
    while (var) { 
      const Variable* v = var->evaluate(0);
      std::cout << v->formatValue("") << std::endl;
      var = args.getNext();
    }
    intVar->setValue(12345);
    floatVar->setValue(2.71);
    uintVar->setValue(2.71);
    
    param.args->getFirst();
    i2->operator()(param);

    
  }
  catch (std::exception& e) { 
    std::cerr << e.what() << std::endl;
    return 1;
  }
  return 0;
}

  
  
