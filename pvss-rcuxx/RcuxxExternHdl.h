/** -*- mode: C++ -*-
 *
 */
#ifndef RcuxxExternHdl_h
#define RcuxxExternHdl_h
#ifdef HAVE_CONFIG_H // Don't define HAVE_CONFIG_H with MSVC++
# include "config.h"
#endif
#ifndef __VECTOR__
# include <vector>
#endif
#ifndef __MAP__
# include <map>
#endif
#ifndef _BASEEXTERNHDL_H_
# include <BaseExternHdl.hxx>
#endif

#ifdef _WIN32
# define  PVSS_EXPORT  __declspec(dllexport)
#else
# define  PVSS_EXPORT
#endif

/** @namespace PVSS 
    @brief Namespace for all interface classes 
*/
namespace Pvss
{
  class Rcu;
}

/** @class RcuxxExternHdl RcuxxExternHdl.h <pvss-rcuxx/RcuxxExternHdl.h>
    @brief Rcu++ interface 
*/
class RcuxxExternHdl : public BaseExternHdl
{
public:
  /** Enum of known functions at this level */
  enum {
    kOpen, 
    kClose, 
    kRcuBase
  };
  /** Constructor 
      @param nextHdl   The next interface (ignored) 
      @param funcCount Number of functions 
      @param fnList    Function list */
  RcuxxExternHdl(BaseExternHdl*  nextHdl, 
		 PVSSulong       funcCount, 
		 FunctionListRec fnList[])
    : BaseExternHdl(nextHdl, funcCount, fnList), 
      fFuncList(fnList)
  {}

  /** Execute a function 
      @param param Function call parameters 
      @return A return value */
  virtual const Variable *execute(ExecuteParamRec &param);

  /** This is mainly for debugging */
  FunctionListRec* getFuncList() { return fFuncList; }
protected:
  /** Open a connection to an RCU. 
      @param param Function call parameters 
      @return An integer which acts as a handle to the opened
      connection.  This should be passed as a first argument to all
      functions in this extension. */
  const IntegerVar* Open(ExecuteParamRec &param);
  /** Close a connection to an RCU 
      @param param Function call parameters.  Must have at least one 
      argument which is the handle to close */ 
  const IntegerVar* Close(ExecuteParamRec &param);
  /** Call an RCU function.  First argument must be a valid handle 
      @param param Parameters */
  const Variable*   Call(ExecuteParamRec &param);

  /** List of opened RCU interfaces.  The handled passed back to the
      caller via the open function, is simple a offset in this list
  */
  typedef std::vector<Pvss::Rcu*> RcuList;
  /** List of known RCUs */ 
  RcuList fRcus;
  /** Map of url to handle.  Using this map, we can make sure that we
      don't open the same connection twice */ 
  typedef std::map<std::string, int> UrlMap;
  /** Map from URLs to handles */
  UrlMap fMap;

  /** This is mainly for debugging */
  FunctionListRec *fFuncList;
};

/** 
    @param nextHdl Pointer to next interface 
    @return A newly acllocated Rcu++ interface 
*/
extern "C" PVSS_EXPORT BaseExternHdl *newExternHdl(BaseExternHdl *nextHdl);

#endif
/*
 * EOF
 */
