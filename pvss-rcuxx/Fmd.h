// -*- mode: c++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    pvss-rcuxx/Fmd.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Declaration of FMD interface 
*/
#ifndef Pvss_Fmd_h
#define Pvss_Fmd_h
#ifndef Pvss_Bc_h
# include "Bc.h"
#endif
#ifndef PVSS_Altro_h
# include "Altro.h"
#endif
class Variable;
class IntegerVar;
namespace Rcuxx 
{
  class Bc;
  class Fmd;
}

namespace Pvss
{
  /** @class Fmd Fmd.h <pvss-rcuxx/Fmd.h>
      @brief BC interface 
  */
  class Fmd : public Bc
  {
  public:
    /** Type of call parameters */
    typedef BaseExternHdl::ExecuteParamRec ExecuteParamRec;
    /** Enum of possible function calls */
    enum {
      kExecFakeTrigger = Bc::kBcLast, 	// FMDD Exec FakeTrigger
      kExecSoftReset,		// FMDD Exec SoftReset
      kExecChangeDacs,		// FMDD Exec ChangeDacs
      kExecPulserOn,		// FMDD Exec PulserOn
      kExecPulserOff,		// FMDD Exec PulserOff
      kExecTestOn,		// FMDD Exec TestOn
      kExecTestOff,		// FMDD Exec TestOff
      kExecCalibrationRun,	// FMDD Exec CalibrationRun
      /** Register read FMD */
      kReadL0CNT,		// FMDD Read L0CNT
      kReadAL_DIG_I,		// FMDD Read AL_DIG_I
      kReadAL_DIG_I_TH, 	// FMDD Read AL_DIG_I_TH
      kReadAL_ANA_I,		// FMDD Read AL_ANA_I
      kReadAL_ANA_I_TH,		// FMDD Read AL_ANA_I_TH
      kReadAL_DIG_U,		// FMDD Read AL_DIG_U
      kReadAL_DIG_U_TH,		// FMDD Read AL_DIG_U_TH
      kReadAL_ANA_U,		// FMDD Read AL_ANA_U
      kReadAL_ANA_U_TH,		// FMDD Read AL_ANA_U_TH
      kReadT1SENS,		// FMDD Read T1SENS
      kReadT1SENS_TH,		// FMDD Read T1SENS_TH
      kReadT2SENS,		// FMDD Read T2SENS
      kReadT2SENS_TH,		// FMDD Read T2SENS_TH
      kReadT1,			// FMDD Read T1
      kReadT1_TH,		// FMDD Read T1_TH
      kReadT2,			// FMDD Read T2
      kReadT2_TH,		// FMDD Read T2_TH
      kReadT3,			// FMDD Read T3
      kReadT3_TH,		// FMDD Read T3_TH
      kReadT4,			// FMDD Read T4
      kReadT4_TH,		// FMDD Read T4_TH
      kReadVA_REC_IP,		// FMDD Read VA_REC_IP
      kReadVA_REC_IP_TH,	// FMDD Read VA_REC_IP_TH
      kReadVA_REC_UP,		// FMDD Read VA_REC_UP
      kReadVA_REC_UP_TH,	// FMDD Read VA_REC_UP_TH
      kReadVA_REC_IM,		// FMDD Read VA_REC_IM
      kReadVA_REC_IM_TH,	// FMDD Read VA_REC_IM_TH
      kReadVA_REC_UM,		// FMDD Read VA_REC_UM
      kReadVA_REC_UM_TH,	// FMDD Read VA_REC_UM_TH
      kReadVA_SUP_IP,		// FMDD Read VA_SUP_IP
      kReadVA_SUP_IP_TH,	// FMDD Read VA_SUP_IP_TH
      kReadVA_SUP_UP,		// FMDD Read VA_SUP_UP
      kReadVA_SUP_UP_TH,	// FMDD Read VA_SUP_UP_TH
      kReadVA_SUP_IM,		// FMDD Read VA_SUP_IM
      kReadVA_SUP_IM_TH,	// FMDD Read VA_SUP_IM_TH
      kReadVA_SUP_UM,		// FMDD Read VA_SUP_UM
      kReadVA_SUP_UM_TH,	// FMDD Read VA_SUP_UM_TH
      kReadGTL_U,		// FMDD Read GTL_U
      kReadGTL_U_TH,		// FMDD Read GTL_U_TH
      kReadFLASH_I,		// FMDD Read FLASH_I
      kReadFLASH_I_TH,		// FMDD Read FLASH_I_TH
      kReadShiftClock,		// FMDD Read ShiftClock
      kReadRange,		// FMDD Read Range
      kReadSampleClock,		// FMDD Read SampleClock
      kReadHoldWait,		// FMDD Read HoldWait
      kReadL0Timeout,		// FMDD Read L0Timeout
      kReadL1Timeout,		// FMDD Read L1Timeout
      kReadStatus,		// FMDD Read Status
      kReadShapeBias,		// FMDD Read ShapeBias
      kReadPulser,		// FMDD Read Pulser
      kReadCalIter,		// FMDD Read CalIter
      kReadVFP,			// FMDD Read VFP
      kReadVFS,			// FMDD Read VFS
      /** Register write FMD */
      kWriteAL_DIG_I_TH, 	// FMDD Write AL_DIG_I_TH
      kWriteAL_ANA_I_TH, 	// FMDD Write AL_ANA_I_TH
      kWriteAL_DIG_U_TH, 	// FMDD Write AL_DIG_U_TH
      kWriteAL_ANA_U_TH, 	// FMDD Write AL_ANA_U_TH
      kWriteT1SENS_TH,		// FMDD Write T1SENS_TH
      kWriteT2SENS_TH,		// FMDD Write T2SENS_TH
      kWriteT1_TH,		// FMDD Write T1_TH
      kWriteT2_TH,		// FMDD Write T2_TH
      kWriteT3_TH,		// FMDD Write T3_TH
      kWriteT4_TH,		// FMDD Write T4_TH
      kWriteVA_REC_IP_TH,	// FMDD Write VA_REC_IP_TH
      kWriteVA_REC_UP_TH,	// FMDD Write VA_REC_UP_TH
      kWriteVA_REC_IM_TH,	// FMDD Write VA_REC_IM_TH
      kWriteVA_REC_UM_TH,	// FMDD Write VA_REC_UM_TH
      kWriteVA_SUP_IP_TH,	// FMDD Write VA_SUP_IP_TH
      kWriteVA_SUP_UP_TH,	// FMDD Write VA_SUP_UP_TH
      kWriteVA_SUP_IM_TH,	// FMDD Write VA_SUP_IM_TH
      kWriteVA_SUP_UM_TH,	// FMDD Write VA_SUP_UM_TH
      kWriteGTL_U_TH,		// FMDD Write GTL_U_TH
      kWriteFLASH_I_TH,		// FMDD Write FLASH_I_TH
      kWriteShiftClock,		// FMDD Write ShiftClock
      kWriteRange,	 	// FMDD Write Range
      kWriteSampleClock,	// FMDD Write SampleClock
      kWriteHoldWait,		// FMDD Write HoldWait
      kWriteL0Timeout,		// FMDD Write L0Timeout
      kWriteL1Timeout,		// FMDD Write L1Timeout
      kWriteShapeBias,		// FMDD Write ShapeBias
      kWritePulser,	 	// FMDD Write Pulser
      kWriteCalIter,		// FMDD Write CalIter
      kWriteVFP,	 	// FMDD Write VFP
      kWriteVFS,	 	// FMDD Write VFS
    };

    /** Constructor 
	@param bc Pointer to low-level access */
    Fmd(Rcuxx::Bc* bc);
    /** Destructor */ 
    virtual ~Fmd();
    
    /** Execute a function.  This is delegated from the handler to
	here. Note, when we get here, at least one parameter is read
	from the execution structore, so we should use `getNext', not
	`getFirst'.  
	@param param Parameters of the function call 
	@return A pointer to the return value */
    virtual const Variable *execute(ExecuteParamRec &param);

  protected:
    /** FMDD Exec FakeTrigger
        @param param Parameters record */
    const IntegerVar* ExecFakeTrigger(ExecuteParamRec& param);
    /** FMDD Exec SoftReset
        @param param Parameters record */
    const IntegerVar* ExecSoftReset(ExecuteParamRec& param);
    /** FMDD Exec ChangeDacs
        @param param Parameters record */
    const IntegerVar* ExecChangeDacs(ExecuteParamRec& param);
    /** FMDD Exec PulserOn
        @param param Parameters record */
    const IntegerVar* ExecPulserOn(ExecuteParamRec& param);
    /** FMDD Exec PulserOff
        @param param Parameters record */
    const IntegerVar* ExecPulserOff(ExecuteParamRec& param);
    /** FMDD Exec TestOn
        @param param Parameters record */
    const IntegerVar* ExecTestOn(ExecuteParamRec& param);
    /** FMDD Exec TestOff
        @param param Parameters record */
    const IntegerVar* ExecTestOff(ExecuteParamRec& param);
    /** FMDD Exec CalibrationRun
        @param param Parameters record */
    const IntegerVar* ExecCalibrationRun(ExecuteParamRec& param);

    /** FMDD Read L0CNT
        @param param Parameters record */
    const IntegerVar* ReadL0CNT(ExecuteParamRec& param);
    /** FMDD Read AL_DIG_I
        @param param Parameters record */
    const IntegerVar* ReadAL_DIG_I(ExecuteParamRec& param);
    /** FMDD Read AL_DIG_I_TH
        @param param Parameters record */
    const IntegerVar* ReadAL_DIG_I_TH(ExecuteParamRec& param);
    /** FMDD Read AL_ANA_I
        @param param Parameters record */
    const IntegerVar* ReadAL_ANA_I(ExecuteParamRec& param);
    /** FMDD Read AL_ANA_I_TH
        @param param Parameters record */
    const IntegerVar* ReadAL_ANA_I_TH(ExecuteParamRec& param);
    /** FMDD Read AL_DIG_U
        @param param Parameters record */
    const IntegerVar* ReadAL_DIG_U(ExecuteParamRec& param);
    /** FMDD Read AL_DIG_U_TH
        @param param Parameters record */
    const IntegerVar* ReadAL_DIG_U_TH(ExecuteParamRec& param);
    /** FMDD Read AL_ANA_U
        @param param Parameters record */
    const IntegerVar* ReadAL_ANA_U(ExecuteParamRec& param);
    /** FMDD Read AL_ANA_U_TH
        @param param Parameters record */
    const IntegerVar* ReadAL_ANA_U_TH(ExecuteParamRec& param);
    /** FMDD Read T1SENS
        @param param Parameters record */
    const IntegerVar* ReadT1SENS(ExecuteParamRec& param);	
    /** FMDD Read T1SENS_TH
        @param param Parameters record */
    const IntegerVar* ReadT1SENS_TH(ExecuteParamRec& param);
    /** FMDD Read T2SENS
        @param param Parameters record */
    const IntegerVar* ReadT2SENS(ExecuteParamRec& param);	
    /** FMDD Read T2SENS_TH
        @param param Parameters record */
    const IntegerVar* ReadT2SENS_TH(ExecuteParamRec& param);
    /** FMDD Read T1
        @param param Parameters record */
    const IntegerVar* ReadT1(ExecuteParamRec& param);		
    /** FMDD Read T1_TH
        @param param Parameters record */
    const IntegerVar* ReadT1_TH(ExecuteParamRec& param);	
    /** FMDD Read T2
        @param param Parameters record */
    const IntegerVar* ReadT2(ExecuteParamRec& param);		
    /** FMDD Read T2_TH
        @param param Parameters record */
    const IntegerVar* ReadT2_TH(ExecuteParamRec& param);	
    /** FMDD Read T3
        @param param Parameters record */
    const IntegerVar* ReadT3(ExecuteParamRec& param);		
    /** FMDD Read T3_TH
        @param param Parameters record */
    const IntegerVar* ReadT3_TH(ExecuteParamRec& param);	
    /** FMDD Read T4
        @param param Parameters record */
    const IntegerVar* ReadT4(ExecuteParamRec& param);		
    /** FMDD Read T4_TH
        @param param Parameters record */
    const IntegerVar* ReadT4_TH(ExecuteParamRec& param);	
    /** FMDD Read VA_REC_IP
        @param param Parameters record */
    const IntegerVar* ReadVA_REC_IP(ExecuteParamRec& param);
    /** FMDD Read VA_REC_IP_TH
        @param param Parameters record */
    const IntegerVar* ReadVA_REC_IP_TH(ExecuteParamRec& param);
    /** FMDD Read VA_REC_UP
        @param param Parameters record */
    const IntegerVar* ReadVA_REC_UP(ExecuteParamRec& param);
    /** FMDD Read VA_REC_UP_TH
        @param param Parameters record */
    const IntegerVar* ReadVA_REC_UP_TH(ExecuteParamRec& param);
    /** FMDD Read VA_REC_IM
        @param param Parameters record */
    const IntegerVar* ReadVA_REC_IM(ExecuteParamRec& param);
    /** FMDD Read VA_REC_IM_TH
        @param param Parameters record */
    const IntegerVar* ReadVA_REC_IM_TH(ExecuteParamRec& param);
    /** FMDD Read VA_REC_UM
        @param param Parameters record */
    const IntegerVar* ReadVA_REC_UM(ExecuteParamRec& param);
    /** FMDD Read VA_REC_UM_TH
        @param param Parameters record */
    const IntegerVar* ReadVA_REC_UM_TH(ExecuteParamRec& param);
    /** FMDD Read VA_SUP_IP
        @param param Parameters record */
    const IntegerVar* ReadVA_SUP_IP(ExecuteParamRec& param);
    /** FMDD Read VA_SUP_IP_TH
        @param param Parameters record */
    const IntegerVar* ReadVA_SUP_IP_TH(ExecuteParamRec& param);
    /** FMDD Read VA_SUP_UP
        @param param Parameters record */
    const IntegerVar* ReadVA_SUP_UP(ExecuteParamRec& param);
    /** FMDD Read VA_SUP_UP_TH
        @param param Parameters record */
    const IntegerVar* ReadVA_SUP_UP_TH(ExecuteParamRec& param);
    /** FMDD Read VA_SUP_IM
        @param param Parameters record */
    const IntegerVar* ReadVA_SUP_IM(ExecuteParamRec& param);
    /** FMDD Read VA_SUP_IM_TH
        @param param Parameters record */
    const IntegerVar* ReadVA_SUP_IM_TH(ExecuteParamRec& param);
    /** FMDD Read VA_SUP_UM
        @param param Parameters record */
    const IntegerVar* ReadVA_SUP_UM(ExecuteParamRec& param);
    /** FMDD Read VA_SUP_UM_TH
        @param param Parameters record */
    const IntegerVar* ReadVA_SUP_UM_TH(ExecuteParamRec& param);
    /** FMDD Read GTL_U
        @param param Parameters record */
    const IntegerVar* ReadGTL_U(ExecuteParamRec& param);	
    /** FMDD Read GTL_U_TH
        @param param Parameters record */
    const IntegerVar* ReadGTL_U_TH(ExecuteParamRec& param);
    /** FMDD Read FLASH_I
        @param param Parameters record */
    const IntegerVar* ReadFLASH_I(ExecuteParamRec& param);	
    /** FMDD Read FLASH_I_TH
        @param param Parameters record */
    const IntegerVar* ReadFLASH_I_TH(ExecuteParamRec& param);
    /** FMDD Read ShiftClock
        @param param Parameters record */
    const IntegerVar* ReadShiftClock(ExecuteParamRec& param);
    /** FMDD Read Range
        @param param Parameters record */
    const IntegerVar* ReadRange(ExecuteParamRec& param);
    /** FMDD Read SampleClock
        @param param Parameters record */
    const IntegerVar* ReadSampleClock(ExecuteParamRec& param);
    /** FMDD Read HoldWait
        @param param Parameters record */
    const IntegerVar* ReadHoldWait(ExecuteParamRec& param);
    /** FMDD Read L0Timeout
        @param param Parameters record */
    const IntegerVar* ReadL0Timeout(ExecuteParamRec& param);
    /** FMDD Read L1Timeout
        @param param Parameters record */
    const IntegerVar* ReadL1Timeout(ExecuteParamRec& param);
    /** FMDD Read Status
        @param param Parameters record */
    const IntegerVar* ReadStatus(ExecuteParamRec& param);
    /** FMDD Read ShapeBias
        @param param Parameters record */
    const IntegerVar* ReadShapeBias(ExecuteParamRec& param);
    /** FMDD Read Pulser
        @param param Parameters record */
    const IntegerVar* ReadPulser(ExecuteParamRec& param);
    /** FMDD Read CalIter
        @param param Parameters record */
    const IntegerVar* ReadCalIter(ExecuteParamRec& param);
    /** FMDD Read VFP
        @param param Parameters record */
    const IntegerVar* ReadVFP(ExecuteParamRec& param);
    /** FMDD Read VFS
        @param param Parameters record */
    const IntegerVar* ReadVFS(ExecuteParamRec& param);

    /** FMDD Write AL_DIG_I_TH
        @param param Parameters record */
    const IntegerVar* WriteAL_DIG_I_TH(ExecuteParamRec& param);
    /** FMDD Write AL_ANA_I_TH
        @param param Parameters record */
    const IntegerVar* WriteAL_ANA_I_TH(ExecuteParamRec& param);
    /** FMDD Write AL_DIG_U_TH
        @param param Parameters record */
    const IntegerVar* WriteAL_DIG_U_TH(ExecuteParamRec& param);
    /** FMDD Write AL_ANA_U_TH
        @param param Parameters record */
    const IntegerVar* WriteAL_ANA_U_TH(ExecuteParamRec& param);
    /** FMDD Write T1SENS_TH
        @param param Parameters record */
    const IntegerVar* WriteT1SENS_TH(ExecuteParamRec& param);
    /** FMDD Write T2SENS_TH
        @param param Parameters record */
    const IntegerVar* WriteT2SENS_TH(ExecuteParamRec& param);
    /** FMDD Write T1_TH
        @param param Parameters record */
    const IntegerVar* WriteT1_TH(ExecuteParamRec& param);
    /** FMDD Write T2_TH
        @param param Parameters record */
    const IntegerVar* WriteT2_TH(ExecuteParamRec& param);
    /** FMDD Write T3_TH
        @param param Parameters record */
    const IntegerVar* WriteT3_TH(ExecuteParamRec& param);
    /** FMDD Write T4_TH
        @param param Parameters record */
    const IntegerVar* WriteT4_TH(ExecuteParamRec& param);
    /** FMDD Write VA_REC_IP_TH
        @param param Parameters record */
    const IntegerVar* WriteVA_REC_IP_TH(ExecuteParamRec& param);
    /** FMDD Write VA_REC_UP_TH
        @param param Parameters record */
    const IntegerVar* WriteVA_REC_UP_TH(ExecuteParamRec& param);
    /** FMDD Write VA_REC_IM_TH
        @param param Parameters record */
    const IntegerVar* WriteVA_REC_IM_TH(ExecuteParamRec& param);
    /** FMDD Write VA_REC_UM_TH
        @param param Parameters record */
    const IntegerVar* WriteVA_REC_UM_TH(ExecuteParamRec& param);
    /** FMDD Write VA_SUP_IP_TH
        @param param Parameters record */
    const IntegerVar* WriteVA_SUP_IP_TH(ExecuteParamRec& param);
    /** FMDD Write VA_SUP_UP_TH
        @param param Parameters record */
    const IntegerVar* WriteVA_SUP_UP_TH(ExecuteParamRec& param);
    /** FMDD Write VA_SUP_IM_TH
        @param param Parameters record */
    const IntegerVar* WriteVA_SUP_IM_TH(ExecuteParamRec& param);
    /** FMDD Write VA_SUP_UM_TH
        @param param Parameters record */
    const IntegerVar* WriteVA_SUP_UM_TH(ExecuteParamRec& param);
    /** FMDD Write GTL_U_TH
        @param param Parameters record */
    const IntegerVar* WriteGTL_U_TH(ExecuteParamRec& param);
    /** FMDD Write FLASH_I_TH
        @param param Parameters record */
    const IntegerVar* WriteFLASH_I_TH(ExecuteParamRec& param);
    /** FMDD Write ShiftClock
        @param param Parameters record */
    const IntegerVar* WriteShiftClock(ExecuteParamRec& param);
    /** FMDD Write Range
        @param param Parameters record */
    const IntegerVar* WriteRange(ExecuteParamRec& param);
    /** FMDD Write SampleClock
        @param param Parameters record */
    const IntegerVar* WriteSampleClock(ExecuteParamRec& param);
    /** FMDD Write HoldWait
        @param param Parameters record */
    const IntegerVar* WriteHoldWait(ExecuteParamRec& param);
    /** FMDD Write L0Timeout
        @param param Parameters record */
    const IntegerVar* WriteL0Timeout(ExecuteParamRec& param);
    /** FMDD Write L1Timeout
        @param param Parameters record */
    const IntegerVar* WriteL1Timeout(ExecuteParamRec& param);
    /** FMDD Write ShapeBias
        @param param Parameters record */
    const IntegerVar* WriteShapeBias(ExecuteParamRec& param);
    /** FMDD Write Pulser
        @param param Parameters record */
    const IntegerVar* WritePulser(ExecuteParamRec& param);
    /** FMDD Write CalIter
        @param param Parameters record */
    const IntegerVar* WriteCalIter(ExecuteParamRec& param);
    /** FMDD Write VFP
        @param param Parameters record */
    const IntegerVar* WriteVFP(ExecuteParamRec& param);
    /** FMDD Write VFS
        @param param Parameters record */
    const IntegerVar* WriteVFS(ExecuteParamRec& param);

    /** Pointer to BC interface */
    Rcuxx::Fmd*  fFmd;
  };
}

#endif
//
// EOF
//
