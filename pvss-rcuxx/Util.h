// -*- mode: c++ -*-
#ifndef PVSS_UTIL_H
# define PVSS_UTIL_H
# include <stdexcept>
# include <typeinfo>
# include <Basics/Variables/UIntegerVar.hxx>
# include <Basics/Variables/IntegerVar.hxx>
# include <Basics/Variables/FloatVar.hxx>
# include <Basics/Variables/TextVar.hxx>
# include <Basics/Variables/BitVar.hxx>
# include <Basics/Variables/Variable.hxx>
# include <Basics/Variables/DynVar.hxx>
# include <Basics/Utilities/ErrClass.hxx>
# include <Ctrl/BaseExternHdl.hxx>
# include <rcuxx/DebugGuard.h>
# include <sstream>
# define  SHOW_DEBUG false
namespace Pvss 
{
  //__________________________________________________________________
  /** @struct not_lvalue 
      @brief Exception thrown in case of a bad return argument passed 
  */
  struct not_lvalue 
  {
    not_lvalue() {}
    const char* what() const { return "not an lvalue"; }
  };

  //__________________________________________________________________
  struct interface_missing 
  {
    interface_missing(const char* where="", const char* name="") 
      : fWhat("Missing interface ")
    {
      if (name && name[0] != '\0') fWhat += name;
      if (!where || where[0] == '\0') return;
      fWhat += " in ";
      fWhat += where;
    }
    const char* what() const { return fWhat.c_str(); }
    std::string fWhat;
  };

  //__________________________________________________________________
  /** Function template to check that type passed is correct 
      @return PVSS type number of @a v's type. */
  template <typename T> VariableType GetTypeId();
  
  /** Specialisation for BitVar
      @return PVSS type number of @a v's type. */
  template <> inline VariableType GetTypeId<BitVar>() { return BIT_VAR; }
  /** Specialisation for UIntegerVar
      @return PVSS type number of @a v's type. */
  template <> inline VariableType GetTypeId<UIntegerVar>(){return UINTEGER_VAR;}
  /** Specialisation for IntegerVar
      @return PVSS type number of @a v's type. */
  template <> inline VariableType GetTypeId<IntegerVar>() {return INTEGER_VAR;}
  /** Specialisation for FloatVar
      @return PVSS type number of @a v's type. */
  template <> inline VariableType GetTypeId<FloatVar>() { return FLOAT_VAR; }
  /** Specialisation for TextVar
      @return PVSS type number of @a v's type. */
  template <> inline VariableType GetTypeId<TextVar>() { return TEXT_VAR; }
  /** Specialisation for DynVar
      @return PVSS type number of @a v's type. */
  template <> inline VariableType GetTypeId<DynVar>() { return DYN_VAR; }

  //__________________________________________________________________
  /** Structure template to extract variable type */
  template <typename T> struct TypeTrait {};
  
  /** Specialisation for BitVar */
  template <> struct TypeTrait<bool> { 
    /** Variable type */
    typedef BitVar VarType;  
    /** Variable id */
    enum { TypeId=BIT_VAR }; 
    /** Bad return value */
    static bool BadValue() { return false; }
    /** String representation */
    static const char* AsString() { return "bool"; }
    /** Type identifier */
    static VariableType VarId() { return BIT_VAR; }
  };
  /** Specialisation for UIntegerVar */
  template <> struct TypeTrait<unsigned int> {
    /** Variable type */
    typedef UIntegerVar VarType;
    /** Variable Id */
    enum { TypeId=UINTEGER_VAR };
    /** Bad return value */
    static unsigned int BadValue() { return 0; }
    /** String representation */
    static const char* AsString() { return "unsigned"; }
    /** Type identifier */
    static VariableType VarId() { return UINTEGER_VAR; }
  };
  /** Specialisation for IntegerVar */
  template <> struct TypeTrait<int> {
    /** Variable type */
    typedef IntegerVar VarType;
    /** Variable Id */
    enum { TypeId=INTEGER_VAR };
    /** Bad return value */
    static int BadValue() { return -1; }
    /** String representation */
    static const char* AsString() { return "int"; }
    /** Type identifier */
    static VariableType VarId() { return INTEGER_VAR; }
  };
  /** Specialisation for FloatVar */
  template <> struct TypeTrait<float> {
    /** Variable type */
    typedef FloatVar VarType;
    /** Variable Id */
    enum { TypeId=FLOAT_VAR };
    /** Bad return value */
    static float BadValue() { return -1.; }
    /** String representation */
    static const char* AsString() { return "float"; }
    /** Type identifier */
    static VariableType VarId() { return FLOAT_VAR; }
  };
  /** Specialisation for TextVar */
  template <> struct TypeTrait<std::string> {
    /** Variable type */
    typedef TextVar VarType;
    /** Variable Id */
    enum { TypeId=TEXT_VAR };
    /** Bad return value */
    static std::string BadValue() { return ""; }
    /** String representation */
    static const char* AsString() { return "string"; }
    /** Type identifier */
    static VariableType VarId() { return TEXT_VAR; }
  };
  /** Specialisation for DynVar */
  template <> struct TypeTrait<void*> {
    /** Variable type */
    typedef DynVar VarType;
    /** Variable Id */
    enum { TypeId=DYN_VAR };
    /** Bad return value */
    static void* BadValue() { return 0; }
    /** String representation */
    static const char* AsString() { return "void*"; }
    /** Type identifier */
    static VariableType VarId() { return DYN_VAR; }
  };
 
  //==================================================================
  /** Assign to pointer address of @a dest if @a source is of a
      compatible type, or assign zero it not compatible. 
      @param v Pointer to assign from 
      @return 0 if @a v isn't of the right type */ 
  template <typename T> 
  inline T* 
  DynCast(Variable* v) 
  {
    if (!v) return 0;
    VariableType id = GetTypeId<T>();
    if (!v->isA(id)) return 0;
    return static_cast<T*>(v);
  }

  //==================================================================
  /** Check passed  arguments 
      @param r      Return value 
      @param expr   Argument expression 
      @param thread Executing thread */
  template <typename T> 
  inline 
  void CheckArg(T& r, CtrlExpr* expr, CtrlThread* thread) 
  {
    Rcuxx::DebugGuard g(SHOW_DEBUG, "CheckArg<&>(%p, %p, %p)",&r,expr,thread);
    if (!expr) {
      std::cerr << "No expression" << std::endl;
      throw std::invalid_argument("No expression");
    }
    const Variable* var = expr->evaluate(thread);
    if (!var)  {
      std::cerr << "Failed to evalute argument" << std::endl;
      throw std::invalid_argument("Couldn't evaluate expression");
    }
    if (!var->isA(r.isA())) {
      std::cerr << "Argument not of the right type!!" << std::endl;
      throw std::bad_cast();
    }
    r = *var;
  }

  //__________________________________________________________________
  /** Check passed arguments 
      @param r      Return value 
      @param expr   Argument expression 
      @param thread Executing thread */
  template <typename T>
  inline 
  void CheckArg(T*& r, CtrlExpr* expr, CtrlThread* thread)
  {
    Rcuxx::DebugGuard g(SHOW_DEBUG, "CheckArg<*>(%p, %p, %p)",r,expr,thread);
    if (!expr) {
      std::cerr << "No expression" << std::endl;
      // throw std::invalid_argument("No expression");
      return;
    }
    g.Message(SHOW_DEBUG, "Getting target variable");
    Variable* var = expr->getTarget(thread);

    if (!var)  throw not_lvalue();
    // r = dynamic_cast<T*>(var);
    r = DynCast<T>(var);
    if (!r) throw std::bad_cast();
  }

  //__________________________________________________________________
  /** Base class for interface declarations */
  struct InterfaceDeclBase 
  {
  public:
    /** Type of execution parameters */
    typedef BaseExternHdl::ExecuteParamRec ExecuteParamRec;
    /** Destructor */
    virtual ~InterfaceDeclBase() {}
    /** Get the function record */
    const FunctionListRec& GetRecord() const { return fRec; }
    /** Get the function record */
    FunctionListRec& GetRecord() { return fRec; }
    /** Call member function 
	@param obj Object to call member function of. 
	@param param Execution parameters. */
    virtual const Variable* operator()(ExecuteParamRec& param) = 0;
    /** Get the function name */ 
    virtual const char* AsString() const = 0;
  protected:
    /** Constructor 
	@param ret        Return type 
	@param name       Name of interface function 
	@param threadSafe Not used */
    InterfaceDeclBase(VariableType ret, 
		      const char*  name,
		      bool         threadSafe=true)
    {
      fRec.retType            = ret;
      fRec.name               = const_cast<char*>(name);
      fRec.threadCallPossible = threadSafe;
    }
    /** The function list record */
    FunctionListRec fRec;
  };

  //____________________________________________________________________
  /** Actual implementation of class template for read interface to a
      register, etc. */
  template <typename Component>
  struct InterfaceDeclT : public InterfaceDeclBase
  {
    /** Type of component */ 
    typedef Component ComponentType;
    /** Set the component */
    void SetComponent(ComponentType* c) { fComponent = c; }
  protected:
    InterfaceDeclT(VariableType ret, 
		   const char*  name,
		   bool         threadSafe=true)
      : InterfaceDeclBase(ret, name, threadSafe)
    {}
    /** The component */ 
    ComponentType*       fComponent;
  };
		   
  //____________________________________________________________________
  /** Actual implementation of class template for read interface to a
      register, etc. */
  template <typename T, 
	    typename Component,
	    typename Return,
	    typename Arg01=void*, 
	    typename Arg02=void*, 
	    typename Arg03=void*>
  struct ReadInterfaceDecl : public InterfaceDecl<Component>
  {
    /** Type of interface object */
    typedef T InterfaceType;
    /** Type of component */ 
    typedef InterfaceDeclT<Component>::ComponentType ComponentType;
    /** Type of parameters */
    typedef InterfaceDeclBase::ExecuteParamRec ExecuteParamRec;
    /** Return type */
    typedef Return ReturnType;
    /** Return variable type */
    typedef typename TypeTrait<ReturnType>::VarType ReturnVarType;
    /** First argument type */
    typedef Arg01 Arg01Type;
    /** Second argument type */
    typedef Arg02 Arg02Type;
    /** Third argument type */
    typedef Arg03 Arg03Type;
    /** Function type to get the interface from the component */
    typedef InterfaceType* (ComponentType::* GetInterfaceType)();
    /** First getter function type */
    typedef Arg01Type (InterfaceType::* Get01Type)() const;
    /** Second getter function type */
    typedef Arg02Type (InterfaceType::* Get02Type)() const;
    /** Third getter function type */
    typedef Arg03Type (InterfaceType::* Get03Type)() const;
    /** Return type Id */
    enum { 
      ReturnTypeId = TypeTrait<ReturnType>::TypeId
    };

    /** Constructor 
	@param type  Return type Identifier 
	@param name  Name of interface function 
	@param get01 First getter function 
	@param get02 Second getter function 
	@param get03 Third getter function */
    ReadInterfaceDecl(const char*      name, 
		      GetInterfaceType getif,
		      Get01Type        get01=0, 
		      Get02Type        get02=0, 
		      Get03Type        get03=0) 
      : InterfaceDeclT<T>(TypeTrait<ReturnType>::VarId(), name),
	fGetIF(getif),
	fGet01(get01), 
	fGet02(get02),
	fGet03(get03)
    {
      InterfaceDeclBase::GetRecord().paramList = 
	const_cast<char*>(GetParams());
    }
    /** Call interface function. 
	@param param Execution parameters 
	@return object of appropriate type */
    const Variable* operator()(ExecuteParamRec& param) 
    {
      Rcuxx::DebugGuard g(true, "Executing %s", AsString());
      // Make static return value, and set default value.
      static ReturnVarType ret;
      ret.setValue(TypeTrait<ReturnType>::BadValue());
      
      if (!this->fComponent) 
	throw interface_missing("", this->fRec.name);

      InterfaceType* inter = (fComponet->*fGetIF)();
      if (!inter)
	throw interface_missing("", this->fRec.name);

      if (fGet01) GetOne<Arg01Type>(inter, param, fGet01);
      if (fGet02) GetOne<Arg02Type>(inter, param, fGet02);
      if (fGet03) GetOne<Arg03Type>(inter, param, fGet03);

      return &ret;
    }
  protected:
    template <typename Arg, typename Get>
    bool GetOne(InterfaceType* inter, ExecuteParamRec& param, Get get)
    {
      // Check if this getter is defined.
      if (!get) return false;

      // Type of return parameter 
      typedef typename TypeTrait<Arg>::VarType* PtrType;
      PtrType var;
      
      // Check that the passed parameter is if the right type. 
      CheckArg(var, param.args->getNext(), param.thread);
      
      // Now assign the return parameter 
      var->setValue((inter->*get)());
      return true;
    }
    /** Get the function as a string */
    const char* AsString() const { 
      static std::string s;
      if (s.empty()) {
	std::stringstream sstr(s);
	sstr << this->fRec.name << GetParams();
	s = sstr.str();
      }
      return s.c_str();
    }
    mutable std::string fParams;
    /** Make a parameter string */
    const char* GetParams() const { 
      if (!fParams.empty()) return fParams.c_str();
      std::stringstream s(fParams);
      s << "(";
      int n = 0;
      n = GetParam<Arg01Type>(n, fGet01, s);
      n = GetParam<Arg02Type>(n, fGet02, s);
      n = GetParam<Arg03Type>(n, fGet03, s);
      s << ")";
      fParams = s.str();
      return fParams.c_str();
    }
    template <typename Arg, typename Get>
    int GetParam(int i, Get get, std::stringstream& s) const { 
      if (!get) return i;
      if (i != 0) s << ", ";
      s << TypeTrait<Arg>::AsString() << "& arg" << i;
      return ++i;
    }
    /** Get the interface from the component */
    GetInterfaceType fGetIF;
    /** First getter */
    Get01Type fGet01;
    /** Second getter */
    Get02Type fGet02;
    /** Third getter */
    Get03Type fGet03;
  };
  //____________________________________________________________________
  template <typename T, 
	    typename C,
	    typename R,
	    typename A1,
	    typename A2,
	    typename A3>
  InterfaceDeclT<C>*
  MakeReadIF(const char* name, 
	     T* (C::*i)(),
	     A1 (T::*f1)() const, 
	     A2 (T::*f2)() const, 
	     A3 (T::*f3)() const)
  {
    return new ReadInterfaceDecl<T,C,R,A1,A2,A3>(name, i, f1, f2, f3);
  }

  //____________________________________________________________________
  /** Actual implementation of class template for read interface to a
      register, etc. */
  template <typename T, 
	    typename Return,
	    typename Arg01=void*, 
	    typename Arg02=void*, 
	    typename Arg03=void*>
  struct WriteInterfaceDecl : public InterfaceDeclT<T>
  {
    /** Type of interface object */
    typedef typename InterfaceDeclT<T>::InterfaceType InterfaceType;
    /** Type of parameters */
    typedef InterfaceDeclBase::ExecuteParamRec ExecuteParamRec;
    /** Return type */
    typedef Return ReturnType;
    /** Return variable type */
    typedef typename TypeTrait<ReturnType>::VarType ReturnVarType;
    /** First argument type */
    typedef Arg01 Arg01Type;
    /** Second argument type */
    typedef Arg02 Arg02Type;
    /** Third argument type */
    typedef Arg03 Arg03Type;
    /** First setter function type */
    typedef void (InterfaceType::* Set01Type)(Arg01Type);
    /** Second setter function type */
    typedef void (InterfaceType::* Set02Type)(Arg02Type);
    /** Third setter function type */
    typedef void (InterfaceType::* Set03Type)(Arg03Type);
    /** Return type Id */
    enum { 
      ReturnTypeId = TypeTrait<ReturnType>::TypeId
    };

    /** Constructor 
	@param type  Return type Identifier 
	@param name  Name of interface function 
	@param set01 First setter function 
	@param set02 Second setter function 
	@param set03 Third setter function */
    WriteInterfaceDecl(const char* name, 
		       Set01Type   set01=0, 
		       Set02Type   set02=0, 
		       Set03Type   set03=0) 
      : InterfaceDeclT<T>(TypeTrait<ReturnType>::VarId(), name),
	fSet01(set01), 
	fSet02(set02),
	fSet03(set03)
    {
      InterfaceDeclBase::GetRecord().paramList = 
	const_cast<char*>(GetParams());
    }
    /** Call interface function. 
	@param param Execution parameters 
	@return object of appropriate type */
    const Variable* operator()(ExecuteParamRec& param) 
    {
      Rcuxx::DebugGuard g(true, "Executing %s", AsString());
      // Make static return value, and set default value.
      static ReturnVarType ret;
      ret.setValue(TypeTrait<ReturnType>::BadValue());
      
      if (!this->fComponent) 
	throw interface_missing("", this->fRec.name);

      InterfaceType* inter = (fComponet->*fGetIF)();
      if (!inter)
	throw interface_missing("", this->fRec.name);

      if (fSet01) SetOne<Arg01Type>(inter, param, fSet01);
      if (fSet02) SetOne<Arg02Type>(inter, param, fSet02);
      if (fSet03) SetOne<Arg03Type>(inter, param, fSet03);

      return &ret;
    }
    /** Get the function as a string */
    const char* AsString() const { 
      static std::string s;
      if (s.empty()) {
	std::stringstream sstr(s);
	sstr << this->fRec.name << GetParams();
	s = sstr.str();
      }
      return s.c_str();
    }
  protected:
    template <typename Arg, typename Set>
    bool SetOne(InterfaceType* inter, ExecuteParamRec& param, Set set)
    {
      // Check if this setter is defined.
      if (!set) return false;

      // Type of return parameter 
      typedef typename TypeTrait<Arg>::VarType RefType;
      RefType var;
      
      // Check that the passed parameter is if the right type. 
      CheckArg(var, param.args->getNext(), param.thread);
      
      // Now assign the return parameter 
      (inter->*set)(var.getValue());
      return true;
    }
    /** Make a parameter string */
    const char* GetParams() const { 
      if (!fParams.empty()) return fParams.c_str();
      std::stringstream s(fParams);
      s << "(";
      int n = 0;
      n = GetParam<Arg01Type>(n, fSet01, s);
      n = GetParam<Arg02Type>(n, fSet02, s);
      n = GetParam<Arg03Type>(n, fSet03, s);
      s << ")";
      fParams = s.str();
      return fParams.c_str();
    }
    template <typename Arg, typename Set>
    int GetParam(int i, Set set, std::stringstream& s) const { 
      if (!set) return i;
      if (i != 0) s << ", ";
      s << TypeTrait<Arg>::AsString() << "& arg" << i;
      return ++i;
    }
    /** parameter string */
    mutable std::string fParams;
    /** First setter */
    Set01Type fSet01;
    /** Second setter */
    Set02Type fSet02;
    /** Third setter */
    Set03Type fSet03;
  };

  //____________________________________________________________________
  template <typename T, 
	    typename C,
	    typename R,
	    typename A1,
	    typename A2,
	    typename A3>
  InterfaceDeclT<C>*
  MakeWriteIF(const char* name, 
	      T*   (C::*i)(),
	      void (T::*f1)(A1), 
	      void (T::*f2)(A2), 
	      void (T::*f3)(A3))
  {
    return new WriteInterfaceDecl<T,C,R,A1,A2,A3>(name, i, f1, f2, f3);
  }
}
#endif

//==================================================================
#define NO_CHECK
#ifndef NO_CHECK
# define CHECK_CALL(X,Y,Z) \
  do { \
    std::string zs(Z), xs(X.funcName.c_str()); \
    if (X.funcNum != Y || zs != xs) \
      std::cerr << "Wrong function # " << X.funcNum \
		<< " for call to real function # " << Y \
		<< " (wanted " << xs << " got " << zs << ")" << std::endl; \
    else \
     std::cout << "Requested: " << std::setw(3) << X.funcNum \
               << " Got: " << std::setw(3) << Y << " (" \
               <<  xs << " " << zs << ")" << std::endl; \
    return 0; \
  } while(false) 
#else 
# define CHECK_CALL(X,Y,Z) do { } while (false)
#endif

//
// EOF
//

  
  

  
    
