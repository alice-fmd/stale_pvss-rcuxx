#include "RcuxxExternHdl.h"
#include "Util.h"
#include "Rcu.h"
#include <rcuxx/Rcu.h>
#include <stdexcept>
#ifndef ADDVERINFO_HXX
# include <addVerInfo.cxx>
#else 
# include ADDVERINFO_HXX
#endif


namespace 
{
#ifdef NO_CHECK
  bool fgDebug = true;
#else
  bool fgDebug = false;
#endif
}

/** List of interface functions, and their return values and arguments */
static FunctionListRec fnList[] = {
  // TODO add for every new function an entry
  { INTEGER_VAR, "rcuOpen",		"(string url, int emul, int debug)", false },
  { INTEGER_VAR, "rcuClose",		"(int handle)",                      false },
  { INTEGER_VAR, "rcuAddAltro",		"(int handle)",                      false },
  { INTEGER_VAR, "rcuAddBc",		"(int handle, int type)",            false },
  /** Command access */ 
  { INTEGER_VAR, "rcuExecABORT",	"(int handle)",	false },
  { INTEGER_VAR, "rcuExecCLR_EVTAG","(int handle)",	false },
  { INTEGER_VAR, "rcuExecDCS_ON",	"(int handle)",	false },
  { INTEGER_VAR, "rcuExecDDL_ON",	"(int handle)",	false },
  { INTEGER_VAR, "rcuExecEXEC",		"(int handle, unsigned addr)",	false },
  { INTEGER_VAR, "rcuExecFECRST",	"(int handle)",	false },
  { INTEGER_VAR, "rcuExecGLB_RESET",    "(int handle)",	false },
  { INTEGER_VAR, "rcuExecL1_CMD",	"(int handle)",	false },
  { INTEGER_VAR, "rcuExecL1_I2C",	"(int handle)",	false },
  { INTEGER_VAR, "rcuExecL1_TTC",	"(int handle)",	false },
  { INTEGER_VAR, "rcuExecRCU_RESET",    "(int handle)",	false },
  { INTEGER_VAR, "rcuExecRDABORT",	"(int handle)",	false },
  { INTEGER_VAR, "rcuExecRDFM",		"(int handle)",	false },
  { INTEGER_VAR, "rcuExecRS_DMEM1",	"(int handle)",	false },
  { INTEGER_VAR, "rcuExecRS_DMEM2",	"(int handle)",	false },
  { INTEGER_VAR, "rcuExecRS_STATUS",    "(int handle)",	false },
  { INTEGER_VAR, "rcuExecRS_TRCFG",	"(int handle)",	false },
  { INTEGER_VAR, "rcuExecRS_TRCNT",	"(int handle)",	false },
  { INTEGER_VAR, "rcuExecSCCOMMAND",    "(int handle)",	false },
  { INTEGER_VAR, "rcuExecSWTRG",	"(int handle)",	false },
  { INTEGER_VAR, "rcuExecTRG_CLR",	"(int handle)",	false },
  { INTEGER_VAR, "rcuExecWRFM",		"(int handle)",	false },
  { INTEGER_VAR, "rcuExecRS_ERRREG",    "(int handle)",	false },
  /** Register read access */       
  { INTEGER_VAR, "rcuReadERRST",	"(int handle, bool& pattern, bool& abort, bool& timeout, bool& altro, bool& add, bool& busy, unsigned& where)",            false },
  { INTEGER_VAR, "rcuReadTRCNT",	"(int handle, unsigned& l1, unsigned& l2)",	false },
  { INTEGER_VAR, "rcuReadLWADD",	"(int handle, unsigned& v1, unsigned& v2)",	false },
  { INTEGER_VAR, "rcuReadIRADD",	"(int handle, unsigned& a)",	false },
  { INTEGER_VAR, "rcuReadIRDAT",	"(int handle, unsigned& d)",	false },
  { INTEGER_VAR, "rcuReadEVWORD",	"(int handle, unsigned& add1, unsigned& add2)",	false },
  { INTEGER_VAR, "rcuReadACTFEC",	"(int handle, unsigned& mask)",	false },
  { INTEGER_VAR, "rcuReadRDOFEC",	"(int handle, unsigned& mask)",	false },
  { INTEGER_VAR, "rcuReadTRCFG1",	"(int handle, bool& o, bool& p, unsigned bmd, unsigned& mode, unsigned& wait, unsigned& wptr, unsigned& rptr, unsigned& remb, bool& empty, bool& full)",	false },
  { INTEGER_VAR, "rcuReadTRCFG2",	"(int handle, bool& push, bool& h)",	false },
  { INTEGER_VAR, "rcuReadFMIREG",	"(int handle, unsigned& d)",	false },
  { INTEGER_VAR, "rcuReadFMOREG",	"(int handle, unsigned& d)",	false },
  { INTEGER_VAR, "rcuReadPMCFG",	"(int handle, unsigned& begin, unsigned& end)",	false },
  { INTEGER_VAR, "rcuReadCHADD",	"(int handle, unsigned& v1, unsigned& v2)",	false },
  { INTEGER_VAR, "rcuReadINTREG",	"(int handle, bool& id, unsigned& w, bool& soft, bool& ans, bool& sclk, bool& alps, bool& paps, bool& dc, bool& dv, bool& ac, bool& av, bool& temp)",	false },
  { INTEGER_VAR, "rcuReadRESREG",	"(int handle, unsigned& a, unsigned& r)",	false },
  { INTEGER_VAR, "rcuReadERRREG",	"(int handle, bool& noack, bool& notact)",	false },
  { INTEGER_VAR, "rcuReadINTMOD",	"(int handle, bool& a, bool& b)",	false },
  { INTEGER_VAR, "rcuReadStatusCount",  "(int handle, unsigned& n)",	false },
  { INTEGER_VAR, "rcuReadStatusEntry",  "(int handle, unsigned i, bool& id, unsigned& w, bool& soft, bool& ans, bool& sclk, bool& alps, bool& paps, bool& dc, bool& dv, bool& ac, bool& av, bool& temp)",	false },
  { INTEGER_VAR, "rcuReadACL",		"(int handle, dyn_uint& data)",	false },
  { INTEGER_VAR, "rcuReadIMEM",		"(int handle, dyn_uint& data)",	false },
  { INTEGER_VAR, "rcuReadPMEM",		"(int handle, dyn_uint& data)",	false },
  /** Register write access */       
  { INTEGER_VAR, "rcuWriteACTFEC",	"(int handle, unsigned mask)",	false },
  { INTEGER_VAR, "rcuWriteTRCFG1",	"(int handle, bool o, bool p, unsigned b, unsigned m, unsigned t)",	false },
  { INTEGER_VAR, "rcuWriteTRCFG2",	"(int handle, bool push, bool h)",	false },
  { INTEGER_VAR, "rcuWriteFMIREG",	"(int handle, unsigned d)",	false },
  { INTEGER_VAR, "rcuWriteFMOREG",	"(int handle, unsigned d)",	false },
  { INTEGER_VAR, "rcuWritePMCFG",	"(int handle, unsigned b, unsigned e)",	false },
  { INTEGER_VAR, "rcuWriteRESREG",	"(int handle, unsigned a, unsigned r)",	false },
  { INTEGER_VAR, "rcuWriteINTMOD",	"(int handle, bool a, bool b)",	false },
  { INTEGER_VAR, "rcuWriteACL",		"(int handle, unsigned base, dyn_uint data)",	false },
  { INTEGER_VAR, "rcuWriteIMEM",	"(int handle, unsigned base, dyn_uint data)",	false },
  { INTEGER_VAR, "rcuWritePMEM",	"(int handle, unsigned base, dyn_uint data)",	false },
  { INTEGER_VAR, "rcuZeroACL",		"(int handle)",	false },
  { INTEGER_VAR, "rcuZeroIMEM",		"(int handle)",	false },
  { INTEGER_VAR, "rcuZeroPMEM",		"(int handle)",	false },
  { INTEGER_VAR, "rcuSizeOfACL",	"(int handle)",	false },
  { INTEGER_VAR, "rcuSizeOfIMEM",	"(int handle)",	false },
  { INTEGER_VAR, "rcuSizeOfPMEM",	"(int handle)",	false },
  /** Address interface */ 
  { INTEGER_VAR, "altroSetAddress",	"(int handle, unsigned bd, unsigned chip, unsigned channel, bool broadcast)",	false },
  { INTEGER_VAR, "altroGetAddress",	"(int handle, unsigned& bd, unsigned& chip, unsigned& channel)",	false },
  /** Command interfaces */ 
  { INTEGER_VAR, "altroExecWPINC",	"(int handle)",	false },
  { INTEGER_VAR, "altroExecRPINC",	"(int handle)",	false },
  { INTEGER_VAR, "altroExecCHRDO",	"(int handle)",	false },
  { INTEGER_VAR, "altroExecSWTRG",	"(int handle)",	false },
  { INTEGER_VAR, "altroExecTRCLR",	"(int handle)",	false },
  { INTEGER_VAR, "altroExecERCLR",	"(int handle)",	false },
  /** Register read access */ 
  { INTEGER_VAR, "altroReadK1",		"(int handle, unsigned& v)",	false },
  { INTEGER_VAR, "altroReadK2",		"(int handle, unsigned& v)",	false },
  { INTEGER_VAR, "altroReadK3",		"(int handle, unsigned& v)",	false },
  { INTEGER_VAR, "altroReadL1",		"(int handle, unsigned& v)",	false },
  { INTEGER_VAR, "altroReadL2",		"(int handle, unsigned& v)",	false },
  { INTEGER_VAR, "altroReadL3",		"(int handle, unsigned& v)",	false },
  { INTEGER_VAR, "altroReadVFPED",	"(int handle, unsigned& vp, unsigned& fp)",	false },
  { INTEGER_VAR, "altroReadPMDTA",	"(int handle, unsigned& v)",	false },
  { INTEGER_VAR, "altroReadZSTHR",	"(int handle, unsigned& offset,unsigned& zs_thr)",	false },
  { INTEGER_VAR, "altroReadBCTHR",	"(int handle, unsigned& lo, unsigned& hi)",	false },
  { INTEGER_VAR, "altroReadTRCFG",	"(int handle, unsigned& b, unsigned& e)",	false },
  { INTEGER_VAR, "altroReadDPCFG",	"(int handle, unsigned&	firstBMode, bool& IsFirstBPol, unsigned& SecBPre, unsigned& SecBPost, bool& IsSecBEnable, unsigned&	ZSPre, unsigned& ZSPost, unsigned& ZSGlitch, bool& IsZSEnable)",	false },
  { INTEGER_VAR, "altroReadDPCF2",	"(int handle, unsigned& ptrg, bool& IsFlt_en, bool& IsPwsv, unsigned& buff)",	false },
  { INTEGER_VAR, "altroReadPMADD",	"(int handle, unsigned& pmadd)",	false },
  { INTEGER_VAR, "altroReadERSTR",	"(int handle, bool& IsRdo, bool& IsInt2Seu, bool& IsInt1Seu, bool& IsMMU2Seu, bool& IsMMU1Seu, bool& IsTrigger, bool& IsInstr, bool& IsParity, bool& IsEmpty, bool& IsFull, unsigned& buf, unsigned& wp, unsigned& rp)",	false },
  { INTEGER_VAR, "altroReadADEVL",	"(int handle, unsigned& hadd, unsigned& evl)",	false },
  { INTEGER_VAR, "altroReadTRCNT",	"(int handle, unsigned& v)",	false },
  { INTEGER_VAR, "altroWriteK1",	"(int handle, unsigned v)",	false },
  { INTEGER_VAR, "altroWriteK2",	"(int handle, unsigned v)",	false },
  { INTEGER_VAR, "altroWriteK3",	"(int handle, unsigned v)",	false },
  { INTEGER_VAR, "altroWriteL1",	"(int handle, unsigned v)",	false },
  { INTEGER_VAR, "altroWriteL2",	"(int handle, unsigned v)",	false },
  { INTEGER_VAR, "altroWriteL3",	"(int handle, unsigned v)",	false },
  { INTEGER_VAR, "altroWriteVFPED",	"(int handle, unsigned v)",	false },
  { INTEGER_VAR, "altroWritePMDTA",	"(int handle, unsigned v)",	false },
  { INTEGER_VAR, "altroWriteZSTHR",	"(int handle, unsigned offset,unsigned zs_thr)",	false },
  { INTEGER_VAR, "altroWriteBCTHR",	"(int handle, unsigned lo, unsigned hi)",	false },
  { INTEGER_VAR, "altroWriteTRCFG",	"(int handle, unsigned b, unsigned e)",	false },
  { INTEGER_VAR, "altroWriteDPCFG",	"(int handle, unsigned firstBMode, bool IsFirstBPol, unsigned SecBPre, unsigned SecBPost, bool IsSecBEnable, unsigned	ZSPre, unsigned ZSPost, unsigned ZSGlitch, bool IsZSEnable)",	false },
  { INTEGER_VAR, "altroWriteDPCF2",	"(int handle, unsigned ptrg, bool IsFlt_en, bool IsPwsv, unsigned buff)",	false },
  { INTEGER_VAR, "altroWritePMADD",	"(int handle, unsigned v)",	false },
  /** Address interface */ 
  { INTEGER_VAR, "bcSetAddress",	"(int handle, unsigned  board, bool broadcast)", false },
  { INTEGER_VAR, "bcGetAddress",	"(int handle, unsigned &board)", false },
   /** Command interface  */
  { INTEGER_VAR, "bcExecCNTLAT",	"(int handle)",	false },
  { INTEGER_VAR, "bcExecCNTCLR",	"(int handle)",	false },
  { INTEGER_VAR, "bcExecCSR1CLR",	"(int handle)",	false },
  { INTEGER_VAR, "bcExecALRST",		"(int handle)",	false },
  { INTEGER_VAR, "bcExecBCRST",		"(int handle)",	false },
  { INTEGER_VAR, "bcExecSTCNV",		"(int handle)",	false },
  { INTEGER_VAR, "bcExecSCEVL",		"(int handle)",	false },
  { INTEGER_VAR, "bcExecEVLRDO",	"(int handle)",	false },
  { INTEGER_VAR, "bcExecSTTSM",		"(int handle)",	false },
  { INTEGER_VAR, "bcExecACQRDO",	"(int handle)",	false },
  /** Register read access */
  { INTEGER_VAR, "bcReadTEMP_TH",	"(int handle, float& tempTh)",	false },
  { INTEGER_VAR, "bcReadAV_TH",		"(int handle, float& avTh)",	false },
  { INTEGER_VAR, "bcReadAC_TH",		"(int handle, float& acTh)",	false },
  { INTEGER_VAR, "bcReadDV_TH",		"(int handle, float& dvTh)",	false },
  { INTEGER_VAR, "bcReadDC_TH",		"(int handle, float& dcTh)",	false },
  { INTEGER_VAR, "bcReadTEMP",		"(int handle, float& t)",	false },
  { INTEGER_VAR, "bcReadAV",		"(int handle, float& av)",	false },
  { INTEGER_VAR, "bcReadAC",		"(int handle, float& ac)",	false },
  { INTEGER_VAR, "bcReadDV",		"(int handle, float& dv)",	false },
  { INTEGER_VAR, "bcReadDC",		"(int handle, float& dc)",	false },
  { INTEGER_VAR, "bcReadL1CNT",		"(int handle, unsigned& c)",	false },
  { INTEGER_VAR, "bcReadL2CNT",		"(int handle, unsigned& c)",	false },
  { INTEGER_VAR, "bcReadDSTBCNT",	"(int handle, unsigned& c)",	false },
  { INTEGER_VAR, "bcReadSCLKCNT",	"(int handle, unsigned& c)",	false },
  { INTEGER_VAR, "bcReadTSMWORD",	"(int handle, unsigned& w)",	false },
  { INTEGER_VAR, "bcReadUSRATIO",	"(int handle, unsigned& v)",	false },
  { INTEGER_VAR, "bcReadCSR0",		"(int handle, bool &iparity, bool &iinstr, bool &isclk, bool &ialps, bool &ipaps, bool &idc, bool &idv, bool &iac, bool &iav, bool &it, bool &icnv)",	false },
  { INTEGER_VAR, "bcReadCSR1",		"(int handle, bool &ierr, bool &isc, bool &ialtro, bool &iinter ,bool &iparity, bool &iinstr, bool &isclk, bool &ialps, bool &ipaps, bool &idc, bool &idv, bool &iac, bool &iav, bool &it)",	false },
  { INTEGER_VAR, "bcReadCSR2",		"(int handle, unsigned& hadd, unsigned& altro_add, unsigned& adc_add, bool& IsIso, bool& IsContTsm, bool& IsAdcClk, bool& IsRdoClk, bool& IsPasaSw, bool& IsAltrosSw)",	false },
  { INTEGER_VAR, "bcReadCSR3",		"(int handle, unsigned& war, unsigned& watchdog, bool& IsCnv)",	false },
  /** Register write access */
  { INTEGER_VAR, "bcWriteTEMP_TH",	"(int handle, float t)",	false },
  { INTEGER_VAR, "bcWriteAV_TH",	"(int handle, float th)",	false },
  { INTEGER_VAR, "bcWriteAC_TH",	"(int handle, float th)",	false },
  { INTEGER_VAR, "bcWriteDV_TH",	"(int handle, float th)",	false },
  { INTEGER_VAR, "bcWriteDC_TH",	"(int handle, float th)",	false },
  { INTEGER_VAR, "bcWriteTSMWORD",	"(int handle, unsigned w)",	false },
  { INTEGER_VAR, "bcWriteUSRATIO",	"(int handle, unsigned v)",	false },
  { INTEGER_VAR, "bcWriteCSR0",		"(int handle, bool parity, bool instr, bool sclk, bool alps, bool paps, bool dc, bool dv, bool ac, bool av, bool t, bool cnv)",	false },
  { INTEGER_VAR, "bcWriteCSR2",		"(int handle, unsigned altro_add, unsigned adc_add, bool IsIso, bool IsContTsm, bool IsAdcClk, bool IsRdoClk, bool IsPasaSw, bool IsAltrosSw)",	false },
  { INTEGER_VAR, "bcWriteCSR3",		"(int handle, unsigned war, unsigned watchdog)",	false },
  /** fmd digitizer specific part  - exec */
  { INTEGER_VAR, "fmddExecFakeTrigger",	"(int handle)",                      false },
  { INTEGER_VAR, "fmddExecSoftReset",	"(int handle)",                      false },
  { INTEGER_VAR, "fmddExecChangeDacs",	"(int handle)",                      false },
  { INTEGER_VAR, "fmddExecPulserOn",	"(int handle)",                      false },
  { INTEGER_VAR, "fmddExecPulserOff",	"(int handle)",                      false },
  { INTEGER_VAR, "fmddExecTestOn",	"(int handle)",                      false },
  { INTEGER_VAR, "fmddExecTestOff",	"(int handle)",                      false },
  { INTEGER_VAR, "fmddExecCalibrationRun",	"(int handle)",                      false },
  /** fmd digitizer specific part  - read */
  { INTEGER_VAR, "fmddReadL0CNT",	"(int handle, unsigned& c)",	false },
  { INTEGER_VAR, "fmddReadAL_DIG_I",	"(int handle, float& c)",	false },
  { INTEGER_VAR, "fmddReadAL_DIG_I_TH",	"(int handle, float& c)",	false },
  { INTEGER_VAR, "fmddReadAL_ANA_I",	"(int handle, float& c)",	false },
  { INTEGER_VAR, "fmddReadAL_ANA_I_TH",	"(int handle, float& c)",	false },
  { INTEGER_VAR, "fmddReadAL_DIG_U",	"(int handle, float& c)",	false },
  { INTEGER_VAR, "fmddReadAL_DIG_U_TH",	"(int handle, float& c)",	false },
  { INTEGER_VAR, "fmddReadAL_ANA_U",	"(int handle, float& c)",	false },
  { INTEGER_VAR, "fmddReadAL_ANA_U_TH",	"(int handle, float& c)",	false },
  { INTEGER_VAR, "fmddReadT1SENS",	"(int handle, float& c)",	false },
  { INTEGER_VAR, "fmddReadT1SENS_TH",	"(int handle, float& c)",	false },
  { INTEGER_VAR, "fmddReadT2SENS",	"(int handle, float& c)",	false },
  { INTEGER_VAR, "fmddReadT2SENS_TH",	"(int handle, float& c)",	false },
  { INTEGER_VAR, "fmddReadT1",		"(int handle, float& c)",	false },
  { INTEGER_VAR, "fmddReadT1_TH",	"(int handle, float& c)",	false },
  { INTEGER_VAR, "fmddReadT2",		"(int handle, float& c)",	false },
  { INTEGER_VAR, "fmddReadT2_TH",	"(int handle, float& c)",	false },
  { INTEGER_VAR, "fmddReadT3",		"(int handle, float& c)",	false },
  { INTEGER_VAR, "fmddReadT3_TH",	"(int handle, float& c)",	false },
  { INTEGER_VAR, "fmddReadT4",		"(int handle, float& c)",	false },
  { INTEGER_VAR, "fmddReadT4_TH",	"(int handle, float& c)",	false },
  { INTEGER_VAR, "fmddReadVA_REC_IP",	"(int handle, float& c)",	false },
  { INTEGER_VAR, "fmddReadVA_REC_IP_TH","(int handle, float& c)",	false },
  { INTEGER_VAR, "fmddReadVA_REC_UP",	"(int handle, float& c)",	false },
  { INTEGER_VAR, "fmddReadVA_REC_UP_TH","(int handle, float& c)",	false },
  { INTEGER_VAR, "fmddReadVA_REC_IM",	"(int handle, float& c)",	false },
  { INTEGER_VAR, "fmddReadVA_REC_IM_TH","(int handle, float& c)",	false },
  { INTEGER_VAR, "fmddReadVA_REC_UM",	"(int handle, float& c)",	false },
  { INTEGER_VAR, "fmddReadVA_REC_UM_TH","(int handle, float& c)",	false },
  { INTEGER_VAR, "fmddReadVA_SUP_IP",	"(int handle, float& c)",	false },
  { INTEGER_VAR, "fmddReadVA_SUP_IP_TH","(int handle, float& c)",	false },
  { INTEGER_VAR, "fmddReadVA_SUP_UP",	"(int handle, float& c)",	false },
  { INTEGER_VAR, "fmddReadVA_SUP_UP_TH","(int handle, float& c)",	false },
  { INTEGER_VAR, "fmddReadVA_SUP_IM",	"(int handle, float& c)",	false },
  { INTEGER_VAR, "fmddReadVA_SUP_IM_TH","(int handle, float& c)",	false },
  { INTEGER_VAR, "fmddReadVA_SUP_UM",	"(int handle, float& c)",	false },
  { INTEGER_VAR, "fmddReadVA_SUP_UM_TH","(int handle, float& c)",	false },
  { INTEGER_VAR, "fmddReadGTL_U",	"(int handle, float& c)",	false },
  { INTEGER_VAR, "fmddReadGTL_U_TH",	"(int handle, float& c)",	false },
  { INTEGER_VAR, "fmddReadFLASH_I",	"(int handle, float& c)",	false },
  { INTEGER_VAR, "fmddReadFLASH_I_TH",	"(int handle, float& c)",	false },
  { INTEGER_VAR, "fmddReadShiftClock",	"(int handle, unsigned& d, unsigned& p)",	false },
  { INTEGER_VAR, "fmddReadRange",	"(int handle, unsigned& lo, unsigned& hi)",	false },
  { INTEGER_VAR, "fmddReadSampleClock",	"(int handle, unsigned& d, unsigned& p)",	false },
  { INTEGER_VAR, "fmddReadHoldWait",	"(int handle, unsigned& v)",	false },
  { INTEGER_VAR, "fmddReadL0Timeout",	"(int handle, unsigned& v)",	false },
  { INTEGER_VAR, "fmddReadL1Timeout",	"(int handle, unsigned& v)",	false },
  { INTEGER_VAR, "fmddReadStatus",	"(int handle, bool &IsCalOn, bool &IsTriggerBusy, bool &IsDacBusy, bool &IsReadoutBusy, bool &IsBoxBusy, bool &IsIncompleteRo, bool &IsTriggerOverlap, bool &IsTriggerTimeout, bool &IsTestOn, bool &IsCalManOn)",	false },
  { INTEGER_VAR, "fmddReadShapeBias",	"(int handle, unsigned& t, unsigned& b)",	false },
  { INTEGER_VAR, "fmddReadPulser",	"(int handle, unsigned& l, unsigned& s)",	false },
  { INTEGER_VAR, "fmddReadCalIter",	"(int handle, unsigned& v)",	false },
  { INTEGER_VAR, "fmddReadVFP",	"(int handle, unsigned& t, unsigned& b)",	false },
  { INTEGER_VAR, "fmddReadVFS",	"(int handle, unsigned& t, unsigned& b)",	false },
  /** fmd digitizer specific part  - write */
  { INTEGER_VAR, "fmddWriteAL_DIG_I_TH","(int handle, float c)",	false },
  { INTEGER_VAR, "fmddWriteAL_ANA_I_TH","(int handle, float c)",	false },
  { INTEGER_VAR, "fmddWriteAL_DIG_U_TH","(int handle, float c)",	false },
  { INTEGER_VAR, "fmddWriteAL_ANA_U_TH","(int handle, float c)",	false },
  { INTEGER_VAR, "fmddWriteT1SENS_TH",	"(int handle, float c)",	false },
  { INTEGER_VAR, "fmddWriteT2SENS_TH",	"(int handle, float c)",	false },
  { INTEGER_VAR, "fmddWriteT1_TH",	"(int handle, float c)",	false },
  { INTEGER_VAR, "fmddWriteT2_TH",	"(int handle, float c)",	false },
  { INTEGER_VAR, "fmddWriteT3_TH",	"(int handle, float c)",	false },
  { INTEGER_VAR, "fmddWriteT4_TH",	"(int handle, float c)",	false },
  { INTEGER_VAR, "fmddWriteVA_REC_IP_TH","(int handle, float c)",	false },
  { INTEGER_VAR, "fmddWriteVA_REC_UP_TH","(int handle, float c)",	false },
  { INTEGER_VAR, "fmddWriteVA_REC_IM_TH","(int handle, float c)",	false },
  { INTEGER_VAR, "fmddWriteVA_REC_UM_TH","(int handle, float c)",	false },
  { INTEGER_VAR, "fmddWriteVA_SUP_IP_TH","(int handle, float c)",	false },
  { INTEGER_VAR, "fmddWriteVA_SUP_UP_TH","(int handle, float c)",	false },
  { INTEGER_VAR, "fmddWriteVA_SUP_IM_TH","(int handle, float c)",	false },
  { INTEGER_VAR, "fmddWriteVA_SUP_UM_TH","(int handle, float c)",	false },
  { INTEGER_VAR, "fmddWriteGTL_U_TH",	"(int handle, float c)",	false },
  { INTEGER_VAR, "fmddWriteFLASH_I_TH",	"(int handle, float c)",	false },
  { INTEGER_VAR, "fmddWriteShiftClock",	"(int handle, unsigned d, unsigned p)",	false },
  { INTEGER_VAR, "fmddWriteRange",	"(int handle, unsigned lo, unsigned hi)",	false },
  { INTEGER_VAR, "fmddWriteSampleClock","(int handle, unsigned d, unsigned p)",	false },
  { INTEGER_VAR, "fmddWriteHoldWait",	"(int handle, unsigned v)",	false },
  { INTEGER_VAR, "fmddWriteL0Timeout",	"(int handle, unsigned v)",	false },
  { INTEGER_VAR, "fmddWriteL1Timeout",	"(int handle, unsigned v)",	false },
  { INTEGER_VAR, "fmddWriteShapeBias",	"(int handle, unsigned t, unsigned b)",	false },
  { INTEGER_VAR, "fmddWritePulser",	"(int handle, unsigned l, unsigned s)",	false },
  { INTEGER_VAR, "fmddWriteCalIter",	"(int handle, unsigned v)",	false },
  { INTEGER_VAR, "fmddWriteVFP",	"(int handle, unsigned t, unsigned b)",	false },
  { INTEGER_VAR, "fmddWriteVFS",	"(int handle, unsigned t, unsigned b)",	false }
};

//____________________________________________________________________
BaseExternHdl *newExternHdl(BaseExternHdl *nextHdl)
{
  Rcuxx::DebugGuard g(fgDebug, "newExternHdl");
  // this line counts the number of functions you want to implement.
  PVSSulong funcCount = sizeof(fnList) / sizeof(FunctionListRec);

  // now allocate the new instance
  return new RcuxxExternHdl(nextHdl, funcCount, fnList);
}

//____________________________________________________________________
const Variable*
RcuxxExternHdl::execute(ExecuteParamRec &param)
{
  Rcuxx::DebugGuard g(fgDebug, "execute [# %d -> name: %s, # args %d]", 
		      param.funcNum, param.funcName.c_str(), 
		      (param.args ? param.args->getNumberOfItems() : 0));

  static IntegerVar ret;
  ret.setValue(-1);
  
  // Clear list of errors. 
#ifdef NO_CHECK
  param.thread->clearLastError();
#endif

  // Everyhting inside a try box 
  try {
    switch (param.funcNum)   {
    case kOpen:  return Open(param);
    case kClose: return Close(param);
    default:     return Call(param);
    }
  }
  catch (Pvss::interface_missing& e) {
    ErrClass err(ErrClass::PRIO_WARNING, ErrClass::ERR_CONTROL, 
		 ErrClass::ILLEGAL_OP, param.clientData->getLocation(), 
		 param.funcName, e.what());
    param.thread->appendLastError(&err);
    if (fgDebug) std::cerr << "Rcuxx exception: " << e.what() << std::endl;
    ret.setValue(-1);
  }    
  catch (Pvss::not_lvalue& e) {
    ErrClass err(ErrClass::PRIO_WARNING, ErrClass::ERR_PARAM, 
		 ErrClass::NO_LVALUE, param.clientData->getLocation(), 
		 param.funcName, e.what());
    param.thread->appendLastError(&err);
    if (fgDebug) std::cerr << "Rcuxx exception: " << e.what() << std::endl;
    ret.setValue(-1);
  }    
  catch (std::invalid_argument& e) {
    ErrClass err(ErrClass::PRIO_WARNING, ErrClass::ERR_PARAM, 
		 ErrClass::ARG_MISSING, param.clientData->getLocation(), 
		 param.funcName, e.what());
    param.thread->appendLastError(&err);
    if (fgDebug) std::cerr << "Rcuxx exception: " << e.what() << std::endl;
    ret.setValue(-1);
  }    
  catch (std::bad_cast& e) {
    ErrClass err(ErrClass::PRIO_WARNING, ErrClass::ERR_PARAM, 
		 ErrClass::ILLEGAL_ARG, param.clientData->getLocation(), 
		 param.funcName, e.what());
    param.thread->appendLastError(&err);
    if (fgDebug) std::cerr << "Rcuxx exception: " << e.what() << std::endl;
    ret.setValue(-1);
  }    
  catch (std::exception& e) {
    ErrClass err(ErrClass::PRIO_SEVERE, ErrClass::ERR_CONTROL, 
		 ErrClass::UNEXPECTEDSTATE, param.clientData->getLocation(), 
		 param.funcName, e.what());
    param.thread->appendLastError(&err);
    if (fgDebug) std::cerr << "Rcuxx exception: " << e.what() << std::endl;
    ret.setValue(-1);
  }
  return &ret;
}

//____________________________________________________________________
const IntegerVar*
RcuxxExternHdl::Open(ExecuteParamRec &param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcuxx::Open()");
  CHECK_CALL(param, kOpen, "rcuOpen");

  static IntegerVar ret;
  ret.setValue(-1);
  // check number of arguments
  TextVar url;
  if (!param.args) throw ErrClass::ARG_MISSING;
  
  Pvss::CheckArg(url, param.args->getFirst(), param.thread);

  std::string s = url.getValue();
  UrlMap::iterator i = fMap.find(s);
  if (i != fMap.end()) {
    ret.setValue(i->second);
    return &ret;
  }

  // It's not fatal if we don't have the remaining arguments 
  IntegerVar emul, debug;
  try {
    Pvss::CheckArg(emul, param.args->getNext(), param.thread);
    Pvss::CheckArg(debug, param.args->getNext(), param.thread);
  }
  catch (ErrClass::ErrCode& errCode) {}
  
  Rcuxx::Rcu* rcu = Rcuxx::Rcu::Open(url.getValue(), 
				     emul.getValue(), 
				     debug.getValue());
  if (rcu) {
    fRcus.push_back(new Pvss::Rcu(rcu));
    fMap[s] = fRcus.size()-1;
    ret.setValue(fRcus.size()-1);
  }
  return &ret;
}

//____________________________________________________________________
const IntegerVar*
RcuxxExternHdl::Close(ExecuteParamRec &param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcuxx::Close()");
  CHECK_CALL(param, kClose, "rcuClose");

  static IntegerVar ret;
  ret.setValue(-1);
  // check number of arguments
  // TextVar url;
  if (!param.args || param.args->getNumberOfItems() < 1) 
    throw ErrClass::ARG_MISSING;
  IntegerVar handle;
  Pvss::CheckArg(handle, param.args->getFirst(), param.thread);
  
  int idx = handle.getValue();

  if (idx < 0 || idx >= fRcus.size()) {
	  Rcuxx::DebugGuard::Message(fgDebug, "Index %d/%d out of range", idx, fRcus.size());
    throw ErrClass:: INDEX_OUT_OF_RANGE;
  }
  Pvss::Rcu* rcu = fRcus[idx];
  if (!rcu) { 
	  Rcuxx::DebugGuard::Message(fgDebug, "Didn't find an RCU at index %d/%d", idx, fRcus.size());
	throw ErrClass::ILLEGAL_ARG;
  }
  Rcuxx::DebugGuard::Message(fgDebug, "Deleting Rcu at %d", idx);
  delete rcu;
  Rcuxx::DebugGuard::Message(fgDebug, "Zeroing at %d", idx);
  fRcus[idx] = 0;
  for (UrlMap::iterator i = fMap.begin(); i != fMap.end(); i++) {
    if (i->second == idx) { 
		Rcuxx::DebugGuard::Message(fgDebug, "Removing RCU at %d from map", idx);
      fMap.erase(i);
      break;
    }
  }
  ret.setValue(0);
  return &ret;
}

//____________________________________________________________________
const Variable*
RcuxxExternHdl::Call(ExecuteParamRec &param)
{
  Rcuxx::DebugGuard g(fgDebug, "Rcuxx::Call()");
  if (!param.args || param.args->getNumberOfItems() < 1) 
    throw ErrClass::ARG_MISSING;

  IntegerVar handle;
  Pvss::CheckArg(handle, param.args->getFirst(), param.thread);
  
#ifdef NO_CHECK
  int idx = handle.getValue();
  if (idx < 0 || idx >= fRcus.size()) 
    throw ErrClass::INDEX_OUT_OF_RANGE;
#endif

#ifdef NO_CHECK
  Pvss::Rcu* rcu = fRcus[idx];
  if (!rcu) throw ErrClass::ILLEGAL_ARG;
#else 
  Pvss::Rcu* rcu = 0;
  if (fRcus.size() > 0) rcu = fRcus[0];
  if (!rcu) {
    Rcuxx::Rcu* lrcu = Rcuxx::Rcu::Open("/dev/altro0", true, true);
    if (lrcu) {
      rcu = new Pvss::Rcu(lrcu);
      fRcus.push_back(rcu);
      fMap["/dev/altro0"] = fRcus.size()-1;
    }
    else {
      std::cerr << "Failed to make Test Rcu" << std::endl;
      exit(1);
    }
  }
#endif

  return rcu->execute(param);
}

  

/* ___________________________________________________________________
 * EOF
 */
