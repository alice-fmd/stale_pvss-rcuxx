// -*- mode: c++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free dsoftware; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    pvss-rcuxx/Bc.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Declaration of BC interface 
*/
#ifndef Pvss_Bc_h
#define Pvss_Bc_h
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#ifndef _BASEEXTERNHDL_H_
# include <BaseExternHdl.hxx>
#endif
#ifndef PVSS_Altro_h
# include "Altro.h"
#endif
class Variable;
class IntegerVar;
namespace Rcuxx 
{
  class Bc;
}

namespace Pvss
{
  /** @class Bc Bc.h <pvss-rcuxx/Bc.h>
      @brief BC interface 
  */
  class Bc
  {
  public:
    /** Type of call parameters */
    typedef BaseExternHdl::ExecuteParamRec ExecuteParamRec;
    /** Enum of possible function calls */
    enum {
      /** Address interface */ 
      kSetAddress   = Altro::kBcBase, 
      kGetAddress,
      /** Command interface  */
      kExecCNTLAT,	//  Command CNTLAT - Latch counters 
      kExecCNTCLR,	//  Command ALRST - Altro reset 
      kExecCSR1CLR,	//  Command ALRST - Altro reset 
      kExecALRST,	//  Command ALRST - Altro reset 
      kExecBCRST,	//  Command BCRST - Board controller reset 
      kExecSTCNV,	//  Command STCNV - Start conversion 
      kExecSCEVL,	//  Command SCEVL - Slow control 
      kExecEVLRDO,	//  Command EVLRDO -  
      kExecSTTSM,	//  Command STTSM - Start test mode 
      kExecACQRDO,	//  Command ACQRDO - Acquisition read-out 
      /** Register read access */
      kReadTEMP_TH,	//  Register TEMP_TH - temperature threshold 
      kReadAV_TH,	//  Register AV_TH - Analog voltage threshold 
      kReadAC_TH,	//  Register AC_TH - Analog current threshold 
      kReadDV_TH,	//  Register DV_TH - Digital voltage threshold 
      kReadDC_TH,	//  Register DC_TH - Digitial current threshold 
      kReadTEMP,	//  Register TEMP - Current temperatur 
      kReadAV,		//  Register AV - Current analog voltage 
      kReadAC,		//  Register AC - Current analog current 
      kReadDV,		//  Register DV - Current digital voltage 
      kReadDC,		//  Register DC - Current digital current 
      kReadL1CNT,	//  Register L1CNT - L1 counter 
      kReadL2CNT,	//  Register L2CNT - L2 counter 
      kReadSCLKCNT,	//  Register SCLKCNT - Sample clock counter 
      kReadDSTBCNT,	//  Register DSTBCNT - Data strobe counter 
      kReadTSMWORD,	//  Register TSMWORD - Test mode word 
      kReadUSRATIO,	//  Register USRATIO - Under sampling ratio 
      kReadCSR0,	//  Register CSR0 - Control and status 0 
      kReadCSR1,	//  Register CSR1 - Control and status 1 
      kReadCSR2,	//  Register CSR2 - Control and status 2 
      kReadCSR3,	//  Register CSR3 - Control and status 3 
      /** Register write access */
      kWriteTEMP_TH,	//  Register TEMP_TH - temperature threshold 
      kWriteAV_TH,	//  Register AV_TH - Analog voltage threshold 
      kWriteAC_TH,	//  Register AC_TH - Analog current threshold 
      kWriteDV_TH,	//  Register DV_TH - Digital voltage threshold 
      kWriteDC_TH,	//  Register DC_TH - Digitial current threshold 
      kWriteTSMWORD,	//  Register TSMWORD - Test mode word 
      kWriteUSRATIO,	//  Register USRATIO - Under sampling ratio 
      kWriteCSR0,	//  Register CSR0 - Control and status 0 
      kWriteCSR2,	//  Register CSR2 - Control and status 2 
      kWriteCSR3,	//  Register CSR3 - Control and status 3 
      kBcLast
    };

    /** Constructor 
	@param bc Pointer to low-level access */
    Bc(Rcuxx::Bc* bc);
    /** Destructor */ 
    virtual ~Bc();
    
    /** Execute a function.  This is delegated from the handler to
	here. Note, when we get here, at least one parameter is read
	from the execution structore, so we should use `getNext', not
	`getFirst'.  
	@param param Parameters of the function call 
	@return A pointer to the return value */
    virtual const Variable *execute(ExecuteParamRec &param);

  protected:
    /** SetAddress */
    const IntegerVar* SetAddress(ExecuteParamRec& param);
    /** GetAddress */
    const IntegerVar* GetAddress(ExecuteParamRec& param);
    /** Command interface  */
    /** Command CNTLAT - Latch counters */
    const IntegerVar* ExecCNTLAT(ExecuteParamRec& param);
    /** Command ALRST - Altro reset */
    const IntegerVar* ExecCNTCLR(ExecuteParamRec& param);
    /** Command ALRST - Altro reset */
    const IntegerVar* ExecCSR1CLR(ExecuteParamRec& param);
    /** Command ALRST - Altro reset */
    const IntegerVar* ExecALRST(ExecuteParamRec& param);
    /** Command BCRST - Board controller reset */
    const IntegerVar* ExecBCRST(ExecuteParamRec& param);
    /** Command STCNV - Start conversion */
    const IntegerVar* ExecSTCNV(ExecuteParamRec& param);
    /** Command SCEVL - Slow control */
    const IntegerVar* ExecSCEVL(ExecuteParamRec& param);
    /** Command EVLRDO -  */
    const IntegerVar* ExecEVLRDO(ExecuteParamRec& param);
    /** Command STTSM - Start test mode */
    const IntegerVar* ExecSTTSM(ExecuteParamRec& param);
    /** Command ACQRDO - Acquisition read-out */
    const IntegerVar* ExecACQRDO(ExecuteParamRec& param);
    /** Register read access */
    /** Register TEMP_TH - temperature threshold */
    const IntegerVar* ReadTEMP_TH(ExecuteParamRec& param);
    /** Register AV_TH - Analog voltage threshold */
    const IntegerVar* ReadAV_TH(ExecuteParamRec& param);
    /** Register AC_TH - Analog current threshold */
    const IntegerVar* ReadAC_TH(ExecuteParamRec& param);
    /** Register DV_TH - Digital voltage threshold */
    const IntegerVar* ReadDV_TH(ExecuteParamRec& param);
    /** Register DC_TH - Digitial current threshold */
    const IntegerVar* ReadDC_TH(ExecuteParamRec& param);
    /** Register TEMP - Current temperatur */
    const IntegerVar* ReadTEMP(ExecuteParamRec& param);
    /** Register AV - Current analog voltage */
    const IntegerVar* ReadAV(ExecuteParamRec& param);
    /** Register AC - Current analog current */
    const IntegerVar* ReadAC(ExecuteParamRec& param);
    /** Register DV - Current digital voltage */
    const IntegerVar* ReadDV(ExecuteParamRec& param);
    /** Register DC - Current digital current */
    const IntegerVar* ReadDC(ExecuteParamRec& param);
    /** Register L1CNT - L1 counter */
    const IntegerVar* ReadL1CNT(ExecuteParamRec& param);
    /** Register L2CNT - L2 counter */
    const IntegerVar* ReadL2CNT(ExecuteParamRec& param);
    /** Register SCLKCNT - Sample clock counter */
    const IntegerVar* ReadSCLKCNT(ExecuteParamRec& param);
    /** Register DSTBCNT - Data strobe counter */
    const IntegerVar* ReadDSTBCNT(ExecuteParamRec& param);
    /** Register TSMWORD - Test mode word */
    const IntegerVar* ReadTSMWORD(ExecuteParamRec& param);
    /** Register USRATIO - Under sampling ratio */
    const IntegerVar* ReadUSRATIO(ExecuteParamRec& param);
    /** Register CSR0 - Control and status 0 */
    const IntegerVar* ReadCSR0(ExecuteParamRec& param);
    /** Register CSR1 - Control and status 1 */
    const IntegerVar* ReadCSR1(ExecuteParamRec& param);
    /** Register CSR2 - Control and status 2 */
    const IntegerVar* ReadCSR2(ExecuteParamRec& param);
    /** Register CSR3 - Control and status 3 */
    const IntegerVar* ReadCSR3(ExecuteParamRec& param);
    /** Register write access */
    /** Register TEMP_TH - temperature threshold */
    const IntegerVar* WriteTEMP_TH(ExecuteParamRec& param);
    /** Register AV_TH - Analog voltage threshold */
    const IntegerVar* WriteAV_TH(ExecuteParamRec& param);
    /** Register AC_TH - Analog current threshold */
    const IntegerVar* WriteAC_TH(ExecuteParamRec& param);
    /** Register DV_TH - Digital voltage threshold */
    const IntegerVar* WriteDV_TH(ExecuteParamRec& param);
    /** Register DC_TH - Digitial current threshold */
    const IntegerVar* WriteDC_TH(ExecuteParamRec& param);
    /** Register TSMWORD - Test mode word */
    const IntegerVar* WriteTSMWORD(ExecuteParamRec& param);
    /** Register USRATIO - Under sampling ratio */
    const IntegerVar* WriteUSRATIO(ExecuteParamRec& param);
    /** Register CSR0 - Control and status 0 */
    const IntegerVar* WriteCSR0(ExecuteParamRec& param);
    /** Register CSR2 - Control and status 2 */
    const IntegerVar* WriteCSR2(ExecuteParamRec& param);
    /** Register CSR3 - Control and status 3 */
    const IntegerVar* WriteCSR3(ExecuteParamRec& param);

    /** Pointer to BC interface */
    Rcuxx::Bc*  fBc;
  };
}

#endif
//
// EOF
//
